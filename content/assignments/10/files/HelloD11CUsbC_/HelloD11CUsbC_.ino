#define led_pin PIN_PA05

int blinkDelay = 100;

void setup() {
  pinMode(led_pin, OUTPUT);
}

void loop() {
  digitalWrite(led_pin, true);
  delay(blinkDelay);
  digitalWrite(led_pin, false);
  delay(blinkDelay);
}
