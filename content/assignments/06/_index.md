---
title: '06 / Electronics Production'
---
______
## Task

Do the following to complete the assignment.

- Characterize the design rules for the PCB production process at the Aalto Fablab.
- Make an in-circuit programmer. Create tool paths for the milling machine, mill it and stuff (solder components) on the board.
- Test the board and debug if needed.
- Document the process in a new page on your documentation website using pictures and text.

&nbsp;
______
## Assignment 06 / Electronics Production
{{<figure src="./hero-shot.jpg">}}

&nbsp;
______

### **Characterize the design rules for the PCB production process**

UPDI D11C. This is a PCB board that I made in the 6th assignment week. This is a D11C-based programmer with the new 2x03 UPDI connector. (The 2x3 IDC connector is meant to be reversible, i.e. it can be connected in both orientations on the target side, as long as the same connector is replicated there, with at least one pad of each type routed to the AVR chip. [more details ⇱](https://gitlab.fabcloud.org/pub/programmers/programmer-updi-d11c)) This board includes AT tiny chip and has a symmetrical connector so that it is not easy to be broken. However, it is hard to deconnect. 

In order to make this PCB board, I followed the three steps;
Board design ⇢ PCB milling machine ⇢ Soldering.

In this assignment, the main task was to use the PCB milling machines. In the Aalto Fablab, two milling machines can be used. One is an SRM-20 milling machine, the other is an Roland MDX milling machine. The biggest difference between these two machines is regarding the z-axis. SRM-20 milling machine requires the users to define the z-axis by themselves. On the other hand, the MDX milling has an automatic system to calculate the Z-axis.

{{<figure src="./SRM-20.jpeg" caption="[SMR-20 milling machine]">}}
{{<figure src="./MDX-40.jpeg" caption="[MDX-40 milling machine]">}}


&nbsp;
______
### **Make an in-circuit programmer: Create tool paths for the milling machine, mill it and stuff (solder components) on the board**

&nbsp;
#### / STEP 1. Board design /
- **Download the board design file with KiCad**
- **Export GBR file**
- **Edit the board design in CopperCAM**

In this assignment, the pre-made board design was given. But, I had to change some settings according to the detail when I go over to the milling part. 

Firstly, the board design could be downloaded with the GBR file (Gerber format file) from [this Gitlab website (Programmers Summary) ⇱](https://gitlab.fabcloud.org/pub/programmers/programmer-updi-d11c). To process the GBR file, I downloaded the whole files in the Gitlab as a zip file, extracted it and opened KiCad pro project file. After the, I used the Plot function to export the Gerber(GBR) file becuase the GBR file can be edited in CopperCAM and be sent to the milling machine to produce the board with the physical material.

{{< figure src="./board_design-01.jpg" caption="[Download the board design]" >}}
{{< figure src="./board_design-02.jpg" caption="[Open KiCad pro project file]" >}}
{{< figure src="./board_design-03.jpg" caption="[Plot function]" >}}

Next, The actual board design can be seen and edited through the CooperCAM. CooperCAM is CNC milling software for the smaller object. With the CooperCAM, the double side PCB board can be made with multiple layers upto 4 layers. How to see the board design in the CooperCAM can be followed the steps below.

>Open CooperCAM ⇢ Clone layer ⇢ Close the old board ⇢ Go to File / Open / New circuit ⇢ Go to Desktop / find the folder (programmer-udpi-d11c-main) / gerbers ⇢ Select the file <Programmer-UPDI-D11C-F_Cu.gbr>; front Cupper layer for the main connection

{{< figure src="./board_design-04.jpg" caption="[Open the gerber file]" >}}

Finally, the setting for the details can be editted for the board design. Things to check for setting. 
>- **Dimension (Ctrl+D):** To specify the margin around the design if wanted.
>- **Origin point (White cross):** To change the X, Y coordinate. The origin point usaually should be in 0mm, 0mm position along the X, Y axis.
>- **Parameter / Tool library:** To specify what kind of tools are used for the PCB board producing by milling machine.
>- **Parameter / Selected tools (Active tools):** To specify for each jobs (Engraving, Hatching, Cutting, Centering, Drilling). 
>- **Calculate contours (Set contours):** To specify Number of successive contours. If it's done with calculation, the path view and preview can be check by toggling the preview button. Also, the set contours should be processed whenever any setting is changed.
>- **Track apertures:** To edit all identical tracks. Especially, width can be edited to make the track thinner.

{{< figure src="./tool-library.jpeg" caption="[Tool Library Setting]" >}}
{{< figure src="./active-tool.jpeg" caption="[Selected tools (Active tools) Setting]" >}}
{{< figure src="./preview.jpg" caption="[Path view (top) and Preview (bottom)]" >}}
{{< figure src="./board_design-05.jpg" caption="[Set the Track apertures]" >}}

After the setting part is done, the two paths (Engraving, Cutting) can be exported to the milling machine by clicking the Mill option in CooperCAM. I selected only two sections in the sequence because I only need Engraving and Cutting. The each section generates one file for each tool. Furthermore, the speed, XY-zero point, Z-zero point need to be double checked.

&nbsp;
#### / STEP 2. PCB milling machine /

- **Start the VPanel software**
- **Attach the board on the machine**
- **Set the screw with the hex key**
- **Set the X, Y, Z origin**
- **Operate the machine**
- **Remove the board and clear the machine**

I firstly tried to use SRM-20 milling machine. The engraving part should be the first by following the steps below.
1. Check the machine is turned on
2. Start VPanel for SRM-20.The spindle of the machine can be moved by Cursor Step.
{{<figure src="./milling-02.jpg">}}
3. Insert the tool. I only need the orange color tool (0.2-0.5mm) for Engraving.
{{<figure src="./milling-03.jpg">}}
{{<figure src="./milling-03-02.jpg">}}
4. Attach the material by using double sided tape. (I used a single side board.)
{{<figure src="./milling-04.jpg">}}
5. Make sure the area on the board is flat and clean. 
{{<figure src="./milling-05.jpg">}}
6. Set the Z-origin. The gap should be checked in order for the tool not to hit the board. 
{{<figure src="./milling-06.jpg">}}
7. Set X/Y-origin
{{<figure src="./milling-07.jpg">}}
8. Close the lid and start to cut 
{{<figure src="./milling-08.jpg">}}
{{<figure src="./milling-08-02.jpg">}}

After the engraving is done, the cutting part can be started by following the steps below.
1. Clear the area of the board (copper surface) by brush.
2. Remove the orange screw and replace the yellow screw. The tip should touch the surface of the copper surface.
{{<figure src="./milling-09.jpg">}}
3. Close the lid down and select the other tool path file and resume
{{<figure src="./milling-10.jpg">}}

Also, I tried to use MDX-40 milling machine. Because of the main difference from the SMR-20 milling machine, I followed the different way to set the Z-axis by using the sensor component in the machine. Steps below is how to set the Z-axis.
1. After the screw set, put the sensor on the board.
2. Locate the screw above the sensor and start Detection. 
3. After detection, remove the sensor and set the Z-origin as 0.
4. Set the XY-origin and then start to cut. 
{{<figure src="./milling-11.jpg">}}

After the cutting is done, use the hex key to remove the screw, use the vacuum cleaner to remove the dregs on the board and use a knife to remove the board out from the table. 
{{<figure src="./milling-12.jpg">}}

{{<figure src="./boards.JPG" caption="[Finished board making]">}}

&nbsp;
#### / STEP 3. Soldering and test the board/

- **Gathering the component**
- **Clean and check the board with multimeter**
- **Set the soldering station**
- **Solder the PCB board**

When the board is done with milling, the next step is soldering. In order to staring soldering, the components (below) should be gathered. 
>- Microcontroller (ATSAMD11C SOIC-14) x 1
>- Connector (2x3) x 1
>- Connector (2x2) x 1
>- Voltage Regulator (REG Linear 3.3V 100MA SOT23) x 1 
>- Resistor (4.99k Ω) x 2
>- Capacitor (1μF 50V) x 1

{{<figure src="./components.jpeg" caption="[Gathering component. The components of this image are for the two boards]">}}

Start soldering with the steps below.
1. Using Isopropyl Alkohol and tissue, swipe the board.
{{<figure src="./soldering-01.jpeg">}}
2. Check if the board is well produced by using the multimeter. (Making sound means 'connected')
{{<figure src="./soldering-02.jpeg">}}
3. Set the temperature of 280 degree for soldering.
{{<figure src="./soldering-03.jpeg">}}
4. Using a tweezer, put on the components and solder them onto the board. Taping the board on the working table makes the soldering process easier.
{{<figure src="./soldering-04.jpeg">}}
{{<figure src="./soldering-05.jpeg">}}
{{<figure src="./soldering-06.jpeg">}}


&nbsp;
______
### **Test and debug the PCB board with the multimeter**
After the soldering is done, test the board with the multimeter again.

