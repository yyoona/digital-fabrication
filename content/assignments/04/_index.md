---
title: '04 / Computer-Aided Design'
---
______
## Task

Use CAD tools to design a possible final project.

- Explore one or more 2D vector graphics tools.
- Explore one or more 2D raster graphics tools.
- Explore one or more 3D modelling tools.
- Document your process with screenshots and descriptions.
- Create page containing the assignment documentation on your website.

&nbsp;
______
### **Explore the 2D raster graphics tool**
What is the Raseter graphics? In computer graphics and digital photography, a raster graphic is a mechanism that represents a two-dimensional image as a rectangular matrix or grid of square pixels, viewable via a computer display, paper, or other display medium.

Typical softwares for raster based drawing are Adobe Photoshop, GIMP, JavaScript using the HTML5 Canvas, various drawing applications on tablet PC. As a typical file types of the 2D raster graphics are JPEG, PNG, APNG, GIF, WebP, BMP and MPEG4.

#### Tayasui Sketches
I chose Tayasui Sketches to explore 2D raster graphic tool. Tayasui Sketches is a drawing application on iPad with an apple pencil. Compared to the Adobe Photoshop, this sketch application has not many functions. But it is simple, intuitive and more than enough to draw the sketch for the ideation and the planing phase of the project. 

I planned to make the 3D modelling for the roation shaft (the connection part) between the motor and the wooden plate module as the assignment of the Computer-Aided Design. As the plan, I used this 2D raster graphic tool to draw a rough sketch for the 3D modelling test.  

{{< figure src="./earlier-structure.jpg" caption="[The mechanical structure of the previous installation. The red circled part will change into the new design]">}}

{{< figure src="./2d-sketch.jpg" caption="[2D drawing for the new design of the rotation shaft with Tayasui Sketches]">}}

&nbsp;
______
### **Explore the 2D vector grahics tool**
What is the Vector graphics? Vector graphics, as a form of computer graphics, is the set of mechanisms for creating visual images directly from geometric shapes defined on a Cartesian plane, such as points, lines, curves, and polygons. Not as the raster graphics defined by the fixels(bitmap), the vector graphics doesn't have aliasing along the rounded edge (which would result in digital artifacts in a raster graphic). Thus, the color gradients are all smooth, and the user can resize the image infinitely without losing any quality.

Typical softwares for vector based drawing are Adobe Illustrator, InkScape, JavaScript+D3.js, FreeCAD and so on. As a typical file types of the 2D vector graphics are SVG, WMF, EPS, PDF, CDR or AI types of graphic file formats. 

#### Adobe Illustrator
I chose Adobe Illustrator to explore 2D vector graphic tool because of this tool is the most familiar tool for me having a background in graphic design. Also, I used this 2D vector graphic to create the wooden plate part, so that I can directly proceed to the Laser cutting part with this file. 

{{< figure src="./WoodPlate.png" caption="[Detail of a piece of the wooden plate in Illustrator]">}}

{{< figure src="./WoodPlate-LaserCut.png" caption="[I set the artboard size as same as the plywood board's size which I am going to use as the material of the Laser Cut.]">}}

&nbsp;
______
### **Explore the 3D modelling tool**
Digital modeling and fabrication is a design and production process that combines 3D modeling or computing-aided design (CAD) with additive and subtractive manufacturing. Additive manufacturing is also known as 3D printing, while subtractive manufacturing may also be referred to as machining, and many other technologies can be exploited to physically produce the designed objects.

Typical softwares for the 3D modelling are Fusion360, Openscad, FreeCAD, Blender, Antimony, DesignX, and so on.

&nbsp;
#### Fusion360
I have a somewhat specific idea for the 3D modelling object. To concrete the idea in a faster way, I chose Fusion360 for the first exploration of the 3D modelling tool. Because Fusion360 is the only tool that I have used before. I am planning to use another 3D modelling tool afterward.

In order to create the 3D modelling object, I followed the shape from the 2D sketch for the rotation shaft (connection part) of the project installation, A Flipped Plate: shape variations.

&nbsp;
#### / STEP 1: Creating the basic shape /
* Create the New Project and then Create Sketch
* Create the solid geometry shape from the top view 
* Extrude the 2D shape to make the main 3D shape
* Apply the Fillet command for the main shape's top part
* Make a hole on the bottom side of the object to put the motor axis into the shaft (object). 


{{< figure src="./3d-process/01-CreateSketch.png" caption="[Create Sketch on Fusion360]">}}

{{< figure src="./3d-process/02-CreateCircle.png" caption="[Creating a circle]">}}

{{< figure src="./3d-process/03-ExtrudeMain.png" caption="[Extrude main part]">}}

{{< figure src="./3d-process/04-FilletMain.png" caption="[Fillet for the main part]">}}

&nbsp;
#### / STEP 2: Making the detail shapes /
* Make holes with creating the 2D shape first on the surface and using Extrude command
* Extrude two surface sides of the main object to make the flattened parts 
* Create two holes on the flattened parts
* Apply the Fillet command for the detail curves

After creating the main shape, more detail shapes can be made based on the main shape. The Four Holes of the motor connection part(bottom side of the object) are same diameter and symmetically arranged. To make these holes eaiser, I created the circle shapes in the middle of the main object, and then, extruded with the specific options; Two Sides for Direction, All for Extent Type. 

Next, I made the 3mm Gap in the middle of the main object. The gap is to put the flattened plate, attached to the wooden plate. The other side part from the Gap is the flattened surface sides of the main object. On these surfaces, the two holes are the part of the the connector that is the component between the shaft and the wooden plate. 

In order to locate the proper spot for the two holes on the flattened sides, I measure the dimension of the object with the construction lines. The lines can be created as Sketches. Also, depending on the change of the dimension, the holes can be automatically moved.

To make the detailed curves for the object after making the holes, I used the Fillet command. Especially for the inner part's holes, this process is essential for the object to be processd with the milling machine. 

{{< figure src="./3d-process/05-HoleMotor.png" caption="[Creating holes for connecting motor]">}}

{{< figure src="./3d-process/06-InnerGap1.png" caption="[Making the rectangular first to make the gap in the middle of the main object]">}}

{{< figure src="./3d-process/07-InnerGap2.png" caption="[The rectangular is the base of the extrusion toward the two sides]">}}

{{< figure src="./3d-process/08-FlattenSides.jpg" caption="[Making the flattened sides]">}}

{{< figure src="./3d-process/09-HoleConnector.png" caption="[Creating the two holes as the circle sketches and find the proper location for the circles with the construction lines]">}}

{{< figure src="./3d-process/10-HoleConnectorExtrude.png" caption="[Using the Extrude command for the two holes]">}}

{{< figure src="./3d-process/11-BeforeAllFillet.png" caption="[Two holes on the two sides]">}}

{{< figure src="./3d-process/12-Fillet.jpg" caption="[Applying Fillet on the curves of the 3D object]">}}

&nbsp;
#### / STEP 3: Finishing /
* Naming for the Sketches
* Editing Parameters (Modify -> Change Parameters)
* Apply Materials (Modify -> Physical Material)

After the Fillet, this 3D object is almost done. But I wanted to make a small change for the two holes' diameter where the bolt and nuts will be put for connecting this object to the wooden plate. In order to do that, first, I changed all the names of the Sketches specifically. Naming is the effective process to edit the parameters as well. Because the changed name is reflected in the Parameters. 

In order to edit parameters, find the Change Parameters command in MODIFY. After then, find the name of the components and change the Expression. 

Finally, the 3d object is done with the shape. To go a bit further, I applied the wooden materials to the shape with the Physical Material command, which is also in MODIFY.

{{< figure src="./3d-process/13-FinishView1.png" caption="[Finished perspective view]">}}

{{< figure src="./3d-process/14-FinishView.jpg" caption="[Finished view - front, side, bottom, top (clockwise direction from the top-left)]">}}

{{< figure src="./3d-process/15-Name.png" caption="[Change the Sketches' name]">}}

{{< figure src="./3d-process/16-Parameter.jpg" caption="[Adjust parameters]">}}

{{< figure src="./3d-process/17-Materials.jpg" caption="[Apply the materials to the finished shape]">}}

&nbsp;
______
### **Original Design Files**
- PNG file of the 2D raster graphic for the shaft design  → [2D_raster_graphic.png](2D_raster_graphic.png) 
- Illustrator (ai) file of the 2D vector graphic for wooden plate design  → [WoodPlate.ai](WoodPlate.ai) 
- Autodesk Fusion 360 archive file of 3D modelling for the shaft  → [shaft.f3d](./files/shaft.f3d)

&nbsp;
______
### **References**

Wikipedia for the definition regarding the Raster graphics, Vector graphics and 3D modelling → 
- https://en.wikipedia.org/wiki/Raster_graphics
- https://en.wikipedia.org/wiki/Vector_graphics
- https://en.wikipedia.org/wiki/Digital_modeling_and_fabrication
