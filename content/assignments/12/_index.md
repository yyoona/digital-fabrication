---
title: '12 / Output Devices'
---
______
## Task

Follow the steps below to complete your assignment.

* Add an output device to a microcontroller board you've designed, and program it to do something.
* Measure the power consumption of an output device.
* Include a hero shot and source files of your board in your documentation.


&nbsp;
______
## Assignment 12 / Output Devices
{{<figure src="./hero-shot.jpeg" caption="[Finished board making (second designed board)]" >}}

&nbsp;
______

### **Add an output device to a microcontroller board**

&nbsp;
### / Information about BJTs, MOSFET, and StepStick /

I chose PowerLED and DC motor as the output devices to add to the microcontroller board.
Basically, I use the HelloTiny412 microcontroller board. Also, there is an especially important additional component that is MOSFET and Motor Driver. These are needed to control the output devices on the board.

As background knowledge, I first learned about the transistor, specifically BJTs, and MOSFETs through the introduced youtube video, [MOSFETs and Transistors with Arduino](https://www.youtube.com/watch?v=IG5vw6P9iY4). Below information is what I noted while I was watching the video. 

**What is Transistor?**
* The First transistor developed at Bell Labs in Murray Hill NJ in 1947.
* 1956 Nobel prize to John Bardeen, Walter Brattain, and William Shockley.
* Replaced vacuum tubes in electronics devices.
* Integrated circuits like microprocessors use transistors.
* Can be used as amplifier or switches.

**What is BJTs?**
* Bipolar Junction Transistor (or Bipolar Transistor / Transistor)
* Two main types: NPN & PNP
* Three leads: Base, Emitter & Collector
{{<figure src="./BTJs.jpg" caption="[Bipolar Junction Transistor]" >}}

**What is MOSFET?**
* Metal-Oxide-Semiconductor-Field-Effect Transistor
* Two main types: N-Channel & P-Channel
* Three Leads: Gate, Drain & Source
{{<figure src="./MOSFET.jpg" caption="[Metal-Oxide-Semiconductor-Field-Effect Transistor]" >}}
* N-Channel MOSFET
    - MOSFETs are Voltage Driven
    - Low Voltage applied to Gate
    - Low Resistance between Drain and Source
    {{<figure src="./NChannelMosftet.jpg" caption="[MOSFETs are Voltage Driven]" >}}

**About the StepStick?**

In addition, I watched the video when it comes to using a StepStick (a motor driver chip breakout board) with a bipolar stepper motor. In this youtube video, [How To Control a Stepper Motor with A4988 Driver and Arduino](https://www.youtube.com/watch?v=5CmjB4WF5XA), The A988 is used. The A988 is the microchipping driver for controlling bipolar stepper motors, which is a built-in translator for easy operation. This means that we can control the stepper motor with just two pins from our controller, or one for controlling the rotation direction and the other for controlling the steps. Aalto Fablab has the Trinamic Silent StepStick (Trinamic 2208 StepStick). 

Before connecting the motor, we should adjust the current limit of the drivers so that we are sure that the current currently meets the motor. We can do that by adjusting the reference voltage using the potentiometer on the board and considering this equation.
 
    Current Limit = VRef (reference voltage) x 2

&nbsp;
### / Board design /

**Start designing the board with KiCad Schematic**

1. In order to start designing the board, open up the schematic of the KiCAD.
2. Add a symbol for `HelloTiny412`.
3. Add symbol for `MOSFET_N-CH_30V_1.7A` (smaller one).
4. Connect PA3 to the gate.
    - The main concept is to use the pin (PA1, PA2, PA3) in order to control the gates.
5. Add something that I want to drive (something would be flexible).
    - Provide a connector for the LED strip.
    - Add Screw_Terminal.
        - `Screw_Terminal_01x02_P3.5mm`: 1 row, 2 columns, 3.5mm pitch (pitch: space in between the coarse) -> we choose this
        - Screw_Terminal_01x02_P5mm: when connect to something big
        - Screw_Terminal_01x03_P3.5mm: 3 positions  
6. Attach the Screw_Termial pin 2 to the Drain.
7. The source goes to the GND.
8. Set up something for the microcontroller.
    - Add a `Bypass capacitor`.
    - The high part goes to the 5V and the other part goes to the GND.
    - Start adding GND and 5V. also copy the GND to the MOSFET part.
    - Edit the value of the capacitor to 1uF.
    - Microcontroller GND also goes to the GND, so copy & paste the GND.
9. **[STEP.1]** We need to provide input voltage (a power input). Generally, we want to use higher voltage for driving the LED strip or DC motor. So we need a higher voltage coming in and a voltage regulator that converts voltage to the chip that the microcontroller can use.
    - Add `power jack`.
        - 2 kinds. A bigger one is more common and convenient. But LED strip or DC motor is okay with the smaller one. So, we choose a smaller one.
    - Connect 1 to the HIGH_VOLTAGE.
    - Connect 3 and 2 to the GND.
    - Add `Power Flag` for the connections.
10. **[STEP.2]** Scale the input voltage down to 5V so that we can use the microcontroller. We need something that provides a 5V; regulator (five kinds in Fablab).
    - `Regulator_Linear_LM3480-3.3V-100mA`: 3.3V. can go up to 100mA (current).
    - If we use little MOSFET, then LM3480 is enough. If we use H-bridge, we need NCP1117 (a bigger regulator). / we choose 5V/100mA.
    - The regulator has 3 pins.
        - Voltage input: connect to the HIGH_VOLTAGE. Any supply input voltage is generally a higher voltage.
        - Voltage output: it will generate an output voltage. voltage according to the specific regulator. so what we have is 5V output.
        - GND
11. **[STEP.3]** We use the microcontroller to control the gate so the device we connect through the screw terminal. 
    - The device actually gets a higher voltage. Thus, add unregulated HIGH_VOLTAGE.
    - High voltage is going to be flowing through the `transistor` and through the device into the GND. So it’s not going to affect the microcontroller. The microcontroller gets the power from the voltage regulator.
12. We need to program the chip. So add the `UPDI programmer`.
    - Need a positive voltage; 5V  / GND / UPDI.
    - Use the UPDI pin of the microcontroller.
13. We could have a button and powerLED. `PowerLED` is always connected to the 5V. 
    - So add PowerLED and Resistor (Don’t care how bright the PowerLED is going to be, so use a `1k resistor`. a bit less bright).
    - Edit the value for them as 1K, PWR_LED.
    - Connect the 5V and GND.
14. For the rest parts, just use <X> to define its no connection.
15. Annotate first (J? changes to J3).
16. Run the Electrical Rules checker.
    - Edit the capacitor part and run the rules checker again until no error.

{{<figure src="./KicadImage.png" caption="[KiCad Schematic for the Mosfet & FullBridge board]" >}}

**Move on to the PCB Editor**

After the schematic process is done, move on the PCB Editor and import the components from the schematic. (✧ ✧ ✧ Some Tip! if you click on the component in the schematic, hit E. Then, there is a link to the datasheet.)
* Set the proper grid.
* Thing to check for the layout.
    * MOSFET should be close to the output device.
    * PowerLED should be close to the regulator.
    * The regulator itself should be close to the input power.
    * DC motor: MotorDriver_FullBridge_A4953
    * ATtiny 412
        * It connects the motor to the out 2 and out 1 pin.
        * IN2 and IN1 control the direction of the motor:
            * IN1 is low and IN2 is high: spin clockwise.
            * IN2 is high and IN1 is low: spin counter-clockwise.
            * If connect these IN1 and IN2 to the PWM Pin, we have PA7 and PA1.
    * VREF - Power 
    * VBB - High Voltage
    * LSS - sends Resistor
        * In order to change the speed, this pin is quit in a special way.
        * Check the datasheet (application information below).
        {{<figure src="./Datasheet.jpg" caption="[PWM Control Truth Table]" >}}
        * Resistor is an important component (If one direction and no speed changes, we don’t need a sensor resistor(?)).
            * If you don’t have a Resistor in between a GND and LSS pin, we can not use the PWM to control the motor speed.
            * Value 0.25 ohm is the special Resistor in stock. we choose a bigger Resistor R_2010.
            * VBB asking for the two capacitors.
                * Filter current, booster current.
                * Recommend using a polarized capacitor for simplicity.
                * Number 1: 0.1 instead of 100 polarized, we can go with 10 microfarads unpolarized.
            {{<figure src="./A4952.jpg" caption="[Information for the A4952]" >}}
    * GND - PAD (PAD is a special feature of this Full_Bridge)
* Annotate & rule check 
* Move onto PCB editor and update; check to Delete footprints with no symbols.
* Arrange
    * When crossing over each other, go to schematic, and change the component position.
    * Make the component closer to each other. 
        * Go to Pre-defined Sizes.
            * Edit Net Classes > Clearance: 0.41mm / Track Width: 0.4mm 
            * Add Net Classes for Power > Track Width: 0.6mm
            * Assign the Power to HIGH_VOLTAGE / GND
    * Capacitors are important so that High voltage is entering the chip. Filter that out.
    * Place all the connectors where the motor is set up.
    * Big resistor footprint to connect to the motor and the GND.
    * Align for the Screw Terminals.
    * MOSFET should not close to others.
    * Make the board as small as possible.
* Last editing
    * Adjust Hole specification.
* Export
    * 1st Plot without Plot Edge.Cuts on all layers.
    * 2nd Plot with Plot Edge.Cuts.
    * Generate the Drill Files...

{{<figure src="./OldBoard-01.png" caption="[First designed board in PCB Editor]">}}

{{<figure src="./NewBoard-01.png" caption="[Second designed board in PCB Editor]">}}

{{<figure src="./FinalBoard.png" caption="[Final board)]">}}

I changed the board design three times. The first design has so many rivet holes. In the end, the connection through the hole is easy to make shorts. Thus, I tried to have fewer holes through the arrangement of the components and the tracks in other ways not as in the first design. 

However, I realized that I need to modify the board design again in order to put the Power Flag all the way into the Power jack. Because the margin of the second designed board makes the Power Flag not reach the end of the Power jack. That made the connection weak so the output device was not working properly.


&nbsp;
### / Mill the board /
**Make the gerber files through CooperCam**
1. Import
2. Set the Pad aperture for the hole parts.
{{<figure src="./mill-01.jpg">}}
3. Set card counter for the cutting outline.
{{<figure src="./mill-02.jpg">}}
4. Align the back layer to the front layer.
{{<figure src="./mill-03.jpg">}}
5. After align done, go to the first layer and set the dimension.
6. Check the tools
{{<figure src="./mill-08.jpg">}}
7. Set contours
{{<figure src="./mill-09.jpg">}}
8. Text: Engrave tracks as centerlines and delete the unnecessary pad around the texts. After the text, set the contour again.
{{<figure src="./mill-10.jpg">}}
9. Set the mill (Front / Back)
{{<figure src="./mill-11.jpg">}}
10. Start milling the board by operating the milling machine with VCarve. 
{{<figure src="./mill-13.jpg">}}

&nbsp;
### / Solder the components to board /

- Do the process to put the rivets on the board.
- Get all the components and solder them on the board.
    - MotorDriver_FullBridge_A4953
        - FullBridge A4953
        - 0.22nF (thinner)
        - Capacitor 100 nF  cp elec 10x10
        - Screw Terminal 01x02
        - Resister: R_2010

    - Microcontroller_ATtiny412
        - Capacitor 1uF
        - MOSFET_N-CH_30V_1.7A
        - ATtiny412
        - Screw Terminal_01x02
    - UPDI : PinHeader_UPDI_2x03
    - Regulator: Regulator_Linear_LM3480-5.0V-100mA
        - Regulator 5v 100ma
        - Resistor: 1K
        - PowerLED
    - Power Supply: PWRJack_0.7x2.35mm
        - PWR jack
        - PWR FLAG

{{<figure src="./Soldering-final.jpeg" caption="[First designed board]">}}
{{<figure src="./NewBoard-02.jpg" caption="[Second designed board]">}}
{{<figure src="./NewBoard-03.jpg" caption="[Final designed board]">}}

&nbsp;
______
### **Program the board**

&nbsp;
### / Program the Mosfet & FullBridge board to make motors spinning /

{{< youtube id="Sno4vqwpMY8" title="Programming Mosfet & FullBridge board" >}}
[Programming Mosfet & FullBridge board]

I tried to program the Mosfet & FullBridge board focusing on the motor. Because the board I made has only 3 pins available of the UPDI PinHeader and it has no connection between the LED and the TX/RX. (problem from Kicad schematic?). However, I could see the LED is on when I connected the power to the board. 

On my board, the two motors are available to be connected as the output device. In the KiCad schematic, the motors are J3 and J4. These motors can be programmed with both analog data and digital data, according to the connection between the motor and the ATtiny412 microcontroller. Thus, this board basically can be programmed following the Pin number of the [ATtiny 412](https://yyoona.gitlab.io/digital-fabrication/assignments/08/ATtiny%20412.png).

I used the code (below) for programming motors to spin through the Mosfet & FullBridge board. First, The UPDI programmer absolutely is needed to program the Mosfet & FullBridge board. After then, I can provide the power to the board with the Benchtop power supplier. The laptop doesn't provide the full voltage for the two motors to activate, so I directly used the power supplier after programming and set the voltage up to 12V because of the motor specification (Standard Motor 6600 RPM 12V)

During the several tries programming the board, I found that only the analog part (J4) was active, but the digital part (J3) didn't work at all. 

Also, I tried to give the different voltages to the board. When I provide the full voltage (12V), I just could see the motor keep spinning at the full speed. Meanwhile, when I decrease the voltage, I could hear the motor sounds are different by the spinning speed spinning. However, I haven't seen the motor stops even though I was supposed to stop the motor from spinning by the code. 

&nbsp;
### / Arduino code for the programming the Mosfet & FullBridge board /
~~~

const int Motor_mosfet = 4; // ATtiny pin 7 (~ PWM)

const int Motor_Pin1 = 1; // ATtiny pin 3 (~PWM)
const int Motor_Pin2 = 2; // ATtiny pin 4 (~PWM)

void setup()
{
  Serial.begin(9600);
  pinMode(Motor_mosfet, OUTPUT); // To change the speed of the motor J4
  pinMode(Motor_Pin1, OUTPUT); // To change the spinning direction of the motor J3
  pinMode(Motor_Pin2, OUTPUT); // To change the spinning direction of the motor J3
}

void loop()
{
  // Motor J4 spins at the full speed
  analogWrite(Motor_mosfet, 255);

  // Motor J3 Spins counter-clockwise
  digitalWrite(Motor_Pin1, HIGH); 
  digitalWrite(Motor_Pin2, LOW);
  
  delay(2000);

  // Motor J4 spins at the half speed 
  analogWrite(Motor_mosfet, 100);

  // Motor J3 Spins clockwise
  digitalWrite(Motor_Pin1, LOW);
  digitalWrite(Motor_Pin2, HIGH);
  
  delay(2000);

  // Motor J4 stop spinning
  analogWrite(Motor_mosfet, 0);

  // Motor J3 stop spinning
  digitalWrite(Motor_Pin1, LOW);
  digitalWrite(Motor_Pin2, LOW); 
   
  Serial.println("motor on");
  delay(2000);
}


~~~

&nbsp;
______
### **Measure the power consumption of an output device**

### / Measuring devices / 

**DC Power Supply (Benchtop power supply)**
* Providing specific voltage and current to device for choice
* Compositions
    * 2 little screens
    * Some knobs 
    * Power button
    * Connections for the test for the cable that we can use to connect to our devices
        * Red connected to the (+) positive of the terminal
        * Black connected to the (-) negative of the terminal

{{<figure src="./measure-01.png" caption="[DC Power Supply]" >}}

* Grabbing connectors
    * Push the button then open up the grabbing element and release for grabbing
    * A reliable way to grab the cable quickly for measurement purposes
    {{<figure src="./measure-02.png" caption="[Grabbing connectors]" >}}

* Powering the supply up by pressing the power button (orange button)

* Left is the current side.
    * Current is measured with amperes; how many amperes the device is pulling.
    * 0; nothing in the current indicator because nothing is connected.
    * We can sign a limit to the amount of ampere. This power supply is able to supply.

* Right is the voltage side.
    * 12.2 V
    * Knob under the right one: COARSE / left one: FINE
        * Coarse: for coarse adjustment
            * tune the voltage to higher and lower values with a big step
            * approximately tune
        * Fine: fine adjustment

**Multimeter**
* Validating the measurement (Benchtop power supply)
{{<figure src="./measure-03.png" caption="[Multimeter]" >}}

**Two Output Devices**
* DC motor
    * DC motor 12V 50rpm +- 12percent Procedure
    * Gear ratio 1:90 
        * Actual DC motors spins are faster.
        * There’s a DC motor inside and there’s a gear system that makes it spins slower, but it has 90 times more tokens (actual DC motor inside here).
{{<figure src="./measure-04.png" caption="[DC motor]" >}}
* LED strip
    * 5V
    * measure how much current it is pulling
{{<figure src="./measure-05.png" caption="[LED strip]" >}}

### / How to sign the limit to the amount of amperes / 
1. Turn off the power.
2. Take the test leads and connect them together.
    - Should do this to adjust the current limit that this power supply can supply.
3. Turn the power on.
4. Then see the voltage dropped to 0 / current jumps up to 2. 
    - The right to coarse adjustment
    - The left to fine adjustment
    - If we know that our device has the limit of the current that it can support, we don’t want to kill it, this is the way how we can set the limit.
5. Going up to 1.90 (2 ampere is quite a lot)
    - Coarse -> Fine adjustment 
6. Now the maximum limit of the current of the amperes is 1.9 amps.
7. Turn off and disconnect the yellow.

### / How much current the DC motor is pulling individually / 
1. Connect the cables to the motor.
    - Make sure that the two connections are not too close to each other. In this case, here we don’t want to show the circuit. We want the positive to go to the motor and the negative to go separately.
    {{<figure src="./measure-06.png"  >}}
    {{<figure src="./measure-07.png"  >}}
2. Switch on the power supply.
    - Then the motor is spinning.
    - Look back to the power supply and write down the values on the screen.
        - The voltage is 12V.
        - Current measurement is 0.03 amps: measurement of the power supply.
    {{<figure src="./measure-08.png" >}}
3. Switch off the power supply.
    {{<figure src="./measure-09.png" caption="[Note for the measured value]" >}}

### / Another way to measure the current: Using the multimeter / 
1. Use it to measure voltage and detect the short circuit for continuity testing.
2. In case to measure the voltage, the black is always connected to COM, and the red one is connected to the application-specific.
    - Use the third connector to measure the voltage, resistances, and direction of the current, and capacitor.
    - In order to measure the amperes, use the left two connectors.
        - If you are looking for a small current, use the micro-ampere, milli-ampere jack (2nd connector).
        - Larger units, then use the ampere measurement jack (1st connector).
    {{<figure src="./measure-10.png" >}}
3. Use the test clips (yellow).
    - For measuring current with the multimeter, it’s important to connect the multimeter in between the power supply and the device we are driving. 
4. Disconnect the motor from the power supply test leads.
5. Connect the negative (black) test lead to the ground of the motor.
6. Connect the positive (red) test lead of the power supply to the test lead of the multimeter.
7. Multimeter is going to be a sort of bridge between the power supply and the motor.
8. Connect the negative side of the multimeter to the positive side of the motor → it’s going to be in between the circuit.
    - Basically the current flows through the multimeter, and the multimeter is able to measure it and it can flow forward to the DC motor.
9. Set the multimeter’s mode of the amperes and specify the DC by the yellow button.
10. Turn on the power supply.
    - Motor is spinning.
    - We can see the 0.03 amps on the screen, it’s the same value on the multimeter.
    - In the multimeter, the value changes 0.029 so we can conclude is that we can get a bit more precise measurement with the multimeter.
11. Add MM: 0.029 to the note
{{<figure src="./measure-11.png" >}}
{{<figure src="./measure-12.png" >}}
{{<figure src="./measure-13.png" >}}
{{<figure src="./measure-14.png" >}}

### / Measure the current of the LED strip in a similar way / 
** LED strip is drivable with the 12V.
1. We should know about the values (Voltage and the limit of the current) of the power supply (Voltage and the limit of the current) beforehand depending on the setting; The power supply has 12 V as a setting and the limit of the current is 1.9 amps.
2. Connect the negative test clip to the negative wire connection of the strip.
3. Connect the positive test clip of the power supply to the positive multimeter.
4. Connect the negative test clip of the multimeter to the positive test clip of the LED.
5. Note that LED strip power supply multimeter to compare both of those.
6. Switch on the multimeter and turn on the power supply.
    - Set it to the DC voltage and DC current ampere setting.
    - 1.54 ampere on the power supply / 1.558 on multimeter.
    - Interesting thing: LED is heating up so they are starting to draw more current.
    - Little offset between what the power supply shows and what the multimeter shows.
** We can limit the voltage of the power supply and see how the measurements are different.
7. Fine-tune to 1.50.
8. Turn off the power supply.
9. Connect as before (through the multimeter).
10. 1.5 amps that we are giving with the power supply.
11. Getting 1.519 amps on the multimeter (Approximately same. wondering which one is more precise among the multimeter and the benchtop power supply?).
{{<figure src="./measure-15.png" >}}
{{<figure src="./measure-16.png" >}}

### / Measure with the device driven by the circuit /
Prepare two circuits (DC motor driving circuit / LED board; developed from the Hello board).

**Motor board**
1. Connect the motor.
    - The direction of the motor spin is not the matter.
    - Only matter if you need to spin in a certain direction (if you flip the cable and flip the direction of the motor).
    {{<figure src="./measure-17.png" >}}
    {{<figure src="./measure-18.png" >}}
2. Connect to the power supply (yellow).
    - Connect the red to the red / black to the black.
    {{<figure src="./measure-19.png" >}}
3. We know that the power supply set the 12V. This is the voltage required to drive the board and also to drive the motor.
4. Look closer at what the board consists of.
    - Power port which is going to supply the board 12V.
    - Voltage regulator: downscale the voltage from 12 to 5V
        - So that the microcontroller (ATtiny412) can survive.
        - Why do we use the big voltage regulator?: The ATtiny needs enough current in order to trigger to control pins of the motor driver.
        - Provide a relatively clean separation between the voltage and the current needed in order to drive the logic or the microcontroller of the board.
    - Microcontroller (ATtiny412): 
    - Motor driver chip 
        - Two lines going from the microcontroller to the motor driver. 
            - The two lines are used to control the direction and the speed of the motor through the motor driver.
            - In order for the motor driver does, it connects the unregulated voltage(12V) coming from the input to a specific voltage, and the current that is being supplied to the motor through the connect through the here.
    - Trimmer (variable resistor)
        - In order to control the speed of the motor.
        - Default: at full speed
    - Button 
        - In order to change the direction.
        - default: spinning clockwise
5. Turn off the power supply with the orange button.
    - Turning clockwise: consuming 0.04 voltage
    {{<figure src="./measure-20.png" >}}
6. If tune down the speed to 0, stop spinning and the amperes dropped down to 0.01.
    - We can assume that the board alone without the motor, 0.01 amperes.
    - With the motor spinning, 0,04 consuming.
    {{<figure src="./measure-21.png" >}}
    {{<figure src="./measure-22.png" >}}
** Try to change the direction of the motor.

**LED board**
1. Connect the power supply to the LED board (the board is designed to support 9V).
2. Before switching on the power supply, disconnect one of the test leads and set the voltage on the power supply to the 9V.
3. Switch off the power supply.
    {{<figure src="./measure-23.png" >}}
    {{<figure src="./measure-24.png" >}}
    {{<figure src="./measure-25.png" >}}
4. Connect the test red clip to the positive of the connector of the board.
5. Before switching on, look closer at what there is on the board.
    - Components:
        - Power connector
        - ATtiny412 chip
        - PowerLED
        - Bypass capacitor
        - Current limiting resistor
        - Button: to trigger the transistor
    - From the main current of the supply, 9V current travel to the voltage regulator. So we use the small regulator because we do not need a lot of current to drive the ATtiny and ATtiny doesn’t need to do anything specific. So regulate Voltage to 5V through the regulator.
    - As we press the button, we supply the voltage to the pin which is connected to the gate of the transistor (Mosfet). 
    - Mosfet is connected to the LED network, and the source of the Mosfet is connected to the ground (the drains of the source of the transistor are connected together as soon as we supply voltage to the ground pin).
6. Turn on the power supply.
    - The board alone is pulling 0.01 amperes (same as the measurement).
7. Look at the board.
    - Press the button.
        - LED is turned on (LED is bright. Heating up quite quickly → more current).
        - Current measurement on the power supply change to 0.50 - 0.60 (rising because of heating up; heating up is pulling more current)
8. Should limit the voltage to the ampere (0.5 amps).
    - Extra consideration about connecting ground parts of the LED to the ground plain and attaching heat things (FR1, FR4 manufactured board) to them.
    {{<figure src="./measure-26.png" >}}
    {{<figure src="./measure-27.png" >}}

**Use this formula to calculate power**

    P = VI
    * P: Power in Watts
    * V: Voltage in Volts
    * I: Current in Amperes

### / Measure the output devices of the Mosfet & FullBridge board /

For the Mosfet & FullBridge board, I used the same two DC motors. To check the current and power of the output device that I used for the board, I used the benchtop power supplier. 
In advance, when I measure the DC motor's current itself, I used the multimeter and the power supplier at the same time. 

* Measure the DC motor itself without the board
    - Current: 0.10 Amps on the power supplier screen.
    - Current: 0.101 Amps on the multimeter screen.

* Measure for the board (supplied by 12V)
    - When connecting two DC motors to the board (LED is on)
        - Current: 0.31 ~ 0.37 Amps 
    - Without the motors on the board (only LED is on)
        - Current 0.08 ~ 0.02 Amps

* Power consumpotion of the two DC motors on the board 
    - P = 12 V x (0.37-0.08) Amps = **3.48 Watts**



&nbsp;
______
### **Source files**
- Board Design KiCad files (Final version) → [FinalVersion.zip](./files/FinalVersion.zip)


