Last login: Tue Feb  1 22:40:47 on ttys000
yoonayang@YOONAs-MacBook-Pro ~ % cd DF2022 
yoonayang@YOONAs-MacBook-Pro DF2022 % ls
df-test			digital-fabrication00
yoonayang@YOONAs-MacBook-Pro DF2022 % git config --global user.name "yyoona"
yoonayang@YOONAs-MacBook-Pro DF2022 % git config --global user.email "yoona.yang@aalto.fi"
yoonayang@YOONAs-MacBook-Pro DF2022 % git clone git@gitlab.com:yyoona/digital-fabrication.git
'digital-fabrication'에 복제합니다...
Enter passphrase for key '/Users/yoonayang/.ssh/id_ed25519': 
remote: Enumerating objects: 15, done.
remote: Counting objects: 100% (15/15), done.
remote: Compressing objects: 100% (10/10), done.
remote: Total 15 (delta 3), reused 0 (delta 0), pack-reused 0
오브젝트를 받는 중: 100% (15/15), 완료.
델타를 알아내는 중: 100% (3/3), 완료.
yoonayang@YOONAs-MacBook-Pro DF2022 % ls
df-test			digital-fabrication	digital-fabrication00
yoonayang@YOONAs-MacBook-Pro DF2022 % cd digital-fabrication
yoonayang@YOONAs-MacBook-Pro digital-fabrication % ls 
README.md
yoonayang@YOONAs-MacBook-Pro digital-fabrication % ls
README.md
yoonayang@YOONAs-MacBook-Pro digital-fabrication % git pull
Enter passphrase for key '/Users/yoonayang/.ssh/id_ed25519': 
remote: Enumerating objects: 3, done.
remote: Counting objects: 100% (3/3), done.
remote: Total 2 (delta 0), reused 0 (delta 0), pack-reused 0
오브젝트 묶음 푸는 중: 100% (2/2), 완료.
gitlab.com:yyoona/digital-fabrication URL에서
   992dc61..06c75e5  main       -> origin/main
업데이트 중 992dc61..06c75e5
Fast-forward
 README.md | 5 -----
 1 file changed, 5 deletions(-)
 delete mode 100644 README.md
yoonayang@YOONAs-MacBook-Pro digital-fabrication % git add .
yoonayang@YOONAs-MacBook-Pro digital-fabrication % git commit -m 'delete readme'
현재 브랜치 main
브랜치가 'origin/main'에 맞게 업데이트된 상태입니다.

커밋할 사항 없음, 작업 폴더 깨끗함
yoonayang@YOONAs-MacBook-Pro digital-fabrication % ls
yoonayang@YOONAs-MacBook-Pro digital-fabrication % touch README.md
yoonayang@YOONAs-MacBook-Pro digital-fabrication % git commit -m "add README"
현재 브랜치 main
브랜치가 'origin/main'에 맞게 업데이트된 상태입니다.

추적하지 않는 파일:
	README.md

커밋할 사항을 추가하지 않았지만 추적하지 않는 파일이 있습니다
yoonayang@YOONAs-MacBook-Pro digital-fabrication % git push -u origin main
Enter passphrase for key '/Users/yoonayang/.ssh/id_ed25519': 
'main' 브랜치가 리모트의 'main' 브랜치를 ('origin'에서) 따라가도록 설정되었습니다.
Everything up-to-date
yoonayang@YOONAs-MacBook-Pro digital-fabrication % git status
현재 브랜치 main
브랜치가 'origin/main'에 맞게 업데이트된 상태입니다.

추적하지 않는 파일:
  (커밋할 사항에 포함하려면 "git add <파일>..."을 사용하십시오)

	README.md

커밋할 사항을 추가하지 않았지만 추적하지 않는 파일이 있습니다 (추적하려면 "git
add"를 사용하십시오)
yoonayang@YOONAs-MacBook-Pro digital-fabrication % git add . 
yoonayang@YOONAs-MacBook-Pro digital-fabrication % git commit -m "add readme"
[main 7ca5620] add readme
 1 file changed, 0 insertions(+), 0 deletions(-)
 create mode 100644 README.md
yoonayang@YOONAs-MacBook-Pro digital-fabrication % git push
Enter passphrase for key '/Users/yoonayang/.ssh/id_ed25519': 
오브젝트 나열하는 중: 4, 완료.
오브젝트 개수 세는 중: 100% (4/4), 완료.
오브젝트 쓰는 중: 100% (3/3), 237 bytes | 237.00 KiB/s, 완료.
Total 3 (delta 0), reused 0 (delta 0)
To gitlab.com:yyoona/digital-fabrication.git
   06c75e5..7ca5620  main -> main
yoonayang@YOONAs-MacBook-Pro digital-fabrication % code .
yoonayang@YOONAs-MacBook-Pro digital-fabrication % git status
현재 브랜치 main
브랜치가 'origin/main'보다 1개 커밋만큼 앞에 있습니다.
  (로컬에 있는 커밋을 제출하려면 "git push"를 사용하십시오)

커밋할 사항 없음, 작업 폴더 깨끗함
yoonayang@YOONAs-MacBook-Pro digital-fabrication % git push
Enter passphrase for key '/Users/yoonayang/.ssh/id_ed25519': 
오브젝트 나열하는 중: 5, 완료.
오브젝트 개수 세는 중: 100% (5/5), 완료.
Delta compression using up to 12 threads
오브젝트 압축하는 중: 100% (2/2), 완료.
오브젝트 쓰는 중: 100% (3/3), 529 bytes | 529.00 KiB/s, 완료.
Total 3 (delta 0), reused 0 (delta 0)
To gitlab.com:yyoona/digital-fabrication.git
   7ca5620..d971349  main -> main
yoonayang@YOONAs-MacBook-Pro digital-fabrication % git status
현재 브랜치 main
브랜치가 'origin/main'보다 1개 커밋만큼 앞에 있습니다.
  (로컬에 있는 커밋을 제출하려면 "git push"를 사용하십시오)

커밋할 사항 없음, 작업 폴더 깨끗함
yoonayang@YOONAs-MacBook-Pro digital-fabrication % git push
Enter passphrase for key '/Users/yoonayang/.ssh/id_ed25519': 
오브젝트 나열하는 중: 5, 완료.
오브젝트 개수 세는 중: 100% (5/5), 완료.
Delta compression using up to 12 threads
오브젝트 압축하는 중: 100% (2/2), 완료.
오브젝트 쓰는 중: 100% (3/3), 297 bytes | 297.00 KiB/s, 완료.
Total 3 (delta 1), reused 0 (delta 0)
To gitlab.com:yyoona/digital-fabrication.git
   d971349..e491e53  main -> main
yoonayang@YOONAs-MacBook-Pro digital-fabrication % hugo version
hugo v0.92.1-85E2E862+extended darwin/amd64 BuildDate=2022-01-27T11:44:41Z VendorInfo=gohugoio
yoonayang@YOONAs-MacBook-Pro digital-fabrication % hugo new site digital-fabrication
Congratulations! Your new Hugo site is created in /Users/yoonayang/DF2022/digital-fabrication/digital-fabrication.

Just a few more steps and you're ready to go:

1. Download a theme into the same-named folder.
   Choose a theme from https://themes.gohugo.io/ or
   create your own with the "hugo new theme <THEMENAME>" command.
2. Perhaps you want to add some content. You can add single files
   with "hugo new <SECTIONNAME>/<FILENAME>.<FORMAT>".
3. Start the built-in live server via "hugo server".

Visit https://gohugo.io/ for quickstart guide and full documentation.
yoonayang@YOONAs-MacBook-Pro digital-fabrication % ls
README.md		digital-fabrication
yoonayang@YOONAs-MacBook-Pro digital-fabrication % cd ../
yoonayang@YOONAs-MacBook-Pro DF2022 % ls                                 
df-test			digital-fabrication	digital-fabrication00
yoonayang@YOONAs-MacBook-Pro DF2022 % cd digital-fabrication
yoonayang@YOONAs-MacBook-Pro digital-fabrication % ls
README.md	config.toml	data		static
archetypes	content		layouts		themes
yoonayang@YOONAs-MacBook-Pro digital-fabrication % hugo version
hugo v0.92.1-85E2E862+extended darwin/amd64 BuildDate=2022-01-27T11:44:41Z VendorInfo=gohugoio
yoonayang@YOONAs-MacBook-Pro digital-fabrication % hugo serve
Start building sites … 
hugo v0.92.1-85E2E862+extended darwin/amd64 BuildDate=2022-01-27T11:44:41Z VendorInfo=gohugoio
WARN 2022/02/01 23:15:57 found no layout file for "HTML" for kind "home": You should create a template file which matches Hugo Layouts Lookup Rules for this combination.
WARN 2022/02/01 23:15:57 found no layout file for "HTML" for kind "taxonomy": You should create a template file which matches Hugo Layouts Lookup Rules for this combination.
WARN 2022/02/01 23:15:57 found no layout file for "HTML" for kind "taxonomy": You should create a template file which matches Hugo Layouts Lookup Rules for this combination.

                   | EN  
-------------------+-----
  Pages            |  3  
  Paginator pages  |  0  
  Non-page files   |  0  
  Static files     |  0  
  Processed images |  0  
  Aliases          |  0  
  Sitemaps         |  1  
  Cleaned          |  0  

Built in 7 ms
Watching for changes in /Users/yoonayang/DF2022/digital-fabrication/{archetypes,content,data,layouts,static}
Watching for config changes in /Users/yoonayang/DF2022/digital-fabrication/config.toml
Environment: "development"
Serving pages from memory
Running in Fast Render Mode. For full rebuilds on change: hugo server --disableFastRender
Web Server is available at http://localhost:1313/ (bind address 127.0.0.1)
Press Ctrl+C to stop
WARN 2022/02/01 23:16:07 found no layout file for "HTML" for kind "home": You should create a template file which matches Hugo Layouts Lookup Rules for this combination.

