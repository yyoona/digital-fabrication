---
title: '08 / Electronics Design'
---
______
## Task

Do the following to complete the assignment.

- Use the test equipment at the Fablab to observe the operation of a microcontroller circuit board: 
    - check operating voltage on the board with multimeter or voltmeter; 
    - use oscilliscope to check noise of operating voltage and interpret a data signal.
- Redraw one of the echo hello-world boards or equivalent and
add (at least) a button and a LED (with current limiting resistor) or equvivalent input and output, check the design rules, make it, test it.
- Document your process in a new page on your website.

&nbsp;
______
## Assignment 08 / Electronics Design
{{< figure src="./Soldering-05.jpeg" >}}


&nbsp;
______

### **Redraw the echo hello-world boards, adding a button and a LED, and check the design rules.**

- Setting up the KiCad (version 6.0), import the example board from the gitlab fabcloud page, and start the project
- Place components of the board and connect them together
- Connect the programming board (UPDI, FTDI)
- Add a button
- Finalize the schematic to send the data to the PCB Editor: annotate, electrical rules checker, run footprint assignment tool 
- Process through the PCB Editor
- Export the Gerber file


For the assignment of redrawing the board, I chose the KiCad and started with the "Hello-ATtiny412" as the example board. 

**KiCad** is sort of the umbrella program as a bridge between the various programs below. (The menus in a bold text are mainly used for the PCB board making)
> - **Schematic Editor**: Edit the project schematic / to arrange the components which are included in the PCB board.
> - Symbols Editor: Edit global to create or edit the symbols
> - **PCB Editor**: Edit the project PCB design / to arrange and connect symbols and footprint
> - Footprint Editor: Edit global and/or project PCB footprint libraries / to create a footprint
> - **Gerber Viewer**: Preview Gerber files / to double check the Gerber files for the PCB production
> - Image Converter: Convert bitmap images to schematic symbols or PCB footprints
> - Calculator: Show tools for calcultating resistance, current capacity, etc / deeper electronics calculations (eg. thickness of the trace...)
> - Drawing Sheet Editor: Edit drawing sheet borders and title blocks for use in schematics and PCB designs
> - Plugin and Content Manager: Manage downloadable packages from KiCad and #rd party repositories / new function in the new version. write the own plugin from KiCad in Python.

{{< figure src="./KiCad_01.png" caption="[KiCad main menu]" >}}


&nbsp;
### / Start the KiCad /

- Download KiCad and the library file
- Have the project file on the KiCad
- Start the project with the schematic file

To start the KiCad, I first went through the set-up process for the software, KiCad (version 6.0). Also, In order to have the project files on the KiCad so that I can start drawing, I downloaded the library file for the electronics from gitlab.fabclound page(https://gitlab.fabcloud.org/pub) by going into the folders; gitlab.fabclound page → libraries → electronics → KiCad.

If it's downloaded, open the KiCad and set some preferences to have the project file properly by following the steps below.
> 1. Go to Preference → Manage Symbol Libraries… → click the folder image → find “fab.kicad_sym” file → open / OK
> 2. Go to Preference → Manage Footprint Libraries… → click the folder image → find “fab.pretty” folder → open / OK
> 3. In order for the 3d modellings to work → Configure Paths → add the Environment Variables, named FAB

After the preferences are set, New Project for the actual redrawing board can be started with the schematic file. 
{{< figure src="./KiCad_02.png" caption="[Start with the Schematic file by double clicking]" >}}

The schematic files are edited on the Schematic Editor. During the redrawing, I mostly used the function on the up-side and right-side of the Schematic Editor. Left-side menu has some global modifiers related to the appearance of the editor such as grid, different cursor.
> - Functions on the up-side: Plot, Zoom, Create, Delete, and Edit symbols and footprints, Browse symbol libraries, Fill in schematic symbol reference designators, Perform electrical rules check, Run footprint assignment tool, Open PCB board editor, and so on.
> - Functions on the right-side: Add a symbol, Add a power pot, Add a wire, Add a bus, Add a no-connection flag, Add a global label, and so on.

Furthermore, the grid feature of the Schematic Editor was essential because I needed to change the grid values depending on the process of the drawing. The grid function can be found by the right-click. `Grid: 2.54mm` or `Grid: 1.27mm` is normally used (1.27mm is the best)

{{< figure src="./KiCad_03.png" caption="[View of the Schematic Editor]" >}}
{{< figure src="./KiCad_04.png" caption="[Grid function in Schematic Editor]" >}}


&nbsp;
### / Place components of the board and connect them together /

Before placing the components on the board, check the reference image. 
To place the component, use the function, `Add a symbol (A)`. When placing, the shortcut 'r' is useful to change the direction of the component.

{{< figure src="./ReferenceImage.png" caption="[Reference image]" >}}

How to place the components is followed by the several steps;
> Add the symbol → Choose symbol → Choose the component name from the libraries → OK → Click somewhere in order to place it. (The components that I needed for the redrawing were as below.) 
>> - Microcontroller_ATtiny412_SSFR
>> - Bypass Capacitor
>> - UPDI Programming board; Conn_PinHeader_UPDI_2x03_P2.54mm_Vertical Connector Header
>>   - 2x03: how many rows and columns it has
>>   - P: Pitch. distance between the pins in this header (2.54 is same as Arduino, RaseberryPi)
>>   - Vertical: the pins are going to be pointing up
>> - FTDI Connector; Conn_PinHeader_FTDI_1x05_P2.54mm_Horizontal_SMD
>> - Resister
>> - LED
>> - Button (additional)

UPDI and FTDI connectors provide a power to the circuit. In order to prevent the overpowering of the board, we can also use the Diode. But, that is quite complicated.

{{< figure src="./KiCad_05.png" caption="[Add the Microcontroller_ATtiny412_SSFR]" >}}

After placing all the components needed on the board, connecting them together is the next step. 
- Use the function `Add a wire (W)` (If we want to finish the line without any component, just double-clcik).
- In order to have power, voltage input is around. The voltage input should be specified voltage.
- Let KiCad know where the ground is. 
    - Use the function `Add a power port (P)`
    - Click somewhere on the canvas
    - Find “Power_GND” → OK
    - Keep the GND somewhere
- The bypass capacitor is important to take out the noisy current (before the current pass the microcontroller). So, connect the bypass capacitor in this way. 
    - Bypass capacitor has a certain value and it can be edited with the value by selecting the component and hitting “v” on the keyboard. (I edited the Value to 1uF)
    {{< figure src="./EditValue.png" caption="[Editing the value of Bypass capacitor]" >}}
- Add 5V output

** Useful shortcut in KiCad is “g”: grab a component without disconnecting the wire

&nbsp;
#### / Connect the programming board (UPDI, FTDI) /

**UPDI programming board**

** In order to make things more manageable in the circuit, use the “modularity option”.
- Use the function `Add a global label (Cmd + L)`.
    - Label named UPDI.
    - The label can be copied and pasted for the same elements.
    - Do the same thing for the Power_GND and Power_+5V

{{< figure src="./UPDI.png" caption="[Add a global label for the UPDI]" >}}


**FTDI Connector**
- Connect GND, VCC by the function `Add a wire`
- Not connect CTS, RTS by the function `Add a no-connection flag (Q)`
- Connect TX, RX by the function `Adding a wire`
    - Basically in the serial, TX means to transfer / RX means receive.
    - Microcontroller side
        - TX sends out data from the microcontroller by bite
        - RX wants to receive a data
    - PA1 here is the secondary serial pins 
    - RX should connect to TX on another side
        - In FTDI, RX to TX / TX to RX

{{< figure src="./FTDI.png" caption="[No-connection flag function for the FTDI connector]" >}}


&nbsp;
### / Add a button /

- Choose a symbol → Find switch → Choose BUTTON_PTS636 (for Arduino)
- In order to connect the voltage and the pin, add the LED (line indicates the ground in the symbol) and the resistor (Arduino… pull-down resistor) 

{{< figure src="./AddButton.png" caption="[Add a button, LED, and the resistor]" >}}

- All the pins can be connected (megatinycore program information https://github.com/SpenceKonde/megaTinyCore )
{{< figure src="./ATtiny 412.png" caption="[Refer the ATtiny 212/412 datasheet]" >}}
    - Pin 2 in the Arduino (PA1) /  Pin3 in the Arduino (PA2)
    - PA1 -- Resister -- LED : coming from the microcontroller -- Power_GND
    - Use a label (LED)
    - If we want the bigger version, refer ATtiny 214/414/814/1614.
        - Choose a symbol → Microcontroller_ATtiny1614-SSFR (but we are not stick to 412)
        {{< figure src="./ATtiny 214.png" caption="[Refer the ATtiny 214/414/814/1614 datasheet]" >}}
- Just add no-connection flag for PA2.

{{< figure src="./Schematic_01.png" caption="[The final version of the schematic]" >}}


&nbsp;
### / Finalize the schematic to send the data to the PCB Editor /

Now the circuit is ready!
- Programm from UPDI connector
- Communicate with the computer by the FTDI (receiver on the microcontroller)

Thus, it is time to finalize the schemtic process to send the data to the PCB Editor by annotating, electrical rules checker, running footprint assignment tool.

**Annotate**
- Manually change the name by editing the value
- Fill in schematic symbol reference designators: a special function for annotating
    - Click this function `Annotate Schematic` → click "Annotate"
    - If done, you can see the ? is changed to the number
    - C? : capacitor / U?: integrated circuit / J?: jumper or header  / R?: resister / D?: diode / J?: connector

{{< figure src="./annotate.png" caption="[Annotate]" >}}

**Electrical rules checker**
- Perform electrical rules check in order to check if the schematic understands the power supply. But, it said error…So, we need more power pots.
- Add a power pot 
    - choose “Power_PWR_FLAG” → OK → place it somewhere close to where the power comes in (FDTI positiv ad negative part; GND, VCC)
- Run the electrical checker again → check the no error

{{< figure src="./ElectricalRules Check.png" caption="[Electrical rules checker]" >}}

**Run footprint assignment tool**
- If there’s no error, Run footprint assignment tool 
    - electrical element from fab library. The individual symbols are already mapped to footprint. (footprint is actual drawing of the cooper pad)  

{{< figure src="./RunFootprint.png" caption="[Run footprint assignment tool]" >}}

**Open PCB in board editor**
{{< figure src="./OpenPCBEditor.png">}}


&nbsp;
### / Process through the PCB Editor /

**Import the schematic**
- Click `Update PCB with changes made to schematic` → Update PCB → Close
- Put the whole imported component in the center
- Click and drag each component for layout 
- In order to make the finer adjustment for the layout, choose the "Grid:0.1000mm (0.0039in)"
- Once, it’s done with the layout, we can lay them out nicely by choosing the "Grid: 2.5400mm"

{{< figure src="./PCBEditor-01.png" caption="[Process to import the component to the PCB Editor]" >}}

**Connect the components together by Route tracks**
- Before the Route tracks, need to Edit Pre-defined Sizes…
- In the menu of Board Setup (Edit Pre-defined Sizes…)
    - Net Classes: Clearance: 0.41mm (clearance between the tracks)
    - Track Width: 0.4mm
    - Via Size: 1.6mm
    - Via Hole: 0.8mm (drilling)
- Click Route tracks
    - By the click and drag and release by the cursor, connect each element
    - Before connecting the bypass and the capacitor to the GND 8 and 5V 1, should try to connect the signals first. 
        - UPDI / TX / RX
        - (**Click the line and shortcut “D” : drag the line around)
        - Connect LED to the Pin / Resister to the LED / connect the each GNDs / connect the each 5Vs 
        - Make everything is connected

{{< figure src="./ConnectComponents.png" caption="[Connect the components]" >}}

**Draw the outline**
- Switch to the Grid: 0.5000mm
- Make sure that you select the Edge Cuts layers
- Draw some individual lines
- Move the lines around
- Add the round edges by drawing an arc
    - Choose the center of the arc
    - Select the starting point and go clockwise
    - 2mm radius

{{< figure src="./outline.png" caption="[Add the outline]" >}}
    
**Add text**
- Select the F.Cu (front cupper layer)
- Add the text T412
- Edit the properties to make the text bigger
- (In order to send pcb manufacturing house, then, choose “F.Silkscreen Layer”)

{{< figure src="./AddText.png" caption="[Add Text]" >}}

**Arrange lines better**
- Jump over to the other side, click ‘v’ (Add Through Via)
- Move back to the front side, click ‘v’ again 

{{< figure src="./BetterOutline.png" caption="[Add Through Via]" >}}

**Make mounting holes**
- Use the Grid:0.5000mm
- Add a footprint → find “MountingHole_2 mm”
- Make the through-hole components through Pad Properties

{{< figure src="./MountingHole.png" caption="[Make mounting holes]" >}}
{{< figure src="./PCBEditor_01.png" caption="[The final version of the PCB board]" >}}

**Show the design rules checker window**
- Once all the components are arranged and additional factors (e.g., text, mounting holes, and so on), click the function `Show the design rules checker window` → Run DRC

{{< figure src="./DesignRulesChecker.png" caption="[Design rules checker]" >}}

&nbsp;
### / Export the Gerber file /
**In order to export the Gerber file, go to `Plot` on the top-side menu.**
- Include Layers
    - F.Cu / B.Cu: should be selected
    - F.Mask / B.Mask: solder mask by the vinyl cutter
    - Edge.Cuts
- For the General Options, unselect all
- For the Gerber Options, only select “Use extended X2 format” and “Include netlist attributes”
- For the output directory: gerbers / YES
- If the details are checked, hit Plot

{{< figure src="./Export.png" caption="[Process for the exporting]" >}}

**From the project file menu, go to Gerber Viewer and load the Gerber file one by one**
- Start with the F_Cu
- Setting in the PCB Editior again. Add Plot Edge.Cuts on all layers → Plot 
- Go back to Gerber Viewer and open the Gerber file again. (F_Cu)
- Add another gerber file (B_Cu) → (F_Mask)

{{< figure src="./exporting.png" caption="[Process for the exporting]" >}}

**Generate Drill Files**
- Setting in the PCB Editior again. → Generate Drill Files… → change some settings: Check Gerber X2 for Drill File Format / check Gerber for Map File Format / check Absolute for Drill Origin → Generate Drill File
- go to Gerber Viewer again

{{< figure src="./GenerateDrills.png" caption="[Generate Drill Files]" >}}

**How to export properly**
- File → export → SVG → change the settings 

{{< figure src="./SVGFile.png" caption="[Export SVG file]" >}}


&nbsp;
______

### **Make the Double-Sided PCB board (redrawn) and test it.**

&nbsp;
### / In CooperCAM /

Once you have the file, Open up the CooperCAM with the front cooper layer file first

- Pad apertures
    - Specify shape / The size of the drill - 1mm / Size of the pad - 1.8mm
    - Full copper pad : 1.8mm (diameter) (red part: copper is going to stay)
    - Drill through it : 1mm (black part: hole)

- Make all the holes
    - Right-click the pad (left-top) / Select Edit all identical pads
    - Choose the specification for the Size and Drill as before / Hit OK
    - Bigger holes for UPDI connector part (top-right)
    - Right-click the UPDI connector part / Select Edit all identical pads
    - Specify the shape - round, the size of the drill - 1.4mm / Size of the pad - 2.1mm
    - Why 1.4mm (not adding the full 0.8mm)? this space in-between, less than 0.4mm, the drilling bit is not gonna able to mill in between those. ***IMPORTANT!***

- Import the additional layer (back layer) → No (I want to delete it (don’t need two contour lines)
    - when exporting from KiCAD, all the layers should contain contours. Because, it makes it easier to align the front layer with the contours after)
    - In the back layer, we do not align to the contours, but to the actual path of the design.
    - Delete all identical tracks (red line: selected track) -> Yes 

- First layer (Top layer)

- Second layer (Back layer): The holes are not precisely aligned, so the holes need to be fixed. 
    - Select the first layer.
    - Set the one of the pads of the front layer (1st layer) as a reference pad. / Hover → Right-click → Set as reference pad (in the 1st layer)
    - Switch over to the 2nd layer / Hover → Right-click the same pad of the second layer → Adjust to reference pad #1
    - Now it is aligned.

{{< figure src="./CopperCAM-01.jpeg" caption="[How to align two layers]" >}}

- Once aligning is done, switch back to the front layer

- Go to file → Dimension → Leave the reframe around existing circuitry with a margin of 1mm, Z thickness: 1.8mm → OK → No (because we want to  the drill a little bit extra because of the geometry drilling bit)

{{< figure src="./CooperCAM-02.jpeg" caption="[Dimension process]" >}}

- Check the tools 
    - go to parameters → Tool Library → Number: 2 (2.5 mm Engraver) / Number: 3 (1mm Cutter) / Number 4 X (not used) / Number 5: 1mm Drill (left side of the board) / Number 6: 1.4mm Drill (on the right side of the board)
    - go to parameters → selected tools (Active tools) → Engraving tool number 2 / Hatching tool: same / Cutting tool number 3 / Drilling tools (Tricky part**): 3 options. 
        - 3 options
           > 1. we can choose one single tool
           > 2. can choose several tools which are gonna enable cooperCAM to calculate to output the separate job or different sizes of the holes. It’s gonna choose automatically which tool to use in order to drill the certain size
           > 3. without circular boring: cooperCAM is not gonna move the tool in a spiral motion. It’s gonna try to mill out the whole of the drilling point. 
        - If we choose “Use for each drill the closest smaller tool, with circular boring”: a hole is 1 mm, tool 0.8mm. drilling will be spiral motion in order to clear out the whole area. – the tool will be a milling tool
        - 1mm drill for the left holes. 
        - 1.4mm drill for the right holes
    - Drilling depth: 2.5mm / drills. They have the pointed shape, so have to go deeper  in order to drill through the edge of the bottom layer

{{< figure src="./CooperCAM-03.jpeg" caption="[Process to check the tools]" >}}

- Calculate contour
    - Delete all contours for engraving the HELLO T412 Text part 
        - right-click → Engrave tracks as centerlines → Yes 
        - Delete pad (because they can be detected as a pad, not the line)
    - Set contours (generate them again)
    - After this process, we already did export the two paths to be milled. 

{{< figure src="./CooperCAM-04.jpeg" caption="[Contour process including the text part]" >}}

- Select Mill option
    - First, we are gonna export all the paths that are relevant to the front cooper layer
        - Selection #1: Engraving layer #1 / not Mirror X (should not be Mirrored)
        - Selection #2: Drilling (Tool #5 1mm) / not Mirror X
        - Selection #3: Drilling (Tool #6 1.4mm) / not Mirror X
        - Selection #4: Cutting out / not Mirror X
        - XY-zero point: South-West corner for the origin point (instead of the white cross)
        - hit OK → project folder (Hello-ATtiny412-THT) → New directory (milling) - save the file with the name added ‘front’ at the end of the name

- What the cooperCAM did? it created the 4 files with the additional information 
    - T2: milling tool or cutting the front cooper layer
    - T3: cutting out
    - T5: 1mm drill
    - T6: 1.4mm drill 

- Prepare the other layer 
    - Mill → Unselect all → Selection #1: Engraving layer #2 / Check Mirror because, if you mill one side and filp it → OK → Save the file with the name “hello-tht-back”

{{< figure src="./CooperCAM-05.jpeg" caption="[Select Mill process]" >}}

&nbsp;
### / Operating Milling Machine /

Firstly, we need to choose the material so get the double-sided cooper pad
and put the tape on the other side.

- Preparing
    - Open up the machine, brush the particles, and swipe with the fabric.
    - Attach the board and check if it’s flat.
    - Find the toolbox 
        - Use the tool #2 (2.5mm diameter tool)
        - Insert the tool and adjust the set screw

{{< figure src="./Milling-00.jpeg" caption="[Set the milling tool for engraving (2.5mm).jpeg]" >}}
        
- Use the VPanel for SRM-20
    - Move the tool to the place of the PCB where we want to mill (x/y)
    - Move the tool down (z), and check the gap about 1cm. We can go a bit less than 1cm
    - Make the tool fall down to hit the top of the PCB material (press a little bit to check the hit properly) and adjust to a tight screw again
    - Button Z to set the Z origin to 0
    - Lift up Z a little bit 
    - Move the tool to place at the south-left corner of the board (x, y origin)
    - Hit the X/U button to turn the X, Y origin to 0
    - Close the lid of the machine and start milling

{{< figure src="./Milling-01.jpeg" caption="[Use VPanel for milling process]" >}}

- Start Cut (milling)
    - Click Cut → Delete all the previous jobs → Find the milling file → Select the front-T2 file to mill the traces on the front → Hit the Output → Hit Resume

- Start Drilling
    - Start with the 1mm drilling tool (unlabeled 1mm drill - left one)
    - Change the tool
    - Move the tool to a place where the less dust is
    - The z gap needs to go 2.5mm 
    - Fall down the tool
    - Set z origin and lift the tool up a few mm
    - Start cutting job
    - Use the tool #5 → Output → resume

{{< figure src="./Milling-02.jpeg" caption="[Start drilling with the 1mm drilling tool]" >}}

- After the 6 holes on the left side are done with the drilling, Change the tool to a 1.4mm drilling tool (green color)
    - Move to the clear location 
    - Make the tool fall down and set the z origin and lift it up and close the lid 
    - Start the drilling job T6 

- After the job T6, Hit the View (to move the board forward) and open the lid and use the vacuum cleaner

- Start Cutting the contour for the edge of the board
    - Change to the contouring bit (1mm. yellow color)
    - Release the tool and set the z-axis origin
    - Lift up a few mm and close the lid
    - Use the same x,y origin
    - Hit Cut → Delete all → Add → Select T3 file (1mm cutter) → Operation → Resume
    - Hit View and remove the dust with the vacuum cleaner
    - remove the tool (put it back)

{{< figure src="./Milling-03.jpeg" caption="[Finished milling; front-side of the board]" >}}

- After cutting the contour, time to mill the backside. 
    - remove the board carefully with the 1mm knife (the board should be symmetrical) and flip the board around by the vertical middle wise.
    - Remove the existing tape and add the tape on the front side 
        - Cut the tape in a way that doesn’t go over the edge of the board (**important**)
    - Put it back and check the surrounding margin of 1mm by using the knife 
    - Check if it’s clean and wipe it with the fabric
    - Change the tool 
    - Use a 2.5mm milling tool (same tool as the front layer)
    - x/y is the same origin, the only z-axis is changed
    - Set the Z and lift it up and close the lid
    - Cut → select back-T2 file → resume
    - when it’s done, hit View
    - open the lid and use the vacuum cleaner 
    - remove the tool
    - remove the board and check the board

{{< figure src="./Milling-04.jpeg" caption="[Finished milling; back-side of the board]" >}}

&nbsp;
### / Soldering the board /

- Clean the board with the isopropyl alcohol
- Use rivet tools
    - The reason for using the rivet: we want the pins to be available on the top of the board. 
    - Insert the rivet in the bigger holes to connect the pad on the top side with the pad on the bottom side.
    - Should know what size of the rivet to use because the each sides of the rivets need different tools.
    - Try to keep the rivet box close 
    - Use the tweezer to take the one rivet
    - T shape of the rivet should be facing down 
    - Put the board and try to push it through
    - Press down with the handle
    - Apply a little bit of solder
    - Hit up the area between the trace on the pad and the actual rivet (doesn’t have to be much)

{{< figure src="./Soldering-01.jpeg" caption="[Manual rivet tool]" >}}
{{< figure src="./Soldering-02.png" caption="[Rivet tool]" >}}

- Solder the components on the board
    - Need to check the direction of the LED and ATtiny

{{< figure src="./Soldering-03.jpeg" caption="[Components of the board]" >}}

{{< figure src="./Soldering-05.jpeg" caption="[Soldered board]" >}}


&nbsp;
______

### **Use the test equipment at the Fablab to observe the operation of a microcontroller circuit board**

&nbsp;
### / Check operating voltage on the board with multimeter or voltmeter /

>**How to use the multimeter** (Reference video → https://youtu.be/TdUK6RPdIrA)
>- Check DC Voltage (measured in volts, written as V)
>    - DC Voltage: main source for the DC voltage in devices we use are batteries. DC voltage stands for Direct Current. 
>    - When using the multimeter to measure for DC voltage, we need to set the mode to the V for voltage with the straight and dotted lines. Next we need to make sure that our test leads are in the right location. The black ground or common test lead will always need to be COM. But the red test lead is the one that we will need to switch around based on what we are measuring.
>    - If we are unsure about how much voltage we are supposed to measure, we can always start higher and come lower to get a more precise measurement. 
>    - If we see a negative sign, it means we have out test leads on the circuit backwards. This doesn't hurt the multimeter but it is a good way to find out the right positive and ground side. 
>- Check AC Voltage
>    - AC Voltage: any device that plugs into the wall socket runs on AC voltage. AC voltage stands for Alternating Current. 
>    - When measuring AC volatage, the lead stays in the same slots as when we measure the DC voltage. But, we need to set the dial to the Voltage sign with the wave sign. 
>    - Set the multimeter to 200 in the AC voltage area. (in case of US, we get about 110 volts at the socket)
>    - When starting to measure, we need to keep our fingers far from the tip of the test leads and that the leads don't come close to each other at when we are taking the measurements. For added safety, we can always wear the gloves.
>    - First, the test lead for the ground (black) goes into the bigger hole. Next, the red test lead goes into the smaller hole. 
>- Check Resistance
>    - Why measuring resistance is important? That's because, different components of an electronic circuit or system are supposed to have a certain amount of resistance so that the whole circuit works properly.
>    - The black ground test lead goes into the COM as always. And, the red test lead stays in the same location as we see the sign for the resistance. But, set the multimeter to the Ohm sign section. 
>- Check Continuity
>    - Checking the electric circuit to see if current can flow through the circuit by sending a very small amount of voltage through it. And, on a multimeter continuity is usually verified by a sound of a beep.
>    - Set the multimeter to the sound wave sign. And test leads stays in the same locations (slots) as when we measure the DC and AV voltage. 
>    - The tips moves in different directions it stops and starts on its own. 
>    - When measuring the continuity, make sure that the devices are not powered so unplug it or remove batteries.
>- Check Amps (current; measured in amperes, written as I)
>    - Red test lead need to go into the 20A(max) or mA slot.
>    - Fused: if we measure over 200mA (0.2Amps)
>    - Unfused: if we measure ober 20Amps
>    - When we are not sure, start with the higher amps.
>    - Set the multimeter to the Amps with the line and doted line section. 
>    - When measuring Amps, we need to place the multimeter  on the power side of the circuit. So, Power side of the battery goes to the red test lead, the power goes through the multimeter and the black test lead to the positive side of the device. Next, completely connect to the ground side and check if the device runs and read the value on the screen. 

**Check continuity of the HelloATtiny412 board**
- After done with the soldering, check the continuity of the board with the multimeter. For the test, we need to turn the know to select Ohm and press the yellow button to set the sound ‘beeping’ mode.
- When checking if it’s soldered well, the ‘beeping sound’ means connected.

{{< figure src="./Soldering-04.jpeg" caption="[Check contiunity of the board]" >}}

**Measure voltage of the HelloATtiny412 board**
- I tried to check the voltage of the board through the capacitor and check the voltage through the LED of the board.
    - Voltage through the capacitor: 5.24 V 
    - Voltage through the LED: 2.071 V
        - I used the orange color LED. It has a voltage drop of 2. Depending on the LED color, the voltage drop value is different. (https://www.electricalengineering.xyz/electronics/led-voltage-drop-by-color/ ← shared by Yuhan) 
{{< figure src="./measure-01.jpg" caption="[Measuring voltage]" >}}

**Some relationship between the basic units within the electrical circuit**
- P (power) = V (voltage) * I (current) = R (resistance) * I^2
- R = V / I → Ohm's law

&nbsp;
### / Use oscilloscope to check noise of operating voltage and interpret a data signal /

>**How to use an oscilloscope** (Reference video for the Tektronix oscilloscope → https://youtu.be/tzndcBJu-Ns)
>- What is the oscilloscope?
>    - A device for seeing how the voltage of the signal varies over time. 
>    - The particular oscilloscope can display up to four analog channels from different inputs and each channel has its own independent set of vertical controls.; Each waveform can be positioned independently of the others on the screen.
>    - **Vertical controls**: The two most important vertical controls are the vertical position and the vertical scale. 
>{{< figure src="./OscillioRef-01.jpg" caption="[About the vertical controls]" >}}
>        - So the vertical position moves the waveform up and down the screen, and the vertical scale expands or compresses the waveform vertically.
>        - What does vertical scale mean? 
>            - Vertical scale is expressed in volts per division. So this particular model of oscilloscope has eight vertical divisions. If we have eight and we happen to have it set to five volts per division, we could fit a 40-volt peak to a peak sine wave on the screen.
>    - Check the waveform moving up and down on the screen (move the vertical position) by controlling the knob connected to the channel. It's continuous. 
>    - Check the waveform by the vertical scale. It is more granular.
>    - **Horizontal controls**: All of the input channels share one common set of horizontal scale parameters. That means as we adjust the horizontal controls, all of the waveforms will move together on the screen.
>{{< figure src="./OscillioRef-02.jpg" caption="[About the horizontal controls]" >}}
>        - Horizontal Position: As we move the horizontal position, the trigger indicator moves left or right. And, that causes all of the waveforms on the screen to move left or right together.
>        - Horizontal Scale: As we adjust the horizontal scale, that causes all the waveforms on the screen to expand or contract horizontally.
>            - Horizontal scale is expressed in seconds per division. So if we have ten horizontal divisions available on the screen and we have an oscilloscope set to one second per division, then that means we can fit ten seconds' worth of data on the screen.
>    - **Trigger**
>{{< figure src="./OscillioRef-03.jpg" caption="[About the trigger]" >}}
>        - Rising edge trigger: with a rising edge trigger, we care mainly about the trigger level. The trigger level is the voltage that the signal has to cross through for the oscilloscope to consider it time to update the display. With the rising edge trigger, we want our trigger level to be somewhere between the top and bottom of the waveform. The reason why it's important to see that correctly is that without triggering, the oscilloscope just updates its data at arbitrary times, and the signal is repeating at arbitrary times, and those probably won't line up. 
>        - Whenever it is time for the oscilloscope to update, it waits until the voltage crosses through the level (points to the center of the signal), and then it marks that point with the trigger indicator. So this is the start of the signal and it updates. Notice that is also able to display a little extra data before the start of the signal. 
>{{< figure src="./OscillioRef-04.jpg" caption="[Oscillioscope in the reference video]" >}}
>(Other reference video for Sparkfun oscilloscope → https://youtu.be/u4zyptPLlJI)


**Try to use the oscilloscope to try to see PWM cycles or bytes in a serial connection**

The oscilloscope model in Aalto Fablab is Tektrnonix TDS 1001C-EDU. [official manual source ⇱](https://www.manualslib.com/products/Tektronix-Tds1001c-Edu-8793363.html)
{{< figure src="./Oscillo-01.jpeg" caption="[Oscilloscope in the Aalto Fablab]" >}}

- We normally used the oscilloscope to check the voltage. This can check high frequency. Also, we can check what the PWM is, by attaching the probe tip to the GND pin on the circuit first. Before testing, we should calibrate the probe precision (check the Manual Probe Compensation).

{{< figure src="./Oscillo-04.jpeg" caption="[Oscilloscope in the Aalto Fablab]" >}}

- Two channels are available on this oscilloscope but focus on one channel. We also can change the waveform and align the viewer (each square is one voltage).
    - Vertical: voltage
    - Horizontal: time (M)

{{< figure src="./Oscillo-06.jpeg" caption="[Control the knob to adjust the viewer]" >}}

- We can also use the oscilloscope for Trigger.
    - Need to specify the source (ch1, 2).
    - Trigger level can be checked by the arrow on the right side.

- Using the cursor, we can measure different things.
    - Delta voltage (delta t : 1mm/s)
        - Disconnect the probe and connect it to the GND and check the PWM signal changes according to the brightness of the LED.
        - the full cycle in a certain amount of time
        - duty cycle (https://en.wikipedia.org/wiki/Duty_cycle)
    - Amplitude: 4.6v for the microcontroller 

{{< figure src="./Oscillo-08.jpeg" caption="[Control the cursor]" >}}
{{< figure src="./Oscillo-10.jpeg" caption="[Check the cycle]" >}}

- When using the oscilloscope to check the PWM value, we should connect other probes to the GND of the board first, and connect to RX/TX.

{{< figure src="./Oscillo-11.jpeg" caption="[Other probes goes to components of the board]" >}}

- Through the oscilloscope, we can also use the screen image capture function with the USB.

{{< figure src="./Oscillo-12.jpeg" caption="[Oscilloscope image capture function]" >}}

&nbsp;
### / Ocsilloscope test with the PWM cycle of the HelloATtiny412 board /

I checked the PWM cycle of my HelloATtiny412 board programmed by the code for LED blinking. For some reason, it was quite difficult to find the proper wave. The arrow of the trigger part didn't show up. Also, In the wave, there are some blanks in between the waves. 

{{< youtube id="Z21yyuzplU4" title="Ocsilloscope test with the HelloATtiny412 board" >}}
[Ocsilloscope test with the HelloATtiny412 board]
