//#include <ArduinoOSCEther.h>
#include <ArduinoOSCWiFi.h>

/*
  Arduino LSM6DS3 - Simple Gyroscope

  This example reads the gyroscope values from the LSM6DS3
  sensor and continuously prints them to the Serial Monitor
  or Serial Plotter.

  The circuit:
  - Arduino Uno WiFi Rev 2 or Arduino Nano 33 IoT

  created 10 Jul 2019
  by Riccardo Rizzo

  This example code is in the public domain.
*/

#include <Arduino_LSM6DS3.h>
#include <ArduinoOSCWiFi.h>

float x, y, z;


// WiFi stuff
const char* ssid = "aalto open";
const char* pwd = "";

// for ArduinoOSC
// this is the IP of your computer
// make sure you are on the same network
const char* host = "10.100.48.161";
const int recv_port = 54321;
const int send_port = 55555;

void setup() {
  Serial.begin(9600);
  //while (!Serial);

  if (!IMU.begin()) {
    Serial.println("Failed to initialize IMU!");
    while (1);
  }
  //connect to WiFi
  WiFi.begin(ssid, pwd);
  while (WiFi.status() != WL_CONNECTED) {
    Serial.print(".");
  }
  Serial.print("WiFi connected, IP = ");
  Serial.println(WiFi.localIP());

  Serial.print("Gyroscope sample rate = ");
  Serial.print(IMU.gyroscopeSampleRate());
  Serial.println(" Hz");
  Serial.println();
  Serial.println("Gyroscope in degrees/second");
  Serial.println("X\tY\tZ");

  // this command starts to publish the values
  OscWiFi.publish(host, send_port, "/gyro", x, y, z);
}

void loop() {

  if (IMU.gyroscopeAvailable()) {
    IMU.readGyroscope(x, y, z);

  Serial.print(x);
    Serial.print('\t');
    Serial.print(y);
    Serial.print('\t');
    Serial.println(z);
  }
  // this update is needed for the publishing to work
  OscWiFi.update();
  delay(100);
}
