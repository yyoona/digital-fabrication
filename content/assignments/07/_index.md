---
title: '07 / 3D Scanning and Printing'
---
______
## Task

Do the following to complete the assignment.

- Test the design rules of the 3D Printers at Aalto Fablab.
- Design an object that cannot be made using subtractive manufacturing.
- 3D print the object you designed.
- 3D scan something and (optionally) print it.
- Document your process in a new page on your website.

&nbsp;
______
## Assignment 07 / 3D Scanning and Printing
{{<figure src="./hero-shot.jpg">}}


&nbsp;
______

### **3D Scan with Artec Leo**

Artec Leo. This is a wireless 3D scanner in the Aalto Fablab. During the scanning, you can walk around the object freely to capture the appearance, texture of the object with the higher resolution. [(Go to check the technical specifications ⇱)](https://www.artec3d.com/portable-3d-scanners/artec-leo#tech-specs) 

Power cable should be always connected to the power supply unit when you charge the battery of the device. If you are not in the wifi network environment, the network cable can be used to transfer and download the data from the the device to the computer. 

For the assignment of the 3D scanning, I firstly tried to scan small household objects such as a hook, a small cup. However, the object was so small to scan with the Artec Leo. Eventually, I changed to scanning a person.

{{< figure src="./Leo case.png" caption="[Components of Artec Leo in the case]" >}}
{{< figure src="./Artec Leo.png" caption="[Artec Leo]" >}}

&nbsp;
#### / Operate the 3D Scanner /

Operating the Artec Leo is very simple and straightforward. After preparing the object to scan on the base (table), start operating the Artec Leo.
- Turn on the power button on the device
- Touch NEW PROJECT on the screen
- Start scanning by touching the record button on the screen or pressing the button on the handle (Stop function is same as the start.)
- Keep tracking around the object until the red part on the screen is almost disappeared (Green color means that the part are well scanned.)
- If you change the position of the object, use the ADD SCAN function
- After the scanning is done, rename the file on the screen and check the scanned files

{{< figure src="./Scanning-01.jpeg" caption="[Turn on the power button to start the device]" >}}
{{< figure src="./Scanning-02.jpeg" caption="[Start the new project]" >}}
{{< figure src="./recording button.jpeg" caption="[Red button on the handle is the record button]" >}}
{{< figure src="./AddScan.jpeg" caption="[ADD SCAN function]" >}}
{{< figure src="./Scanning-03.jpeg" caption="[Scanning the object]" >}}
{{< figure src="./Scanning-04.png" caption="[Scanning a person]" >}}

#### ✧ ✧ ✧  
Tips
- While scanning the object, keeping the distance away from the object is important.
- Apply masking tape to the background or draw 'X' on it or the base. 
- Base removal makes the users can scan more easliy. 

&nbsp;
#### / Post Process by Artec Studio on Computer /

After the scanning process is done, go to computer (3 computers are available in the Aalto fablab) for the post processing of the scanned object by using the post processing software, the Artec Studio 15 Professional. The post process can be divided into three steps; 1) Import the scanned file, 2) Clean the scanned object image, 3) After export.
{{< figure src="./computers.png" caption="[Computers that can be used for the Artec Studio in the Fablab]" >}}

>#### STEP 1. Import the scanned file
>1. Open the Artec Studio 15 on the desktop and import the scanned contents by clicking the `Import from Leo`
>2. Connect the Artec Leo to the computer by IP
>3. Hit Setting → Network → Check wifi if it is same as the wifi of the computer → Hit the arrow, check the IP address
>4. Enter the IP address on the Artec Leo → Hit connect
>5. Check if the scanner is on
>6. Click the project and import
>7. Unload or load the object (scanned components) depending on the needs, or can delete the object at all

{{< figure src="./PostProcess-01.jpeg" caption="[Open the Artec Studio 15 on the computer]" >}}
{{< figure src="./Import.jpeg" caption="[Import the scanned contents]" >}}
{{< figure src="./PostProcess-02.jpeg" caption="[Import the scanned file by connecting the scanner (Artec Leo) and the computer via IP address]" >}}
{{< figure src="./load.png" caption="[Load and unload the scanned components]" >}}

&nbsp;
>#### STEP 2. Clean the scanned object image
>1. Go to Editor → Eraser → Lasso selection → Erase
>2. Go to View → Change the Perspective view to Orthographical view in order to select the object more efficiently
>3. Use Tools menu → Apply Global registration in the Registration
>4. Apply Outlier removal in the Fusion of the Tools menu
>5. Use Align menu → Select the Auto-alignment → After the alignment is done, click Apply
>6. Go back Tools → Outlier removal again → Apply Global registration 
>7. Apply Outlier removal again
>8. Apply Fast fusion in the Tools menu
>9. Close the projects, and focus on the fast fusioned object
>10. In the Postprocessing section of the Tools, apply remove Small-object filter → Apply Hole filling (specify maximum parameter at 300)
>11. Fix holes Manually → Can select all → Fill holes 
>12. Go back to Tools and apply Mesh simplification in the Postprocessing section (try to apply remeshing or smoothing or others as well)
>13. Go to Texture menu → Select all of the components → Check the Expor, Enable texture normalization, and Inpaint missing texture → Click Apply
>14. Try the different adjustment for the Brigntness, Saturation, Hue, Contrast, and Gamma correction in the TEXTURE menu. 
 
[(For better understanding about the useful tools in Artec Studio software ⇱)](https://www.laserdesign.com/5-essential-tools-to-master-in-artec-studio-software/#:~:text=Fast%20fusion%2C%20as%20the%20name,more%20time%20in%20the%20model)

{{< figure src="./PostProcess-04.jpeg" caption="[Erase the unnecessary part with the Lasso selection tool]" >}}
{{< figure src="./PostProcess-05.jpeg" caption="[Change the view to Orthographical view]" >}}
{{< figure src="./Tool.jpeg" caption="[The Tools menu]" >}}
{{< figure src="./alignment.jpeg" caption="[Auto-alignment in the Align menu]" >}}
{{< figure src="./PostProcess-08.jpeg" caption="[Process for the filling holes]" >}}
{{< figure src="./PostProcess-09.jpeg" caption="[Process for the Fast Fusion]" >}}
{{< figure src="./PostProcess-10.jpeg" caption="[Result image After Fast Fusion]" >}}
{{< figure src="./PostProcess-11.jpeg" caption="[Adjust the options in the texture menu]" >}}
{{< figure src="./PostProcess-12.jpeg" caption="[Final result image]" >}}

&nbsp;
>#### STEP 3. After export
>1. Go to File → Export → Meshes → Save under LeDemo → Choose `obj` format for sending to 3d printing slicing software ( or choose `stl` format for sending to 3d printer directly)
>2. Model texture format as png (for the less loss the quality)
>3. Go to Blender
>4. In Blencder, go to File → Import → Select Wavefront(obj) → Find the obj file → Click Import OBJ
>5. Go to View mene → Choose PerspectiveOrthographic 
>6. Change the position of the object by the Transform meny(for example, rotate 90 degree)
>7. Check the material
>8. Finalize by saving the file

{{< figure src="./PostProcess-13.jpeg" caption="[Export meshes]" >}}
{{< figure src="./PostProcess-14.jpeg" caption="[Save as obj format for the 3d printing slicing software]" >}}
{{< figure src="./PostProcess-15.jpg" caption="[3d scanned image in Blender]" >}}

&nbsp;
______

### **Design an object that cannot be made using subtractive manufacturing**
In order to try 3D Printing, I designed the hook which is the double parallel lines with a round shape. This shape can be made by the additive manufacturing, not the subtractive manufacturing. In order to better understand for those manufacturing, I refer a diagram from the research; [(Three-dimensional printing in orthopaedic surgery: A scoping review ⇱)](https://www.researchgate.net/publication/342609958_Three-dimensional_printing_in_orthopaedic_surgery_A_scoping_review).

{{< figure src="./additive.png" caption="[Comparing the additive manufacturing and the substractive manufacturing]" >}}

In order to design this rounded object, I especially used the Sweep function.  Except for that, I only use the Sketch, Mirror, and Fillet function. The following images illustrate the flow of the hook design. 

{{< figure src="./design_01.png" caption="[Make a sketch]" >}}
{{< figure src="./design_02.png" caption="[Make the diameter]" >}}
{{< figure src="./design_00.png" caption="[Sweep function]" >}}
{{< figure src="./design_03.png" caption="[Sweeped body]" >}}
{{< figure src="./design_04.png" caption="[Mirror function]" >}}
{{< figure src="./design_05.png" caption="[Fillet 2.50mm]" >}}
{{< figure src="./design_06.png" caption="[Fillet all the outside parts]" >}}

After the design is done, exporting is the next. The exported file should be in the STL file format in order to send the design to the slicing software before the 3D printing.

{{< figure src="./design_07.png" caption="[Go to file and Select the Export]" >}}
{{< figure src="./design_08.png" caption="[STL(*stl) Files are needed for exporting]" >}}



&nbsp;
______

### **3D Print**

If the design file is ready, it is time to try 3D printing. In the Aalto Fablab, there are four different 3D printers.  
- Ultimaker 2+ extended
{{< figure src="./Ultimaker2.png" >}}
- Ultimaker 3S
{{< figure src="./UltimakerS3.png" >}}
- Lazbot Mini
{{< figure src="./LazbotMini.png" >}}
- Lazbot TAZ
{{< figure src="./LazbotTAZ.png" >}}

All the printers have the FDM (Fused filament fabrication) technology. Ultimaker can be used for the overnight print, but, Lazbot is available to use only during the daytime (opening time of the Aalto Fablab). According to the time convenience, I explored Ultimaker Printers to accomplish this assignment.

&nbsp;
#### / Use the Ultimaker Cura /

In order to print, the prepared file should be specified by using the computer next to the 3D printer. Slicing is the process of cutting the object in layer, so that the layer can be transformed into lines to be printed out. ‘Ultimaker Cura’ is the software is for the ultimaker 2+ extended and ultimaker 3S (Cura-lazbot is for the Lazbot 3d printers).

The following steps are how to start the 'Ultimaker Cura'.
> - Open the Ultimaker Cura on the desktop
> - Select the printer to use
> - Select the material to use (material and nozzle size)
> - load a '.stl' file for printing
> - After loading, the basic manipulations through the several tools on the left can be changed
> - Through the Multiply Selected function (right click or shortcut: Ctrl+M), the number of the object can be managed

{{< figure src="./cura-01.png" caption="[Opening the slicing software 'Ultimaker Cura']" >}}
{{< figure src="./cura-02.png" caption="[Opening the slicing software 'Ultimaker Cura']" >}}
{{< figure src="./cura-03.png" caption="[Basic manipulations on the left side of the software]" >}}

After the objects on the bed are arranged by dealing with the left side tools, more detailed print setting can be specified throught menu on the right-top side. Profiles, Infill(%), Support, and Adhesion can be managed with the menu. If the specification is set, the estimated time to print can be checked on the right-bottom side by clicking the slice. 

> - Profiles: layer height
>   - if needed more detail, select the leftest one; thinner one (0.06), but not recommended because it makes for the nozzle to be jammed).
>   - 0.15 or 0.2 is the general select.
> - Infill(%): defining how much infill that your object is going to have. 
>   - none: only outline. only air inside
>   - 20% (or 10%): usually default
>   - if need more infill, then choose higher percentage of the Infill. But, 100% is not recommended. Because, if the object is bigger, bottom layer can be shrinked more so that the shape would be changed unintentionally.
>   - The pattern of the Infill also can be changed with various patterns. 
> - Support: Additional structure. Use this tool in case the plane can not printed in the air. (if the angle is slight, then might not need the support)
> - Adhesion: Additional layer for the bottom part

{{< figure src="./cura-04.png" caption="[The menu for the detailed print setting on the right-top side of the software]" >}}

{{< figure src="./cura-05.png" caption="[Slice button on the right-bottom side]" >}}

&nbsp;
#### / How to operate the 3D Printer based on the Ultimaker 2+ Extended /
- Switch to turn on/off the machine is on the left-bottom part on the backside.
- To start printing, need to load the material
    - Navigate to the material section by rotating the navigator and select the section and press the navigator, then can enter the specific menu.
    - In order to change the filament, select the ‘Change’
    - While the machine is heating up the nozzle, prepare the clipper to cut the end of the filament
    - Select the ‘Remove material’ and select ‘PLA’ in the material menu
    - Wait until the material comes out the nozzle.
    - When the filament is done with coming out the nozzle, confirm ‘ready’.
- Go to computer to specify the prepared file with the slicing process and save the `g.code` file in the SD card.
- Go back to the printer.
    - Put the SD card back to the printer
    - Make sure the print bed is clean 
    - Select PRINT option by the dial 
    - Select the file name and enter it
    - The machine start printing (if the machine extrudes the filament before printing, make sure to remove it out quickly.)

&nbsp;
#### / Test with the different setting with Ultimaker 2+ Extended /

Firstly, I tried to get used to the setting for the 3D printing by printing the shaft that I designed in the previous assignment. For doing this, I used the Ultimaker 2+ Extended and printed the shaft two times with the different print setting. Through the Cura software, I set the Profiles and Infill, Support, and Adhsion. 

The first setting was not suitable for the shaft design because it doesn't need the `Support`. By having the Support, the holes in the shaft are almost blocked up. Thus, the Support needs to be unchecked for this shaft design.

Also, I set the different Infills for the two shafts. Infill patterns is the interesting part that I can choose among the various patterns. [(Go to see more about the Infil settings and patterns ⇱)](https://support.ultimaker.com/hc/en-us/articles/360012607079-Infill-settings). Depending on the pattern, the density of the printed object can be varied and the estimated time to print can be changed. For the first shaft, I followed the Grid among the pattern options. But I changed it to the Gyroid for the second shaft in order to increae the strength of the object. As the result, I could even feel the difference between the weight of the two shafts. The second shaft has more weight with more firmness.

{{< figure src="./test-01.jpeg" caption="[The print setting that I did for the first time]" >}}
{{< figure src="./test-02.png" caption="[The print setting that I did for the second time]" >}}
{{< figure src="./test-03.png" caption="[Infill Pattern - Gyroid]" >}}
{{< figure src="./infillProgress.jpg" caption="[Printing progress for the second shaft]" >}}
{{< figure src="./shaft.jpg" caption="[Comparing the two shafts for the different print setting (left: second try, right: first try)]" >}}

- Print setting for the first shaft:
    - Profile: 0.15
    - Infill: 90% (Gradual infill) / Grid
    - Support: checked
    - Adhension: checked
    - Estimated time: 1 hour 54 minutes

- Print setting for the second shaft:
    - Profile: 0.15
    - Infill: 50% / Gyroid
    - Support: unchecked
    - Adhension: unchecked
    - Estimated time: 3 hours 29 minutes

&nbsp;
#### / Use Ultimaker S3 for printing the hooks /
After the testing the print setting with the shaft, I also tried to print the hook that I designed. For doing this, I used the Ultimaker 3S 3D printer. With this printer, I used the additional nozzle for Print Core 02 with BB core and PVA (water solluble) material. This part should be specified when select the material to use in the Ultimaker Cura. 

{{< figure src="./UltimakerS3Guide.png" caption="[Guide for the Ultimaker S3 3D Printer about the Print Core 01 and 02]" >}}

First, I loaded the hook as the STL file and added one more hook by the Multiply Selected function. Also, I scaled down the added hook at 70%. 

{{< figure src="./printHook-02.png" caption="[Load the hooks on the bed and set the printer detail and the material (filament detail)]" >}}
{{< figure src="./printHook-04.png" caption="[Specify the Print setting for the hook]" >}}

After specifying the print setting, I exported the file as `.ufp` file format and add the file in the USB to be inserted to the 3D Printer. 

When the file is ready in the USB, go to the 3D printer and insert the USB on the slot. The Ultimaker 3S has a touch screen so that I just touch the Select from USB button on the screen in order to start to get ready to print. 

{{< figure src="./screen-01.jpg" >}}
{{< figure src="./screen-02.jpg" >}}
{{< figure src="./screen-03.jpg" >}}
{{< figure src="./screen-04.jpg" caption="[Process to start printing with the touch-screen of Ultimaker S3 3D Printer]" >}}

This 3D printer has the special calibration process for the two different nozzles. Thus when the printing starts, it takes more time for the calibration process. After the calibration is done, finally it starts printing. Because I used the PVA Natural water solluble material for the second nozzle, I go through the additional process after the printing is done. That was to melt the PVA material. It took quite long time (about 4 hours for the bigger hook) for the material to be melted so that easy to detach the material from the PLA part. 

{{< figure src="./calibration.jpg" caption="[Calibration for the two nozzles and the bed]" >}}
{{< figure src="./hookProgress.jpg" caption="[Progress of the printing]" >}}
{{< figure src="./printedHook.jpg" caption="[The Printing is done]" >}}
{{< figure src="./water.jpg" caption="[Soak the hook into the water to melt the PVA material]" >}}
{{< figure src="./finishedHook.jpg" caption="[The finished hooks]" >}}








