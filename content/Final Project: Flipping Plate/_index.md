---
title: 'Final Project: Flipping Plate'
weight: 1
---
______

## A Flipped Plate: shape variations
______
&nbsp;


{{< figure src="./final-project-idea.png" caption="[Sketch for the display of the installtion]" >}}


&nbsp;&nbsp;

The project title is "A Flipped Plate: shape variations".  
It will show the kinetic installation as the final outcome.  
This is a part of Yoona's artistic practice, exploring the idea of coexisting ambivalence.

>> *The flipped wooden plate is two-sided. At first glance, the plate appears to have front and back sides. However, as the audience moves around the installation, each side of the plate simply switches to another side, creating a natural and ambient sound by touching the other plate. The audience eventually walks around the wooden object, just observing the plates flipping while listening to their sound and watching their changes, not knowing which side is actually front or back. / artist note*

This sculptural installation uses the movement of flipping the wood blocks by strings as a system of the toy, [Jacob's ladder ⇱](https://en.wikipedia.org/wiki/Jacob's_ladder_(toy)). As the previous practice for the artistic concept, 'coexisting ambivalence', I made one module (dimension: W 120 x H 1660 x D 260 mm) with the linked twenty-five sets of wooden plates (length: 1540mm) and the aluminum structure with the electronic components. Especially, the first practice includes the Infrared Array sensor to detect the human body's temperature for generating the movement of the installation.

In the new practice during the Digital Fabrication Course, meanwhile, the installation's structure will have various forms with different materials for the technical and mechanical parts. ~~On top of that, the new practice will not include the interactive factor.~~

&nbsp;&nbsp;

{{< youtube id="2-TWkduYR8k" title="Video of the previous practice" >}}

&nbsp;&nbsp;

{{< figure src="./installation-view.JPG" caption="[Installation view of the previous practice]" >}}


{{< figure src="./technical-structure.png" caption="[Mechanical structure detail]" >}}


&nbsp;

-----
#### Reference works

- [Jacob's Wall (2018) by Parker Heyl ⇱](https://www.parkerheyl.com/?pgid=j9zl8qi9-0b3bc0a6-5487-463b-9b37-b29a25c6d8f3)

- [Katakata (2015) by Kirsty Keatch ⇱](https://www.kirstykeatch.com/katakata.html)
 
- [Light Forest (2016) by Legacy Lab International ⇱](http://www.legacy-lab.com/en/portfolio_page/light-forest/)


&nbsp;
______
### **PLANNING**

&nbsp;

#### Questions about how to rotate the wide wood plate

- using 2 motors for the each edges?
- one motor with additional structure?
- with one motor, is it possible to rotate the wide plate?
- to fix the plate without tilted edge

{{< figure src="./IdeationSketch.jpg" caption="[IdeationSketch]" >}}


&nbsp;
______
### **FABRICATING**

&nbsp;

#### / Shaft /

##### [1] 2D Sketch
I planned to make the 3D modelling for the roation shaft (the connection part) between the motor and the wooden plate module as the assignment of the Computer-Aided Design. As the plan, I used this 2D raster graphic tool to draw a rough sketch for the 3D modelling test.  

{{< figure src="./earlier-structure.jpg" caption="[The mechanical structure of the previous installation. The red circled part will change into the new design]">}}

{{< figure src="./2d-sketch.jpg" caption="[2D drawing for the new design of the rotation shaft with Tayasui Sketches]">}}

##### [2] Modeling with Fusion360
After the sketch, I designed the 3D object of the shatter with Fusion360.

{{< figure src="./05-HoleMotor.png" caption="[Creating holes for connecting motor]">}}

{{< figure src="./10-HoleConnectorExtrude.png" caption="[Using the Extrude command for the two holes]">}}

{{< figure src="./12-Fillet.jpg" caption="[Applying Fillet on the curves of the 3D object]">}}

{{< figure src="./14-FinishView.jpg" caption="[Finished view - front, side, bottom, top (clockwise direction from the top-left)]">}}

##### [3] 3D Printing 
After the modeling is done, I tried to print it out by the 3D printer 'the Ultimaker 2+ Extended' in the Fablab. First try was failed because of the wrong setting for the support Through the Cura software, I set the Profiles and Infill, Support, and Adhsion. 

The first setting was not suitable for the shaft design because it doesn't need the `Support`. By having the Support, the holes in the shaft are almost blocked up. Thus, the Support needs to be unchecked for this shaft design.

Also, I set the different Infills for the two shafts. Infill patterns is the interesting part that I can choose among the various patterns. [(Go to see more about the Infil settings and patterns ⇱)](https://support.ultimaker.com/hc/en-us/articles/360012607079-Infill-settings). Depending on the pattern, the density of the printed object can be varied and the estimated time to print can be changed. For the first shaft, I followed the Grid among the pattern options. But I changed it to the Gyroid for the second shaft in order to increae the strength of the object. As the result, I could even feel the difference between the weight of the two shafts. The second shaft has more weight with more firmness.

{{< figure src="./test-01.jpeg" caption="[The print setting that I did for the first time]" >}}
{{< figure src="./test-02.png" caption="[The print setting that I did for the second time]" >}}
{{< figure src="./test-03.png" caption="[Infill Pattern - Gyroid]" >}}
{{< figure src="./infillProgress.jpg" caption="[Printing progress for the second shaft]" >}}
{{< figure src="./shaft.jpg" caption="[Comparing the two shafts for the different print setting (left: second try, right: first try)]" >}}

Final Print setting for the shaft:
- Profile: 0.15
- Infill: 50% / Gyroid
- Support: unchecked
- Adhension: unchecked
- Estimated time: 3 hours 29 minutes

##### [4] Adjustment
Now I got the component ready. But, I haven't assembled the components yet. Because, I realized that the hole of the shaft needs to be more adjustment to fit to the rod of the motor. The rod diameter is 4mm and my design of the shaft hole is also same diameter. However, the result of the 3D printed shaft should be a bit bigger. 
{{< figure src="./ShaftHole.jpg" caption="[Shaft hole and the motor]" >}}

Also, according to the next step, the rod bolt also needs to be added to the shaft design.
I should choose if I use the metal rod bolt or plastic rod which is attached to the shaft directly. 
{{< figure src="./Rod.png" caption="[Rod on the shaft to touch the microswitch]" >}}

##### [5] Remake the shaft 
I had to make the shaft again. Rhe previous shaft looks okay with the appearance, but several details need to be fixed. Firstly, The set screw hole should be bigger and the hole should be a thread shape so that the set screw can fix well into the hole. 

{{< figure src="./SetScrewChange.jpg" caption="[Edit the set screw hole to the bigger holde and add the thread]" >}}
{{< figure src="./3DPrintingAgain.png" caption="[Ready to do the 3d printing for shaft and holder]" >}}
{{< figure src="./3dPrinting.jpg" caption="[3D Printing progress]" >}}


&nbsp;

#### / Wooden Plate /

##### [1] Planning and the first design
Each plate has six holes for the screws (2mm's bolts and nuts) that will fix the string to the plates. Also, the string needs to be fixed on right position of the plate. Some plates need a string to be fixed in both ends of them. Other plates need a string to be fixed in the middle of them. So I decided to use ENGRAVE in order to mark the string's position on the plate. 

{{< figure src="./wooden-plates-ideation.JPG" caption="[Sketch for how to fix the string to the plates. Dotted line is the string that is fixed to the back-side]">}}

{{< figure src="./Wooden-plates-ai.png" caption="[Wooden Plates Sketch on Illustrator]">}}

To decide the set-up detail for cutting and engraving, I tried to make the test illustrator file. The different colors are used to test the different speeds of Engraving. Also, the hole’s kerf compensation is applied by the offset setting on Illustrator. As a result of the laser-cutting test, I specified the set-up detail below.

- Cutting Set-up: Speed 38.0% / Power 60.0% / Frequency 50.0% / kerf 0
- Engraving Set-up: Resolution 300 / Speed 10.0% / Power 50.0%

{{< figure src="./test.jpg" caption="[Test for the laser cutter's detail]">}}

For the prototype, I made twelve pieces of the plates so that the length of the total wooden plates for the test is 900mm. 
{{< figure src="./laser-outcome-01.jpeg">}}
{{< figure src="./laser-outcome-02.jpg">}}

Unlike the original plan, using the bolt and nut to fix the string onto the wooden plate is inefficient. So, I decided again to use the adhesive instead of the bolt and nut. However, I needed to use two plates to attach together with the string into them if I use the glue. As the result, the wooden plates module had more weight and made a dull sound. 

How to make the Jacobs Ladder 01 (I firstly followed the method with two attached plates in the reference video below.)
{{< youtube id="_SwKHP0VyVA" >}}

{{< figure src="./attachment.jpg">}}
{{< youtube id="5l44maD59p0" title="Wooden plates material test" >}}

##### [2] Explore the wood materials

After my first try of the flipping wooden plates, I am deciding to use another way to make the Jacobs Ladder in the reference video below. This method is not to attach the two plates together to connect the string. Instead, the wooden plate has more thickness so I assume that I need to find the lighter wood materials.  

<How to make the Jacobs Ladder 02>
{{< youtube id="VgTfken5c6Y" >}}

* Things to explore
    - Foley studio in Aalto Studio: to check the sound of the wood materials
    - Veneer finish: If I use this wood veneer, I will use a glue to attach the two wood plates with the string.
    - Timbers
        {{< figure src="./timber-01.png" caption="[PLANED 18 X 70 MM SH OK]">}}
        - PLANED 18 X 70 MM SH OK; SH micro-chamfered, knotty spruce https://www.bauhaus.fi/lauta-hoylatty-18-x-70-mm-sh-ok.html
        - PLANED 18 X 95 MM SH; SH micro-chamfered, knotty spruce https://www.bauhaus.fi/lauta-hoylatty-18-x-95-mm-sh.html
    
* **Fixed wood material**
    - W X H: 40mm x 100m (thickness: 5mm) 
    - https://www.bauhaus.fi/hobbylista-maler-manty-puuvalmis-5-x-40-x-1000-mm.html 
    - Try to use the staple gun to fix the string on the wooden plate.


&nbsp;

#### / Holding Structure /
I am planning to use laser cutting with the plywood and use the timber.
Also, some parts will be made with the 3D printer.
{{< figure src="./HoldingSketch.jpeg" caption="[The sketch for the holding structure is ongoing]">}}



&nbsp;
______
### **TECHNICAL MAKING**

#### Technical Components
The following list is all the technical components that I used for building the Flipped Plate previous project. In the previous version, I used the sensor to detect the human temperature to trigger the movement of the object, and the detected range was too broad. However, I will develop this range to be narrower and more accurate. 

> - 1 x Arduino Uno
> - 1 x Breadboard
> - 1 x Qwiic Shield (SparkFun)
> - 1 x Stepper motor (Bipolar)
> - 1 x Sensor (SparkFun Grid-EYE Infrared Array Breakout - AMG8833 (Qwiic))
> - 1 x Stepper Motor Driver (DRV8834 Low-Voltage Stepper Motor Driver Carrier)
> - 2 x Resistor (10kΩ)
> - 1 x Capacitor (1000uf, 10v)
> - 1 x Qwiic Cable (100mm)
> - 1 x Power Adapter (5V)
> - 2 x Microswitch
> - 1 x Metal Rod Bolt (40mm)
> - Nuts, Bolts

Additional components to explore
- Omron D6T-8L-09H; 1x8 / 4x4 (but, 8x8 Grid-EYE has more resolution) 
    - [Link to the component](https://www.digikey.fi/en/products/detail/omron-electronics-inc-emc-div/D6T-8L-09H/10716557?s=N4IgTCBcDaICIDYAqBaAHAGRQBgJwAkQBdAXyA)
    - [Link to the datasheet](https://omronfs.omron.com/en_US/ecb/products/pdf/en_D6T_catalog.pdf)
    - Power supply voltage: 4.5 to 5.5VDC
    - Operating temperature range: 0 to 60°C
{{< figure src="./omron.png" caption="[Omron 4x4]">}}
{{< figure src="./omron_spec_all.jpg" caption="[Viewing angle and measurement area]">}}
- SparkFun IR Array Breakout - 110 Degree FOV, MLX90640 (Qwiic) https://www.sparkfun.com/products/14843 
{{< figure src="./MLX90640_110.jpeg">}}
- SparkFun IR Array Breakout - 55 Degree FOV, MLX90640 (Qwiic) https://www.sparkfun.com/products/14844
{{< figure src="./MLX90640_55.jpeg">}}


#### Breakout Board
- how I can integrate the stepper motor?
    - Trinamic Silent StepStick (TMC2208) to control the stepper motor


&nbsp;&nbsp;

-----
#### Project guideline

- What does it do?  
- Who's done what beforehand?  
- What did you design?  
- What materials and components were used?  
- Where did they come from?  
- How much did they cost?  
- What parts and systems were made?  
- What processes were used?  
- What questions were answered?  
- What worked?  
- What didn't?  
- How was it evaluated?  
- What are the implications?  
- At the end of the course you should have a summary slide and one-minute video explaining the conception, construction and operation of your final project.

- Your project should incorporate:  
2D and 3D design,  additive and subtractive fabrication processes, electronics design and production, embedded microcontroller interfacing and programming as well as system design and packaging.  