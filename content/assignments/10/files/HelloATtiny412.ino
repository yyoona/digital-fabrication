#define LED_PIN 2

int blinkDelay = 1000;

void setup() {
  pinMode(LED_PIN, OUTPUT);
}

void loop() {
  pinMode(LED_PIN, true);
  delay(blinkDelay);
  digitalWrite(LED_PIN, false);
  delay(blinkDelay);
}
