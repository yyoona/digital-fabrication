---
title: '09 / Computer-Controlled Machining'
---
______
## Task

Do the following to complete the assignment.

* Participate in one of the introduction sessions or watch instruction videos.
* Pay attention to what runout, alignment, feedrate and tool paths are.
* Design, CNC mill and assemble something big (meter scale). You will get a 1200x1200x18 mm sheet of plywood for your assignment. Make use of it.
* Document your process in a new page on your website.
* Add Downloadable design files to your documentation page.

&nbsp;
______
## Assignment 09 / Computer-Controlled Machining
{{<figure src="./hero-shot.jpg">}}


&nbsp;
______

### **Design**

When I thought about making something big as the assignment, the shoe rack is the first thing that came up with in my mind. That is because it was hard to find a suitable design and size for my home. Thus, it was a really good opportunity to make my own design for it. 

&nbsp;
### / Inspiration and Sketch /

I explored different styles of shoe racks and got inspiration from Pinterest. After the exploration, I decided to use the circular wood stick (12mm Diameter, 100mm length, beechwood) that can be found easily in the material shop. The sticks will be the layers to put the shoes on so that the layers can be changed depending on the use.

{{< figure src="./inspiration.png" caption="[Inspiration through Pinterest]" >}}

{{< figure src="./sketch.JPG" caption="[Hand drawing for the sketch]" >}}

&nbsp;
### / Modeling with Fusion360  /

- Modeling; make a shape and define the parameters for the shoe rack design
- Milling test the joint and the hole size
- Align the final design bodies on the material sheet
- Export the DXF file to go on the CNC milling process

**Modeling**

In order to model for the object to be milled by the CNC milling machine, I used the Autodesk Fusion360 because it is easy to vary the dimensions with the parameters. Through the functions of Sketch, Extrude, Mirror, and Pattern in Fusion360, I modeled the box shape without the front face. Also, I used finger joints for the way to assemble each face. The last part of the modeling was to make the holes on the left and right faces to fix the wood stick inside them.

{{< figure src="./Design_01.png" caption="[Parameters and the final design in Fusion360]" >}}

**Milling test for the finger joints and the holes**

After I modeled the basic design for the shoe rack, I needed to test the finger joint and the hole size by CNC milling with the wood material. I used leftover plywood and the material thickness (14.8mm) was thinner than `the actual material thickness(17.7mm)` that I will use for the final milling. So the test result has some gaps in the joint parts. To make sure the joint part with the same thickness as the actual material, I tested it again with the same thickness plywood sheet. Meanwhile, I also checked the hole size for the wood stick to be fixed by testing with the five different diameters' holes. According to the result, the hole size (diameter) for the final design was fixed at 12.50mm. Basecally, in addition, I generated the `toolpaths with the VCarve`, which is in the process of CNC milling, and used the `'Dog-Bone' Fillet` for the angled corner to change the corner into a circular fillet. I chose the Dog-Bone fillet as a safe way to fit all the joints well because I have many joints.

What is the toolpath in CNC? (Reference → https://bobcad.com/2d-toolpath-why-you-need-it/)
- The word “Toolpath” is a CAD/CAM related term that is basically a series of coordinate locations that a cutting tool will follow in the machining process. Toolpath is traditionally divided into two categories: Roughing and Finishing.

The general fillet types are 'Dog-Bone' and 'T-Bone'. 
* Dog-Bone: used for creating clearance in internal corners to allow slotted pieces to fit together. The Dog-Bone goes all the way to the corner. 
* T-Bone: used for creating clearance in internal corners when the slot is the same size as the tool. The T-Bone goes only one side of the corner so some people find this for a better aesthetic purpose. 

{{< figure src="./fillet.png" caption="[Fillet types: Dog-Bone(left), T-Bone(right)]" >}}

{{< figure src="./Design_02.jpg" caption="[Preparing for the joint test in Fusion360]" >}}

{{< figure src="./Design_03.jpg" caption="[Testing the joint and deciding the parameter for the hole size]" >}}

**Align the final design bodies on the material sheet**

Based on the basic design with the specific hole size, the final design was fixed. The next step was to align each face on the one face. To progress this step, I simply moved each body of the face and changed the angle. In this step the Align function in Fusion360 was useful.

{{< figure src="./Design_04.png" caption="[Alignment process in Fusion360]" >}}

After making the arrangement including the whole bodies (faces) of the shoe rack, the final process is to export the DXF file. The DXF file is used in VCarve.
{{< figure src="./Export.jpg" caption="[Exporting process in Fusion360]" >}}

&nbsp;
______

### **CNC Milling Process**

&nbsp;
### / Recontech1312 Composition /

{{< figure src="./cnc-01.jpg" caption="[Rcontech1312 vacuum table]" >}}

In Aalto Fablab, the model of the CNC milling machine is **Recontech1312**.
The machine is built in to a room for damage problems and the soundproof. The machine's milling tool is mounted on the x, y, and z-axis and rotated. This machine features a vacuum table. The holes on the vacuum table are where the air flows. With the vacuum pump, the vacuum-activated surface is divided into two sides; left, and right. Those can be activated separately by the valves.

{{< figure src="./cnc-02.jpg" caption="[Vacuum pump and the seperated valves for activating the vacuum]" >}}

The machine controller to control the machine is connected to the computer (the computer is outside of the room as well). The computers are connected to the network with the ethernet cable. On the computer, you can upload the G-Code. 

{{< figure src="./cnc-03.jpg" caption="[Machine controller and the computer for the CNC milling machine in the room]" >}}

&nbsp;
### / Material /
- Pine plywood (outdoor plywood) → We use this material for the assignment. 
- With this machine, wood and soft material like a form are available
- Handling material wood always needs us to wear gloves.
- Measuring the material: Width x Height / Thickness
    - we need to take care of some properties that we will need in the process of the toolpath generation process.
    - we should know about the properties of the tool.
    - We need to check the thickness of the material with the caliper. Even if the material is supposed to be 18mm thick, the actual thickness is different because of the quality of the material. (shrink, compressed, or bend...) → The actual thickness was 17.64mm. Write down the actual thickness.


&nbsp;
### / Tools /
- Different tools in general
    - Drilling tool for wood (Left in the picture below): It can go only vertical way.
    - Metal drilling tool (Middle in the picture below): It can go only vertical way. Good for pulling chips out. 
    - Milling tool (Right in the picture below): Two flutes flat end milling tool. Good at going to the side-way. Cut and engrave on the surface of the material. But this tool is not good at going downward. If we want it to go downward, it needs a circular motion (not good at chipping the material vertically) → We focus on this tool during this weekly assignment.

{{< figure src="./cnc-04.jpg" caption="[Tools; drilling tool for wood, metal drilling tool, and milling tool]" >}}

- From the drawer, we can find more tools.
    - The tools have a label with the serial numbers (First number: diameter of the cutting part of the tool)
    - We can find more information about the tool from the paper on the wall and we need to look up the table or use the caliper to measure the tool.
        - Flute Diameter
        - Flute Length
        - Number of Flutes: important for Fusion360
        - Shank Diameter: important for mounting the tool
        - Full Length: it gives us an understanding of how much we can move along the z-axis. 
    - Interesting geometry on the tool: So-called down-cut tool (When it's cutting, it is going to be pressing the chip down so it's very good for cutting the thin material (For plywood, it is not used to use it for the top surfaces but if cut very nicely then, use this tool to cut the top surface first, and then rest of them with other tools)

{{< figure src="./cnc-05.jpg" caption="[Tool information and measuring tools]" >}}

- Write down the parameter of the tool which we are going to use. (diameter, number of flutes)
    - The number of flutes: 2 flutes on the bottom. These are the cutters of the tool.
    - Diameter: 8mm

In the picture below, these are all the data that we need for now in order to start setting up the toolpath.

{{< figure src="./cnc-06.jpg" caption="[Note for the data to set up the toolpath]" >}}

&nbsp;
### / VCarve (to generate the toolpath) /

Before operating the milling machine, we need to use a computer. 
Start the software, VCarve Pro to generate the toolpath. (FreeCAD or Fusion360 are also available to generate the tool path. The same principles are applied to all of them) Start the VCarve by importing the DXF file.

- Job Setup
    - Job type: Single-sided
    - Job Size
    - Z Zero Position: Material Surface (In the case of a PCB milling machine, the machine needs to set the origin on the top of the material. But, CNC milling machine needs to set the origin on the table; on the machine bed)
    - XY Datum Position
        - South-left corner for the x, y origin point (Same as PCB milling machine).
        - Make sure to uncheck the Use Offset (It can cause some issues, and uncertainties in the layer).

{{< figure src="./VCarve-01.jpeg" caption="[Job Setup in VCarce]" >}}

- If the job files are already ready, import them into VCarve with DXF file format. Otherwise, we can draw something in the VCarve. 
    - With the imported object, we can move it around (Transform Mode - move, scale, rotate selection) and measure the size with the Dimension or Measure tool. 
    - We can use also other functions. (Alignment Tool, Copy and Paste, Weld, Subtract, and so on)
    - Create the Fillet for the corner part in order for all the joints fit to each other well ('Dog-Bone' or 'T-Bone' Fillet)

{{< figure src="./VCarve-02.jpeg" caption="[Drawing tool, Fillet → Dog-bone menu]" >}}

- After the processes above are done, save the file somewhere.

{{< figure src="./VCarve-03.jpeg" caption="[Save the file after the Fillet process]" >}}

- Generate the toolpath so that the machine can understand how to move the tool in order the shape out. For generating the toolpath, use the Toolpaths menu. (disable the auto-hiding)
- Material setup: If we forgot something to do, then we can always go back to material setup and edit some fine values. 
- Toolpath Operations
    - Start with the 2D Profile Toolpath. We can also use a cutting toolpath.
    - Cutting Depths: how deep our cut is going to be
        - Start Depth (D): top of the material → 0mm
        - Cut Depth (C): same as the material thickness or add a little bit. (+0.2mm) → 17.9mm (My actual material thickness: 17.64mm)
    - Tool: End Mill
        - I chose an 8mm tool.
        - Select
            - Geometry: 8.0 (for the cutting geometry diameter)
            - Cutting Parameters
                - Pass Depth: (depending on the chip load, specify half of it to make it in a safer way.)
                - Stepover (Important!! for the clearing the pocket. How many mm or percent of tool diameter we want to hop over in order to clear the certain part)
            - Feeds and Speeds (the most important part in CNC milling. Able to calculate this.)
                - Spindle Speed: 16000 r.p.m (maximum ~ 18000 r.p.m)
                - Feed Rate: the speed of x, y movement (maximum ~ 20000mm/min)
                - Plunge Rate: the speed of the vertical movement
                    - vertical movement is tricky in the milling machine process, so, 1/4 ~ 1/5 of the feed rate. 
            - Tool Number (number of flutes): 2
            - Parameters about the machine: https://www.cnc.fi/recontech-1312.html
    - Machine Vectors (Now we need to assign how we cut among 'outside' or 'inside')
        - Select the shape we want to cut
        - Choose Outside
            - Direction
                - Climb: More pressure. Try to climb the edge 
                - Conventional: More aggressive. Less pressure
    - Skip Do Separate Last Pass
    - Add tabs to the toolpath (Important!)
        - In order to prevent the finished parts from flying out from the material that was there fully cut. 
        - We can specify the size, depending on the material. If more dense material, the smaller tabs we can actually leave. If soft, bigger tab.
        - We can also edit them.
            - We can generate a Constant Number of tabs.
            - The tab on the corner is hard to fly them away. So just put them manually. 
    - Ramp → advanced part. 

{{< figure src="./Toolpath-01.jpeg" caption="[Profile Toolpath and menu in the 2D profile Toolpath fuction. In the picture, the job was for the test (14.8mm plywood)]" >}}
{{< figure src="./Toolpath-02-2.jpeg" caption="[Add Tabs]" >}}

> **How do we calculate the feed rate?**
> - The tool that we are going to use comes from a certain manufacturer. So the manufacturer usually provides the table (guide values for milling parameters) which are specifying a chip rod or a certain diameter of their tools for different materials.
> - Parameters about the machine: https://www.cnc.fi/recontech-1312.html
> - With the Laser cutter as the starting point, do a small test in order to find out what is the best setting for you.
> - Amount in the mm that each of the teeth of the tools is going to be cut per revolution (It means to move forward. It rotates and moves forward at the same time.)
> - **In order to calculate the feed rate.** 
>   - **Chip rod: 0.07 mm**
>   - **Number of flute: 2 n**
>   - **Spindle speed: 16000 rpm**
> - **Feed Rate = chip rod x number of flutes x spindle speed = 2240 mm/ minute**
> - **Plunge Rate = Feed Rate / 4 = 560**

{{< figure src="./Toolpath-01-3.jpeg" caption="[Define the Tool Database]" >}}

- Name
    - Give the job the name → Calculate → Then the warning pop up about cutting the machine bed. We should attention to this. 

- Preview
    - How the tool is going to cut. We can see what the tabs are.
    - Blue line: the actual toolpath
    - The center is going to move along this path.

- If we have a pocket, the process below needs to be gone through from the 2D View again.
    - Select Pocket Toolpath.
        - Cut Depth: 7mm
        - Tool: (same tool. End Mill 8mm) Setpover is important here. 40% of the tool diameter is fine. 
    - Clear Pocket (how this pocket is cleared)
        - Preview. Start from the center. Spiral way. Zigzag motion. Clear the final edge in the end. 
        - Cut Direction: Climb
        - Specify the profile pass: Last, for generating less stress on the machine
    - Ramp: uncheck
    - Name 
    - Select Vector 
    - Calculate

{{< figure src="./Toolpath-03.jpeg" caption="[Preview Toolpaths]" >}}

- Save for the Toolpaths
    - Select two (Outlines / Pocket)
    - Save toolpaths
        - If we use the same tool, then Output all visible toolpaths to one file
        - If we use the separate tools, then we would rather separate them in the separate files
        - PostProcessor
            - Mach2/3 Arcs (mm) →  we select this
            - Grbl: if we are building our own Arduino-based CNC milling machine. 
            - Jit save Toolpath(s).
        - Naming: What kind of job it is. Use T8mm, Thickness of material, Depth of cut... (If we use multiple tools, give a hint to the naming)

{{< figure src="./Toolpath-04.jpeg" caption="[Save Toolpath]" >}}

&nbsp;
### / Set up the material /

- Use vacuum table capabilities.
    - We can open a few bends according to the material size. 
    - The area that could be used for the vacuum to hold the material down. 
    - Use the different chambers on different sides (left / right).
- Open the bend. (The caps should go into the yellow box, next to the machine.)
- Insert the rubber string and closed the cap. (left-top)
- Put the sacrificial layer on the vacuum table. 
- The reason why we put the material on top of the vacuum system is to block it. The air is actually flowing through the MDF.  The material we put on the MDF is going to be held down because the air is going to flow through it. 
- In order to fix the material firmly, use the screw and the additional metal tools.

{{< figure src="./SetupMaterial.jpeg" caption="[Set up the material on the sacrificial layer]" >}}

&nbsp;
### / Operate the Recontech1312 /

After the VCarve process is done, it is time to turn on the CNC milling machine, Recontech1312. We have to make sure that we need to remove the gloves whenever we are doing with the machine itself.

* Turn on the machine with the switch to enable the power supply and press the green button. After that, wait for 15 seconds.

{{< figure src="./Operate-01.jpeg" caption="[Start operating Recontech1312 with the power switch and the green button]" >}}

* On the desktop, Click Mach3 and hit OK. (Mach3 CNC control software is a platform that turns most windows PCs into a CNC machine controller that manages the motions of motors and generators.)
    - On the interface of Mach3, there are three things to click before setting up the machine.
        1. `Reset`
        2. `Soft Limits`: Carvo is on, so it's the green line.
        3. `REF ALL HOME`: For all of the axis, make sure that you are at a safe distance from the machine. Because it's gonna start to move. 

{{< figure src="./Operate-02.jpeg" caption="[Start Mach3 on the computer and view of the Mach3]" >}}

* From now on, we shouldn't be able to move the machine around. But, we can only use the arrow key on the keyboard. The laptop computer from outside of the room is also connected to the keyboard in the room so that we can use the laptop keyboard to control the arrow key. (**IMPORTANT!!** Make sure that anyone does not touch the keyboard while you are working.)

{{< figure src="./Operate-03.jpeg" caption="[Control the arrow key]" >}}

* Prepare the tools
    - Firstly, we need to remove the dust shoe (vacuum brush part) in order to access the actual tool holder. (do not drop it)
    - Remove the **nut** of the tool holder. 
        - This nut has a geometry that allows it to hold down the collet which is a work-holding device that is used to keep an object in place. 
        - In the Aalto Fablab, there are several different sizes of collets so that we can allow fixing the different tools properly (position the tool holder where we can easily access it).
    - Get the **collet** from the top shelf of the drawer.
        - Number means the diameter of the shaft that can support.
        - Since we are going to use the **8mm tool**, we need to choose the number 8.
        - We can find the tools (Wrench)  in order to set up the collet together with this tool.
    - Put the sponge (soft pad) under the tool holder, in order to prevent the tool and the surface of the material to get damaged too much, in case of the tools fall down. 

{{< figure src="./Operate-04.jpeg" caption="[Remove the nut]" >}}
{{< figure src="./Operate-05.jpeg" caption="[Prepare the tools; collet and the drill bits, and other tools like wrench keys and the soft pad]" >}}
{{< figure src="./Operate-06.jpeg" caption="[Information sheet for the drilling and milling tools]" >}}

* Install the tools
    - Insert the collet into the nut.
        - Put it in, applying a little bit of force to hear the clicking sound. As a result, make sure that the surface of the collet is aligned with the bottom surface of the nut. Thus, these should be paralleled.
        - Put it and screw it onto the tool holder until we feel that counter pressure. 
    - Put the 8mm milling tool 1cm-1.5cm deep inside of the collet (after that, no need for extra movement).
        - Use a bigger wrench key (32mm diameter) to hold the top part of the tool holder.
        - Wrench key ER32 is the standard for the collet. With this, adjust the tool holder (shouldn't put too much pressure. If with much pressure, hard to remove the tool later)
        - Put the tools (keys) back and remove all unnecessary objects from the machine.

{{< figure src="./Operate-07.jpeg" caption="[Install the collet and milling tool]" >}}

* Set the Z-origin with the tool by using the Z-origin sensor on the left side of the machine.
    - Take the sensor tool and put it on the location onto the sacrificial layer.
    - Prepare to activate the vacuum pump by opening the chamber (mark O is opened). Because, the material is going to be fully stuck on the table, and it is going to be approximately 1 mm lower than when the vacuum pump isn't activated.
        - Before activating the vacuum pump, make sure that how the valves are arranged. Because one of the vacuum pumps; left or right should be activated depending on the cutting area of the material. If we use both sides, we can activate the vacuum pumps for both sides.
    - The Mach3 software knows the height of this. Auto operations. The machine detects the current Z-location and subtracts the height of the sensor from it. And then, set that to the zero-position.
    - Use the soundproof earmuffs because the vacuum pump is very loud.
    - In order to activate the vacuum pump, use the toggle button on the controller.
        - Press the button → turn the vacuum pump on
        - press the button again → turn it off
    - Move the machine above the sensor.
    - On the screen, look for the only Finnish label button `Teran mitaus`. Press it then start setting the origins to 0.
    - Press the white toggle button to Turn off the vacuum pump.
    - Put the sensor back in the place and continue for operation.
    - Move the tool away so that we have more room to set up the material.

{{< figure src="./Operate-08.jpeg" caption="[Set the Z-origin (In the picture, there is the only sacrificial layer. It should have the actual material to be cut.)]" >}}

* Remember to set the X/Y origin as well.

* Put the dust shoe (vacuum brush part) in place again by opening the set screws. Also, use the air-gun to get rid of some dust on the surface.

* Activate the vacuum pump and exit from the room.

{{< figure src="./Operate-09.jpeg" caption="[Set the X/Y origin, use the air-gun, and activate the vacuum pump (In the middle picture, there is only sacrificial layer. It should have the actual material to be cut.)]" >}}

* Load the G.code. 
    - After the loading, we can see the information from the G.code and a little preview of the toolpath.
    - Preview
        - Grey area: work area
        - Yellow: where the tool is currently and where the starting point
        - Blue line: toolpath

{{< figure src="./Operate-10.jpeg" caption="[Load the G.code file in Mach3]" >}}

* Start the job by pressing the Green button.
    - In order to start the job, need to use the button interface next to the laptop
        - Top part: only light
        - Bottom part: buttons
        - Green light: the spindle is not spinning
        - Red light: the spindle is spinning so it's dangerous
        - Green button: start the job
        - Red button: stop it (if we need to stop before the job end, press the Red button. The Red button is also linked to the door mechanism. Whenever we open the door, the job and the spindle stop.) The spindle needs 10 seconds to stop until it visually stops.

{{< figure src="./Operate-11.jpeg" caption="[Start the milling by pressing the Green button]" >}}

* Start and wait until the job ends. If the job ends, the light changes to green again.

{{< figure src="./Operate-12.jpeg" caption="[The job is starting and the finished result for testing]" >}}

* If the job ends, turn off the vacuum pump in the room and put aside the material for the post-processing.

* Before leaving the room, make sure that the tools are left in order.
    - Remove the bits from the machine, and move the machine forward.
    - Move the dust shoe.
    - Open up and unmount the tool by grabbing them in one hand.  (Use the soft pad; sponge)
    - Use the compressed air gun to blow in the collet to clear the dust for the removal motion
    - Push the top side of the collet with the thumb for removal and put the collet back into the place
    - Put the keys back, put the tool in the box, and put it back to it belongs
    - Mount the nut back up
    - Use the vacuum cleaner to clean the dust in the room.
    - Put the sacrificial layer in the right place (not to put your project here)

{{< figure src="./Operate-13.jpeg" caption="[Where to put the sacrificial layer]" >}}

* Turn off the machine
    - Start with the closing down of the software.
    - Press the Red button on the machine controller and wait for a few seconds the number goes down and hear the click sound. 
    - The main switch → Rotates it to the off position.

&nbsp;
______

### **Post-process**
Post-processing should be processed in the wood workshop space. After the post-processing, it is good to calculate the dimension so that we can check the 

&nbsp;
### / Take the parts apart from the material /
- Use the manual tool and hammer to remove the tabs.
- Use the hand router to make the edge flatter (the tools can be found on the bottom shelves in the Blue cabinet).
    - Hand router (connect to the vacuum cleaner). 
        - Turn on the machine. (2 switches should be turned on on the vacuum cleaner)
        - Set the speed (red part. max 6 - min 1)
        - Use this tool to cut the tabs on the bottom side of the object. 
        - Use the clamp.
        - Place the tool on the edge before turning it on.
    - After it's done, put the tools back in the place.

{{< figure src="./Assemble-01.jpeg" caption="[Tools for remove the tabs]" >}}

&nbsp;
### / Sand and assemble the parts /
- Sand the surface of the parts with sand paper. I sanded about 2-3 times (rough → fine)
- Assemble the sanded parts. My parts have very tight joints so I needed to use the hammer.
- Put the pre-made wood sticks into the holes. (I bought the wood sticks from the material shop at the price of 1.4 Euros for each)

{{< figure src="./Assemble-02.jpeg" caption="[Tools for sanding and assembling]" >}}
{{< figure src="./Assemble-03.jpeg" caption="[Putting the wood sticks in the holes]" >}}


&nbsp;
### / Paint the wood oil on the results /
After assembling, the final process is to paint the oil on the surface of the result. I made the shoe rack to be used at home (indoor space). So I used the wood oil for indoor use. This process should be processed in the ATEX-painting room. I painted this oil about 2-3 times and did the sanding process again in between the oiling process.
- Take the oil from the top shelve in the blue cabinet of the wood workshop. 
- Take the stick for mixing and stir the oil several times. 
- Use the fabric sheets to paint the oil on the surface of the wood. 
- Wait the oil dries. (12 hours is the guided dry time. But it can be flexible depending on the condition).

{{< figure src="./Assemble-04.jpeg" caption="[Paint oil for the wood surface ]" >}}
{{< figure src="./hero-shot.jpg" caption="[The shoe rack is finished and in use]" >}}

&nbsp;
______
### **Original Design Files**
- Dxf file for the final desgin of the shoe rack → [All.dxf](./files/All.dxf)
- Dxf file for testing on the joint and hole size → [hole-test.dxf](./files/hole-test.dxf)

