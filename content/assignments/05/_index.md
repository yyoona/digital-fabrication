---
title: '05 / Computer-Controlled Cutting'
---
______
## Task

Do the following to complete the assignment.

- Characterize your lasercutter's focus, power, speed, rate and kerf.
- Cut something on the vinyl cutter.
- Design, lasercut a parametric press-fit kit that takes into account the kerf and can be assembled in multiple ways.
- Document all of the points above in a new page on your documentation website.

&nbsp;
______
### **Characterize lasercutter's focus, power, speed, rate and kerf**

The object which I plan to make with the laser cutter has two different parts. One is the wooden plates that will keep flipping. The other is the structure holding the motor and the PCB board. This structure is also connected to the wooden plate by the rotation shaft.
- Wooden Plates
- (Holding Structure -> work in progress...)

&nbsp;

#### Wooden Plates
- Material: 2mm thickness plywood
- Cutting for the 2mm holes and the rectangular plates (Offset; kerf applies only for the holes)
- Engraving for the position of the string

Each plate has six holes for the screws (2mm's bolts and nuts) that will fix the string to the plates. Also, the string needs to be fixed on right position of the plate. Some plates need a string to be fixed in both ends of them. Other plates need a string to be fixed in the middle of them. So I decided to use ENGRAVE in order to mark the string's position on the plate. 

{{< figure src="./wooden-plates-ideation.JPG" caption="[Sketch for how to fix the string to the plates. Dotted line is the string that is fixed to the back-side]">}}

{{< figure src="./Wooden-plates-ai.png" caption="[Wooden Plates Sketch on Illustrator]">}}

To decide the set-up detail for cutting and engraving, I tried to make the test illustrator file. The different colors are used to test the different speeds of Engraving. Also, the hole’s kerf compensation is applied by the offset setting on Illustrator. As a result of the laser-cutting test, I specified the set-up detail below.

- Cutting Set-up: Speed 38.0% / Power 60.0% / Frequency 50.0% / kerf 0
- Engraving Set-up: Resolution 300 / Speed 10.0% / Power 50.0%

{{< figure src="./test.jpg" caption="[Test for the laser cutter's detail]">}}

&nbsp;

#### Holding Structure (WIP)

&nbsp;
______
### **Cutting with the vinyl cutter**

&nbsp;

#### Plan for the vinyl sticker in the final project
Sticker to put on the box in which the wooden plates are stored. Depending on the project, the box is different in case the plates' sizes are different. So the sticker shows the dimension of the plates in the box.

{{< figure src="./vinyl-sketch.png" caption="[Sketch for the vinyl sticker that will be attached to the wooden plates' store box]">}}

&nbsp;

#### Make the image for the vinyl cutter
* Refine the line of the image 
* Create an outline for the font
* Flip the image horizontally in case of using the heat transfer sticker material

Firstly, I tried to make the sticker image to test the vinyl cutter with the heat transfer sticker in order to attach it to the fabric bag. I made the image of the sticker through Adobe Illustrator. After the image is done, the font should be checked if the font is converted to outline. Also, the vinyl cutter couldn’t accept the complicated design so I made the image more sophisticated and used a thin line (0.01mm). To check the lines, command Y can be used as a shortcut of showing the stroke. I firstly used the font size 6.5pt but the font was too small to take the unused part of the sticker out from the entire sticker after the cutting process. In the end, I enlarge the sticker image itself so that the font size also becomes bigger and thicker.

{{< figure src="./vinly-process-01.png" caption="[Making the image]">}}

&nbsp;

#### Operate the vinyl cutter
The vinyl cutter machine's model in Aalto Fablab is Roland GS-24. The image below is from the offical online manual of the Roland GS-24. 

{{< figure src="./gs-manual.png" caption="[Roland Vinyl Cutter image and the part names]">}}

&nbsp;
Process | -
------- | ------ 
**Choose the material** [I chose the heat transfer vinyl among three materials; standard sticky vinyl, heat transfer vinyl, copper vinyl] | {{< figure src="./vinyl-process-01.jpg">}}
**Check the blade** [Normally, the default setup is okay to follow without any adjustment. But, the blade can be changed depending on how much extrusion is needed by rotating the cap.] | {{< figure src="./vinyl-process-02.jpg">}}
**Fix the vinyl material into the machine** [* Push the lever first and control the roller by the backside of the machine. (The first roller should be in the left white part, the second roller can be in the position of the other white parts. The machine measures only between the rollers, so the rollers should be positioned on the edges of the material. If the material is too small then an error message will show up on the screen.) * Before fixing the vinyl, make the material flat with the plastic scrapper. * If the heat transfer vinyl is used, the mat side should be up for the cutter. (But for the press machine, the glossy side should be up)] | {{< figure src="./vinyl-process-03.jpg">}}
**Turn on the machine and Select the sheet type and enter to measure the size of the material** [The sheet type: Roll, Piece, Edge (Edge measures only width. If you want to use a long piece of material and want to make a small thing, then the edge is the proper option)] | {{< figure src="./vinyl-process-04.jpg">}}
**Press the TEST button more than one second, adjust the blade again, press the ORIGIN button to set and test again** | {{< figure src="./vinyl-process-05.jpg">}}
**Go to the computer to load the prepared image on the artboard (same size as the material size) on Illustrator and go to the Roland CutStudio (Window in the illustrator's top menu ⇢ Extensions ⇢ Roland CutStudio)** | {{< figure src="./vinyl-process-06.jpg">}}
**Select *Output Selected Lines*. Also, *Move to origin always* should be checked for the cutting to start from the edge. Then, click *Output the Paths* ⇢ click *Cut*** | {{< figure src="./vinyl-process-07.jpg">}}
**After the cutting is done, press the MENU button on the machine, select UNSETUP, and then press ENTER to confirm (the Cutting Carriage; blue part should be all the way right). Finally, Use the gap to cut the material with the knife and turn off the vinyl cutter** | {{< figure src="./vinyl-process-08.jpg">}}
**Take the unused part out from the sticker with the sharp tools and tweezer** | {{< figure src="./vinyl-process-09.jpg">}}
**Attach the sticker to the fabric surface** [*If the standard vinyl, use the transparent tape to attach the sticker. * If the heat transfer vinyl, use the press machine.] | {{< figure src="./vinyl-process-10.jpg">}}

&nbsp;
#### How to attach the sticker to the fabric surface by the press machine
- Heat up the press machine beforehand
- Check the temperature by the Infra heat measurement. The temperature should be 130 degrees to press
- Press 3 seconds to flat the surface, put the sticker, and press again until the beep sound coming
- After the sound, take the fabric out and cool down
- If the different colors are used, press down 5 seconds for the first layer and press down ten seconds for the other color’s layer. The last layer should be ten seconds to be pressed.
- If the process is done, switch down.
{{< figure src="./press-process.jpg" caption="[Process of operating the press machine (Clockwise from the top-left)]">}}

&nbsp;

#### Outcome of the vinyl cutting
I’m planning to make the sticker to put on the storage box for the wooden plates. However, the storage box is part of the final process of this project. So, I first made the sticker to put the fabric bag which I usually use to carry anything including the project stuff.

{{< figure src="./vinyl-outcome.jpeg">}}


&nbsp;
______
### **Design, lasercut a parametric press-fit kit that takes into account the kerf and can be assembled in multiple way**

&nbsp;

#### Design the press-fit kit
* Create the parametric object through Fusion360
* Import the object file into the Illustrator 
* Set the kerf compensation and the engraving part

I used the Fusion360 to create the parametric object to cut by the laser cutter. After making the object, I save the object as a DXF file to import it on Illustrator. This is because the file should be sent from Illustrator or CorelDraw to upload the file to the laser cutter. Moreover, I set up the kerf compensation of the holes which is part of the object through the path/offset function, and added the engraving part on Illustrator.

{{< figure src="./01-sketch.png" caption="[Make a sketch for the parametric object (Fusion360)]">}}
{{< figure src="./02-parameter.png" caption="[Set up the parameter (Fusion360)]">}}
{{< figure src="./03-use-parameter.png" caption="[Use the parameter to extrude for the thickness of the material (Fusion360)]">}}
{{< figure src="./04-save.png" caption="[Save the fusion file as a DXF file (Fusion360)]">}}
{{< figure src="./05-add-engrave.png" caption="[Finalize the object image for the laser cutting on Illustrator]">}}

&nbsp;

#### Operate the laser cutter
The laser cutter machine's model in Aalto Fablab is Fusion Pro in Epilog Laser.

{{< figure src="./FusionPro.jpeg" caption="[Laser cutting and engraving machine; FUSION Pro]">}}


&nbsp;
Process | -
------- | ------ 
**Turn on the light and ventilation switch first** [The ventilation switch doesn't need to be switched down after using the laser cutter.] | {{< figure src="./laser-process-01.jpg">}}
**Check the power supply** [In every 5 mins, it makes the beeping sound. Then, tag the card to the supply.] | {{< figure src="./laser-process-02.jpg">}}
**Go to the machine and power on by turning the key to the (-)** [Don’t open the lid laser cutting is moving.] | {{< figure src="./laser-process-03.jpg">}}
**Prepare the material to cut** (Measure it’s thickness and put it on the plate of the laser cutter.) | {{< figure src="./laser-process-04.jpg">}}
**Go to the computer and make a new file with Illustrator or CorelDraw** [The artboard size should be the same as the material size. Check if the unit is *mm*, the color mode is *RGB* and the resolution is *300ppi*] | {{< figure src="./laser-process-05.jpg">}}
**Load the image to cut** [Cutting is vector and Engraving is raster. So, the cutting outline should be a line with 0.01mm stroke thickness but the engraving part should be the shape without the stroke] | {{< figure src="./laser-process-06.jpg">}}
**Go to the Print setting menu for the machine. If the setting is done, click the print button** [Select the right printer, check the Ignore Artboards, uncheck the Auto-Rotate, and Set the Placement on the top-left corner.] | {{< figure src="./laser-process-07.jpg">}}
**Go to laser cutter software and position the image to cut on the loaded material through the dashboard** [The image can be moved with the mouse and the area to be cut can be managed by the pink dotted line] | {{< figure src="./laser-process-08.jpg">}}
**Set up the details(Focus, Speed, Power, Frequency, Resolution) for the cutting** [Load Material Library Item can be used to refer to the default setting especially for the Vector set. Also, the Auto focus should always be the Plunger. The Plunger is for the calibration of the gap between the laser and the material. For the engraving, the frequency doesn't need to be set.] | {{< figure src="./laser-process-09.jpg">}}
**After the setting, Click the button; Send to JM (Job Manager)** | {{< figure src="./laser-process-10.jpg">}}
**Click the button; Quick Print to upload the file to the machine** | {{< figure src="./laser-process-11.jpg">}}
**Go to the Laser cutter, check the file on the screen of the machine and press the play button to start cutting** [The screen of the machine is also useful to do the same job again. Also, the joystick is used to move the laser pointer's cutting start spot when the right side of the file list has the image on the screen.] | {{< figure src="./laser-process-12.jpg">}}
**After the cutting process is done, wait for 2-3 seconds and then open the lid to take the material out from the plate. Finally, turn off the machine by turning the key to the (o)** [In the Laser cutter process, engraving is first, and then cutting has proceeded.] | {{< figure src="./laser-process-13.jpg">}}

&nbsp;

#### Outcome of the laser cutting

For the prototype, I made twelve pieces of the plates so that the length of the total wooden plates for the test is 900mm. 
{{< figure src="./laser-outcome-01.jpeg">}}
{{< figure src="./laser-outcome-02.jpg">}}

#### ✧ ✧ ✧  
Tips
- When the multiple objects are to be cut, separate the objects and use the smaller board rather than the big board once. 
- Use the tape to make the sheet flat on the board of the laser cutter.  

&nbsp;
______
### **Original Design Files**
- Vinyl cutting design file → [Vinyl-Final.ai](./files/Vinyl-Final.ai)
- Wooden Plates design dxf file → [wooden-plates.dxf](./files/wooden-plates.dxf)
- Laser cutting ai file for the test → [wooden-plates-test.ai](./files/wooden-plates-test.ai)
- Laser cutting ai file 01 for the Wooden Plates → [wooden-plates-01.ai](./files/wooden-plates-01.ai)
- Laser cutting ai file 02 for the Wooden Plates → [wooden-plates-02.ai](./files/wooden-plates-02.ai)

&nbsp;
______
### **References**

- Roland Vinyl Cutter official manual → 
https://files.rolanddga.com/Files/GS-24_UsersManual/Responsive_HTML5/index.htm#t=GS-24_USE_EN_03_3.html

- Epilog Fusion Pro Lase Machines offical website → 
https://www.epiloglaser.com/laser-machines/fusion-pro-laser-series.htm