---
title: '03 / Project Management'
---
______
## Task

Please complete the following tasks.

- Create a page for the project management assignment on your website.
- Describe how you created your website on that page.
- Describe how you use Git and GitLab to manage files in your repository.

&nbsp;
______
### **How to create this website**

This website is built in the [GitLab ⇱](https://about.gitlab.com/), which is the DevOps Platform that combines the ability to develop, secure, and operate software in a single application. Through this platform, the website is able to be managed by the repository in the local computer.

The website's structure follows a static web page, in order to show the archived informative contents to viewers. Among the static web page generator, this website uses [HUGO ⇱](https://gohugo.io/). HUGO is a very fast framework for building a website. Based on HUGO, the website choose the theme, 'hugo-book’. This theme is proper for the documentation websites because of various functions related to the book’s form.

According to the theme, ‘hugo-book’, the ‘markdown’ files are generally used for this website’s structure. By the markdown language, the website’s contents are added more easily by the repeated particular styling and formatting method, such as putting several simple symbols to the plain text. Even better, the coding part for the contents becomes finer and easier to be checked in the Visual Studio Code, compared to than HTML-based website. Also, the website uses shortcodes to add image and video content. The shortcode is another effective way to make the website’s contents be flexibly added in the markdown files. With this flexibility, this website is planned to have experimented more with the layout and design within the theme's variations.

{{< figure src="./markdown.png" caption="[The markdown codes in Visual Studio Code for this website's contents.]">}}

This code for the website is developed with the editor, Visual Studio Code. After writing the syntaxes in editor, in order to update codes to the website server, Terminal(MacOS) playes a role to communicate between the web server and the editor. It also means that some processes are needed through the Terminal for the interaction between the GitLab repository (remote) and the local repository.
{{< figure src="./git.png" caption="[Communication between remote and local repository]">}}

&nbsp;
##### References

- GitLab basic guide → 
https://docs.gitlab.com/ee/gitlab-basics/

- Hugo basic guide → 
https://gohugo.io/documentation/

- Theme, 'hugo-book', Demo → 
https://hugo-book-demo.netlify.app/

- Hugo Shortcodes guide →
https://gohugo.io/content-management/shortcodes/

- GitLab Markdown guide →
https://docs.gitlab.com/ee/user/markdown.html


&nbsp;
______
### **How to use Git and GitLab to manage files in the repository**


&nbsp;
#### / STEP 1 /
#### Make new project in Gitlab; Create blank project
* Visiblity level should be 'Public'.
{{< figure src="./step-1.png" caption="[The page for Making new project]">}}

&nbsp;
#### / STEP 2 /

#### Git global setup with terminal
* Chose the right directory.
* Type below in Terminal.
~~~
git config --global user.name "yyoona"
git config --global user.email "yoona.yang@aalto.fi"
~~~

#### Generate an SSH key
The SSH means Secure Shell or Secure Socket Shell used for managing the networks, operating systems and configurations and also authenticates to the GitLab server without username and password each time. SSH keys are one of the choices for authentication against GitLab servers. `ED25519` SSH keys are generally recommended because they are more secure. 
* Generate an `ED25519` SSH key by running the following terminal command. The `-C` and the `comment in quotations` is optional, but should be used when generating more than one pair. Inside " ", type the email which is used for the user email on GitLab.
~~~
ssh-keygen -t ed25519 -C "yoona.yang@aalto.fi"
~~~
* After the above command, the following reponse should be seen.
~~~
Generating public/private ed25519 key pair.
Enter file in which to save the key (/home/user/.ssh/id_ed25519):
~~~
* After generating the pair, specify a passphrase by the following command.
~~~
Enter passphrase (empty for no passphrase):
Enter same passphrase again:
~~~
* The created public and private key file within the ./ssh folder. 

#### Add an SSH key to the GitLab account
In order to use SSH with GitLab, the public key should be copied to the GitLab account.
* Copy the contents of the public key file by the following command and replace `id_ed25519.pub` with the filename.
~~~
tr -d '\n' < ~/.ssh/id_ed25519.pub | pbcopy
~~~
* Sign in to GitLab and go to `Preferences` that can be found in the dropdown menu when cliking the avatar on the top bar, in the top right corner. After then, select `SSH Keys` on the left sidebar. 
{{< figure src="./preferences.png" caption="[Where the Preferences]">}}
{{< figure src="./ssh_keys.png" caption="[Where the SSH Keys menu]">}}
* In the Key box, paste the contents of the public key, type a description in the Title box, and Select `Add key`.


#### Create a new repository
* In the GitLab repository page, find a clone button, then copy SSH link. (`git@gitlab.com:yyoona/digital-fabrication.git`)
{{< figure src="./step-2.png" caption="[Clone button is in the top, right side of the GitLab repository page.]">}}

* Type below in Terminal
~~~
git clone git@gitlab.com:yyoona/digital-fabrication.git
cd digital-fabrication 
touch README.md
git commit -m "add README"
git push -u origin main
~~~
* `cd digital-fabrication` is used for going into the created folder 
* `touch` is used for making a new file

&nbsp;
#### / STEP 3 /

#### Start HUGO 
* Download HUGO zip.file as an extended version, only hugo file is needed among the files in unzipped folder.
* Check hugo version. If the upgraded hugo version is needed, replace the current hugo file to the new hugo file by just dropping the file into the folder.
* Type below in Terminal.
~~~
hugo version
hugo new site digital-fabrication
hugo serve
~~~
* `hugo new site digital-fabrication` is used for making the digital-fabrication folder again. The created files are needed to be taken out to the enclosing folder. 
* `hugo serve` is used for checking the current view of the wesite with the local network. Web Server is available at *http://localhost:1313/* . Sometimes, `hugo serve -D -F` can be used.

#### Apply HUGO theme
* Find the HUGO theme in HUGO site
* Need to check the hugo version depending on the theme
* ~~`git submodule add https://github.com/alex-shpak/hugo-book themes/hugo-book` // Install as git submodule~~
* ~~`hugo server --minify --theme hugo-book` // Local Web Server is available to check~~
* ~~`cp -R themes/hugo-book/exampleSite/content .` Create the content folder from scratch~~
* In 'config.toml' file, `theme = 'hugo-book'` should be added. This is processed in Visual Studio Code.
>>↓  
>>This is a tricky part. After typing submodule part, created 'hugo-book' folder has the different branch not as the enclosing folder (main repository)'s branch; origin/main. The way to fix is to delete the hugo-book folder and download the hugo-book theme zip folder from the hugo-book theme page. and then, insert the un-zipped folder into the local theme folder.

#### Update the current status
The below codes are the basic principle for communication between the Local Repository and GitLab Repository. If the contents in the Local Repository is changed, this basic principle is the way to command in terminal. 
* Type `git status` in order to check the latest status of the local repository.
* If there are the changes in the status. it's the time to stage all the changes. `git add .` is the command for that. But, if wanted to stage one specific file, then `git add [file]` instead of `git add .`.
* After staging, the commit is the next step by typing `'git commit -m "description"`. The description can be varied depending on the contents changed. 
* Finally, by the `'git push'`, the changed contents in the local repository are sent to the GitLab repository so that the GitLab website can be updated with the new contents.
~~~
 git status
 git add . 
 git commit -m "description"
 git push
~~~

#### Add yml file in gitlab (.gitlab-ci.yml, apply a template: hugo)
* yml file is needed to change to the extened file. The way to change is replace line#10 in yml file; `image: registry.gitlab.com/pages/hugo:latest` ⇢ `image: registry.gitlab.com/pages/hugo/hugo_extended:latest`
{{< figure src="./step-3.png" caption="[How to change the yml file into the extended file]">}}

#### Check the changed status with terminal
* After the yml file is changed, the Local repository has to be in the same status as Gitlab repository. So, start to type `git pull` first. 
* After `git pull`, some changes need to be made in the local folder. Then proceed to `git add .` and the following commands.
~~~
* git pull
* git add .
* git commit -m "@@@"
* git push
~~~
&nbsp;
##### ✧ ✧ ✧  
- HUGO Guide for quick start →
https://gohugo.io/getting-started/quick-start/
- HUGO Themes → https://themes.gohugo.io/themes/hugo-book/



&nbsp;
#### / STEP 4 /

#### Start inserting the actual contents of the website. 
* By adding some syntax in 'config.toml', apply the functions from theme according to the contents for website. From this part, Visual Studio Code is mostly used.
* `[params]` is for the Site Configuration. There are a few configuration options that can be add to the 'config.toml' file. This options can be found in 'config.yaml' or hugo-book theme page.
{{< figure src="./step-4.png" caption="[Adding some syntax to 'config.toml' is important process for starting the inserting the actual contents within the theme. ]">}}

&nbsp;
##### ✧ ✧ ✧  
- hugo-book/exampleSite/config.yaml → 
https://github.com/alex-shpak/hugo-book/blob/master/exampleSite/config.yaml



