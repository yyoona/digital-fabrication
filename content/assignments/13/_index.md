---
title: '13 / Machine Building'
---
______
## Task

Follow the steps below to complete your assignment.

* Design a machine that includes mechanism, actuation and automation.
* Build the mechanical parts and operate it manually.
* Actuate and automate your machine.
* Document the group project and your individual contribution.
* Include a hero shot and link to machine repository in your documentation.
* Submit a link to your assignment page here.

&nbsp;
______
## Assignment 13 / Machine Building
{{<figure src="./pilvitheplotter.jpeg" caption="[How to use platform in Pilvi the Plotter; 2D Plotter machine]" >}}

Link to machine repository → https://gitlab.com/aaltofablab/machine-building-2022a


&nbsp;
______

### **Group Work: Pilvi the Plotter**

Our group was working on the 2D(X-Y) Plotter. 

* Team members
    - [Matti Niinimäki](https://fabacademy.org/2022/labs/aalto/students/matti-niinimaki/) (Fab Academy student)
        - Overall machine design
        - Software configuration
        - Electronics
    - [Arthur Tollet](https://fabacademy.org/2022/labs/aalto/students/arthur-tollet/) (Fab Academy student)
        - Limit switch brackets
    - [Darren Bratten](https://darrenbratten.gitlab.io/digital-fabrication_2022/) (Aalto Digital Fabrication course student)
        - Designing and fabricating the pen mechanism
    - [Yikun Wang](https://yikun_wang.gitlab.io/digital-fabrication/post/mechanical-design/) (Aalto Digital Fabrication course student)
        - 3D model of the entire machine
        - Designing and fabricating the frame, chassis and belt pulleys
    - Yoona Yang (Aalto Digital Fabrication course student)
        - Designing and fabricating the paper holder/carrying plate

* Tasks that we need to figure out for machine building weeks.
    1) Servo/pen holder mechanism
    2) Stepper motor brackets (design and fabrication)
    3) The middle frame that holds the pulleys for the belt (design and fabrication)
    4) The parts that hold the rods in place
    5) Some kind of part where we can mount the PCB. Maybe two versions of this, one for Arduino and one for our custom board
    6) Electronics (we can use the Arduino + Gshield for now), but I’ll make a custom board also
    7) Some type of platform for holding the paper in place
    8) A carry case

We defined the tasks above during the kick-off meeting and started building a prototype right away. After the prototyping, each of us took a specific role. I mostly worked on the platform for holding the paper in place and a carry case.

&nbsp;
### / Prototyping /
* Examine the reference machine; AxiDraw
* Understand the technology and the mechanism of the machine
* Check the components that we need to customize and fabricate

2D Plotter team started the machine-building with the reference machine, AxiDraw. By operating the machine, we tried to understand how the machine works. By examining this machine, we also built the prototype to figure out the mechanism detail and the components we need to fabricate. We used the make-block beams to prototype so that we could quickly build something looking like the 2D plotter machine. 

{{<figure src="./Prototyping-01.jpeg" caption="[AxiDraw plotter as a reference machine]">}}
{{<figure src="./Prototyping-02.jpeg" caption="[Start building a protytope with the makeblock beams]">}}

Depending on the needs, we also simply used the laser cutter to make the parts such as the middle frame on which the four pulleys are attached and connected to the X/Y axis rods. At the moment, we didn't have a pulley. But, we were able to manually operate the prototype mechanism with our fingers so that we could actually see the main movement of the mechanism with the stepper motors. The mechanism follows the T-bot.

{{<figure src="./Prototyping-03.jpeg">}}
{{<figure src="./Mechanism.jpg" caption="[The mechanism of the X/Y axis plotter (T-bot)]">}}

Moreover, we could test the electronics in this prototype process. Regarding the electronics, we use the Arduino and gShield (version 5 [Link to the specification](https://www.digikey.fi/fi/products/detail/adafruit-industries-llc/1750/5054480)) to operate the stepper motor (Model: M42STH47-1684S / NEMA-17). We needed to check the specification of the stepper motor, especially we needed to know the maximum current of the motor in order to set the maximum current limit for the motor. After setting the electronics part, we could also test the G-code for actuating the machine. 

> **Things to know for the electronic part**
> * gShield uses a 0.1ohm current sense resistor so the formula is `Vref = 0.8 * I
> *  The Stepper motor `M42STH47-1684S` (NEMA-17 Stepper Motor) has a maximum current rating of 1.68 Amps per phase. Thus, we needed to use the potentiometer to (0.8 * 1.68 Amps) = 1.34 volts to set the maximum current limit for this motor.
> * 12V/3A power supply.
> * [Grbl](https://github.com/gnea/grbl/wikito) (version 1.1)  controls the motion of the machine with Arduino. (We used another version but it made a freezing issue so changed to version 1.1.)

{{<figure src="./MotorSpec.png" caption="[Motor Specification. We use M42STH47-16845, but any decent stepper motor will work.]">}}
{{<figure src="./Prototyping-04.jpeg" caption="[Universal Gcode Platform to operate the machine]">}}
{{<figure src="./Prototyping-05.jpeg" caption="[Testing the movement by the software; Gcode]">}}

&nbsp;
______

### **Individual Contribution: Platform making for Pilvi the Plotter**

* Design ideation
* Measure the actual dimension and design the platform with the parameter on Fusion360
* Laser cutting test for the joint (tab) and engraving part
* Laser cut all the parts with the acrylic material
* Assemble the parts 

&nbsp;
### / Design /
I started ideation for the platform design of the Pilvi the Plotter. 
My team used acrylic and white plastic (3D printed) as the material for the fabricated parts of the machine. The acrylic is the best material for the platform as well, following the mood. From the Aalto Fablab, the maximum acrylic material size is 600 x 900 mm. The Plotter machine's size we make very suits to the material size. I tried to think of the platform to hold the paper, carry the machine, as well as the case for that at the same time. However, I eventually decided to design the platform with only a handle. 

{{<figure src="./Materials.jpeg" caption="[Acrylic  material for the platform design (we used 6mm and 10mm thickness for the platform making)]" >}}

{{<figure src="./Design-01.jpeg" caption="[Ideation and measuring the machine]">}}

{{<figure src="./Design-02.jpeg" caption="[Apply the dimension to the parameter]">}}

{{<figure src="./FirstDesignSketch.PNG" caption="[First Design]">}}

{{< youtube id="kOhpfTFCB_c" title="Design Process in Fusion360">}}
[Design process in Fusion360]

{{<figure src="./Platform.jpg" caption="[Final Design]">}}


&nbsp;
### / Test the design with the Laser cutter /
Firstly, I tried to test the design with the Laser cutter for the engraving. The part for the test was the joint part by the bolt and nut. I designed the back-side of the platform to have the inserted nuts on the acrylic board because the board should be flat so that the platform doesn't have any uneven surface for the Pilvi to draw the line properly. 

This engraving is to make the hole for the nut so the hole should have enough depth to insert the nut but not making a penetrating hole. For this depth, I tried to test with twelve different setup with the Speed, Power, and Cycles. I finally found the suitable setup for engraving.

>* Engraving setup for the joint part on the back-side of the platform
>   - Resolution: 300
>   - Speed: 20.0%
>   - Power: 80.0%
>   - Cycles: 16

{{<figure src="./LaserCut-Test-01.jpeg" caption="[Engraving test for the joints]">}}

Moreover, I tried to test the engraving for the name on the platform, and for the figures and lines which allow the user can check the size of the paper and fit the paper in the right place on the platform.

>* Engraving setup for the front-side of the platform 
> - Resolution: 300
> - Speed: 80.0%
> - Power: 80.0%
> - Cycles: 1

{{<figure src="./EngravingText.jpg" caption="[Desgin for the front-side of the platform]">}}
{{<figure src="./LaserCut-Test-02.jpeg" caption="[Engraving test for the front-side of the platform]">}}

&nbsp;
### / Laser cutting progress /
The Pilvi platform size is too big to be fit in the one acrylic board (W 900mm x H 600mm) in Aalto Fablab. So for the laser cutting process, I had to use more than one board. 

Firstly I tried to cut and engraving for the back parts including the handle and bottom supporter on the 10mm Arcylic board. Not as the previous engraving test for the joint, the result was very unregular. Thus, I changed the bridge part's Height to have more millimeters (10mm → 15mm), making the gap between the back-side of the platform and the ground to put the platform. 

I followed the setting (below) for cutting each parts.

>* First cutting for the back parts on the 10mm acrylic board
>    - Speed: 3%
>    - Power: 100%
>    - Frequency: 90%

{{<figure src="./LaserCut-Process-01.jpg" caption="[Cutting and engraving on the 10mm Acrylic board for the back parts]">}}

>* Second cutting for the handle (front), Bridege / Third cutting for the holder
>    - Speed: 6%
>    - Power: 100%
>    - Frequency: 80%

{{<figure src="./LaserCut-Process-02.jpg" caption="[Cutting on the 6mm Acrylic board for the front Handle and the bridge]">}}

{{<figure src="./LaserCut-Process-03.jpg" caption="[Cutting and engraving on the 6mm Acrylic board for the holer]">}}

&nbsp;
### / Assemble the parts /
After the laser cutting process is done, each part needs to be assembled. I used a 4mm diameter for the bolts and nuts.

{{<figure src="./Assemble-01.jpeg" caption="[Assembled Pilvi platform with the bolts and nuts]">}}

Finally, The last part is gluing the four magnets which hold the paper with the pair magnets on the platform. I tested several different adhesives and eventually glued the magnets (diameter: 3.6mm) in the hole of the platform. The way to attach the magnets is a little bit tricky because the glue needs a day to be dried and some parts have some leaking glue in between the two layers of the front and back parts. This is not really a matter for the platform as the plotter. However, this matter could be fixed in a better way so that users can easily assemble and disassemble the platform, and evermore, it could have a better look as a product.
{{<figure src="./Glue-01.jpg" caption="[The adhesives for sticking the magnet on the platform]">}}
{{<figure src="./Glue-02.jpg" caption="[Leaking glue in between the two layers of the front and back parts]">}}
{{<figure src="./Final.jpg" caption="[Attached magnet holding a paper]">}}

&nbsp;
______
### **Source files**
- Platform design files (DXF format) → [dxf.zip](./files/dxf.zip)
- Platform design files for the laser cutting (Ai format) → [ai.zip](./files/ai.zip)



