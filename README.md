## Hi, I am Yoona Yang. 

I live in Espoo, Finland.  
My background is the visual communication design and media design.  
I have worked in the media art studio as a visual designer and production assistant.    
I currently study media art and design in Aalto University as a master student,  
focusing on the media art installation and artistic research for data-visualization.

My Digital Fabrication website is here --> [https://yyoona.gitlab.io/digital-fabrication](https://yyoona.gitlab.io/digital-fabrication)
