---
title: 'YOONA / DF2022'
type: docs
weight: 1
---

## Welcome to Yoona's Digital Fabrication documentation page.
This website is Yoona's documentation for "Digital Fabrication" course at Aalto University in 2022.