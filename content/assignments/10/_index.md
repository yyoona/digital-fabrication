---
title: '10 / Embedded Programming'
---
______
## Task

Do the following to complete the assignment.

* Compare the performance and development workflows for other architectures. You should try to program two different microcontroller boards.
* Read the datasheet for your microcontroller and identify information that could be useful for your project.
* Use your programmer to program your board to do something.
* Describe the programming process with text and images.
* Document the process on your documentation page.
* Include source files of the code you wrote.
* Add a hero video of your board working.

&nbsp;
______
## Assignment 10 / Embedded Programming
{{< youtube id="CTPR8UV46Fc" title="Programming HELLO ATtiny412" >}}
[Programming HELLO ATtiny412]

&nbsp;
______

 ### **Read the datasheet for the microcontroller and identify information**

Things that I explored...
* Microcontroller: ATtiny412 8-pin SOIC, SAM D11C 14-pin SOIC
* Hello Board: HELLO ATtiny412, HELLO D11C USB-C
* Programmer: UPDI D11C, SWD D11C

For the Embedded Programming assignment, I tried to program 2 different microcontroller boards (HELLO ATtiny412, HELLO D11C USB-C). The datasheet is hard to understand for me, but I was trying to look through the information from the datasheet and focus on the several parts by following the introduction of the Digital Fabrication lecture.

**About ATtiny412** [(Go to Datasheet ⇱)](https://ww1.microchip.com/downloads/en/DeviceDoc/ATtiny212-412-DataSheet-DS40001911B.pdf)

{{< figure src="./ATtiny.jpeg" caption="[ATtiny412]" >}}

According to the introduction of the datasheet (generally provided by pdf file), the ATtiny212/214/412/414/416 are members of the tinyAVR®1-series of microcontrollers, using the AVR® processor with hardware multiplier, running at up to 20 MHz, with 2/4 KB Flash, 128/256 bytes of SRAM, and 64/128 bytes of
EEPROM in an 8-, 14-, and 20-pin package. The tinyAVR®1-series uses the latest technologies with a flexible, low-power architecture, including an Event System, accurate analog features, and Core Independent Peripherals (CIPs).

The information that should be focused on for ATtiny412 is generally, 
> Features / Pinout / ADC; Analog to Digital Converter / DAC; Digital to Analog Converter / UPDI; Unified Program and Debug Interface / Packaging Information / Application examples.

The information below is what I took some notes for the ATtiny412.

* **Features**
    - CPU: AVR type, 20MHz
        - AVR chips are actually also attached to the external [oscillator](https://en.wikipedia.org/wiki/Electronic_oscillator).
    - Memories:  2/4 KB In-system self-programmable Flash memory
        - Memories are important when it comes to the Flash memory. When you use the Arduino library and if the code doesn’t fit onto the chips, you might choose the same family and same producer and check if the memory capacity is bigger.
    - Temperature ranges: -40°C to 105°C / -40°C to 125°C (can be operated in the winter in Northern Europe or somewhere in Africa)

* **Pinout**
{{< figure src="./ATtiny-pinout.jpg" caption="[ATtiny212/412 Pinout]" >}}
    - Pinout information was very useful when I actually try to write some code in Arduino to control the microcontroller boards.
    - How many pins? What functionality the each of the pins has? 
    - VDD / GND 
        - has (external) bypass capacitor to filter the current so that this is as stable as possible
    - Color code
        - Analog function: it has the analog to digital convertor.
        - PA6, PA7, PA1 (these pins) can measure the voltage
        - Digital function only: it can output digital signals so it can pull. The microcontroller can pull the pin high or low physically to be input voltage. If it's 5v or higher, then 0 voltage makes it flow.
        - Clock, crystal: pin where you can connect the external crystal oscillator. 
        - Programming, Debug, Reset: the pin where we connect the UPDI functionality (UPDI pin). It can be also used as a digital pin. when it’s programming, the microcontroller recognizes the specific signal or specific sequence. And then it switches a mode from the actual program load to the programming mode. During this mode, when it is finished, then reset. Also, it can be used inside as a regular digital pin.


**About ATSAMD11C14A-SSUT** [(Go to Datasheet ⇱)](https://ww1.microchip.com/downloads/en/DeviceDoc/Atmel-42363-SAM-D11_Summary.pdf)

{{< figure src="./SAM D11C14.jpg" caption="[SAM D11C14]" >}}

According to the introduction of the datasheet, the SAM D11 series is compatible with the other product series in the SAM D family, enabling easy migration to larger devices with added features.

* **Features**
    - Core size: 32-Bit 
    - Speed: 48MHz
    - built-in USB support
    - The Main different features between this microcontroller and ATtiny412 is the core size, speed and USB support. 

* **Pinout**
{{< figure src="./SAM D11C14-pinout.jpg" caption="[SAM D11C 14-pin SOIC Pinout]" >}}
    - Even though it’s from a different manufacturer, the datasheet has the section about ’Pinout’ and the numbering is similar to ATtiny. However, the analog and digital Pins are not combined as the pins in the ATtiny412.
    - Only one pin OSCILLATOR: connect to the external oscillator

&nbsp;
______

### **Program the microcontroller boards**
 
&nbsp;
### / Prepare the programmer /

In order to program Hello ATtiny412, I used **UPDI D11C** programmer. But, I didn't need any programmer for Hello D11C USB-C. 
The detail for the UPDI D11C can be found on this page: https://gitlab.fabcloud.org/pub/programmers/programmer-updi-d11c

The first step to consider for programming the boards, the programmer should be prepared. In order for UPDI D11C to be activated on the computer, I should have a process for the `flashing bootloader` from [Fab SAM core ⇱](https://github.com/qbolsee/ArduinoCore-fab-sam) because the process makes the serial device capability for the programmer. Even before then, EDBG should be installed on the computer. 

**What is EDBG?**
- The Atmel Embedded Debugger (EDBG) is an onboard debugger for kits with Atmel devices. EDBG enables the user to debug the target device without an external debugger. EDBG also brings additional features with a Data Gateway Interface and a Virtual COM Port for streaming of data to a host PC.

I used a terminal to install the EDBG. As a MacBook user, I needed to check the 'Security and Privacy' of the computer system preferences. (Checked for the terminal in the developer tool. The sources for installing EDBG can be found on this GitHub page: https://github.com/ataradov/edbg)

After installing EDBG, SWD D11C (D11C-based programmer with SWD connector for programming ARM chips such as D11C itself) should be connected for the flashing bootloader process.

* **Flashing bootload process (in Arduino interface)**
    1. Go to tools
        - Select the current board
        - Make sure that you have selected for the programmer
        - Click the bootloader button
    2. go to Fab SAM core: adaptor library for Arduino
        - In order to install it, copy the address of the orange part.
        - JSON file describes all these packages
    3. Open the Arduino and go to preferences
        - Additional boards manager URLs  
        - Hit ok
    4. Go to tools
        - On the boards Manager
        - Look for Fab SAM
        - Install `Fab SAM core for Arduino by Fab Foundation`
    5. Go to tools 
        - board > Fab SAM core for Arduino > Generic D11C14A 
        - you can  use this as a starting point if you have a D11C board
        - Config: for debugging
            - select one of the options (ONE~ / TWO ~)
            - if you want to transform your board into the programmer, select the TWO~
* After programming UPDI, need to upload this code (https://github.com/qbolsee/SAMD11C_serial/blob/main/SAMD11C_serial/SAMD11C_serial.ino) in Arduino.

{{< figure src="./SWC.jpg" caption="[SWD D11C and the wire to connect to other boards]" >}}

{{< figure src="./bootloader.jpeg" caption="[Connecting SWD D11C and UDPI programmer for the flashing bootloader process ]" >}}

&nbsp;
### / Program the Hello ATtiny412 board /

{{< figure src="./ProgrammingATtiny.jpeg" caption="[Programming Hello ATtiny412 board]" >}}

* Plug the boards and the programmer into the computer while the board and programmer are connected together at the same time.
* Go to Arduino
* Install the Boards Manager (megaTinyCore by Spence Konde)
    - This board package can be installed via the board manager. The boards manager URL is: http://drazzy.com/package_drazzy.com_index.json
    - File -> Preferences, enter the above URL in "Additional Boards Manager URLs"
    - Tools -> Boards -> Boards Manager...
    - Wait while the list loads (takes longer than one would expect, and refreshes several times).
    - Select "megaTinyCore by Spence Konde" and click "Install". For best results, choose the most recent version.
 * Select the megatiny board family

**Debugging**

I had some debugging when I tried to program the code with Arduino and couldn't find the reason. I eventually made the new board again.
{{< figure src="./helloATtiny-programming-bebugging.png" caption="[Trouble with the Hello ATtiny412 board]" >}}

&nbsp;
### / Program the HELLO D11C USB-C board /

{{< figure src="./D11CUSBC.jpg" caption="[Hello D11C USB-C]" >}}

* Plug only the boards into the computer. The microcontroller board does not need to connect to the programmer while using it.
* Go to Arduino.
* Install the Boards Manager (Fab SAM core for Arduino) through the Gitlab page (https://github.com/qbolsee/ArduinoCore-fab-sam).
* Select the Generic D11C14A in Fab SAM core for Arduino in the Tools menu of Arduino.

{{< figure src="./helloD11C-programming-00.png" caption="[Getting the board manager URL from Gitlab page for Hello D11C USB-C]" >}}
{{< figure src="./helloD11C-programming-01.png" caption="[Add the Boards Manger URL from gitlab page for Hello D11C USB-C in the Preferences of Arduino]" >}}
{{< figure src="./helloD11C-programming-02.png" caption="[Select the Board: Generic D11C14A in the Tools menu]" >}}

&nbsp;
______

### **Use the microcontroller**

**Test the boards**

For the testing of the programmed two boards (Hello ATtiney412 and Hello D11C USB-C), I used basic code in Arduino to make the LED blink.

{{< figure src="./Code.png" caption="[Simple code to make the LED blinking]" >}}

{{< youtube id="CTPR8UV46Fc" title="Programming HELLO ATtiny412" >}}
[Testing HELLO ATtiny412]

{{< youtube id="k85pRD3liXo" title="Programming HELLO D11C USB-C" >}}
[Testing HELLO D11C USB-C]

**Additional programming exploration**

* ATtiny412 code to use PWM to fade the LED in and OUT.
    - I tried to use the button as an input to turn off the led as an output. (Arduino code below) 
    {{< youtube id="r-6uVZS122w" title="Programming HELLO ATtiny412" >}}
[Fading test with HELLO ATtiny412]

    ~~~
        // constants won't change. They're used here to set pin numbers:
        const int buttonPin = 4;     // the number of the pushbutton pin
        const int ledPin =  2;      // the number of the LED pin

        // variables will change:
        int buttonState = 0;         // variable for reading the pushbutton status

        void setup() {
        // initialize the LED pin as an output:
        pinMode(ledPin, OUTPUT);
        // initialize the pushbutton pin as an input:
        pinMode(buttonPin, INPUT);
        }

        void loop() {
        // read the state of the pushbutton value:
        buttonState = digitalRead(buttonPin);

        // check if the pushbutton is pressed. If it is, the buttonState is HIGH:
        if (buttonState == HIGH) {
            // turn LED on:
            digitalWrite(ledPin, HIGH);
        } else {
            // turn LED off:
            digitalWrite(ledPin, LOW);
        }
        }
    ~~~
    

* Try to understand the difference between char, int, and long data types.
    - char: The char data type is a signed type, meaning that it encodes numbers from -128 to 127. For an unsigned, one-byte (8-bit) data type, use the byte data type. (https://www.arduino.cc/en/reference/char)
    - int: Integers are your primary data type for number storage. On the Arduino Uno (and other ATMega-based boards) an int stores a 16-bit (2-byte) value. This yields a range of -32,768 to 32,767 (minimum value of -2^15 and a maximum value of (2^15) - 1). (https://www.arduino.cc/en/reference/int)
    - long data types: Long variables are extended size variables for number storage, and store 32 bits (4 bytes), from -2,147,483,648 to 2,147,483,647.If doing math with integers, at least one of the numbers must be followed by an L, forcing it to be a long. (https://www.arduino.cc/en/Reference/Long)

* How to use a for versus while loop.
    - for loop: The for statement is used to repeat a block of statements enclosed in curly braces. An increment counter is usually used to increment and terminate the loop. The for statement is useful for any repetitive operation and is often used in combination with arrays to operate on collections of data/pins. (https://www.arduino.cc/reference/en/language/structure/control-structure/for/)
    
    - while loop: A while loop will loop continuously, and infinitely, until the expression inside the parenthesis, () becomes false. Something must change the tested variable, or the while loop will never exit. This could be in the code, such as an incremented variable, or an external condition, such as testing a sensor. (https://www.arduino.cc/reference/en/language/structure/control-structure/while/)

    - I tried to use a 'for' in a loop to dim the LED. (Arduino code below) 
        {{< youtube id="F0Q6tmV5Kvw" title="Programming HELLO ATtiny412" >}}
[For loop test with HELLO ATtiny412]
    ~~~
        // Dim an LED using a PWM pin
        int PWMpin = 2;  // LED in series with 470 ohm resistor on pin 10

        void setup() {
        // no setup needed
        }

        // With this code, the LED is turned off and start being dimmed from the brightest value.
        /*
        void loop() {
        for (int i = 0; i <= 255; i++) {
            analogWrite(PWMpin, i);
            delay(10);
        }
        }
        */

        // With this code, the LED keeps being dimmed inbetween the brightest and darkest value like a slow blinking.
        void loop() {
        int x = 1;
        for (int i = 0; i > -1; i = i + x) {
            analogWrite(PWMpin, i);
            if (i == 255) {
            x = -1;  // switch direction at peak
            }
            delay(10);
        }
        }
    ~~~
&nbsp;
______
### **Source files of the code**
- Code for the Hello ATtiny412  → [HelloATtiny412.ino](./files/HelloATtiny412.ino)
- Code for the Hello D11C USB-C → [HelloD11CUsbC_.ino](./files/HelloD11CUsbC_.ino)

