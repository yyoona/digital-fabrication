---
title: 'About'
weight: 1
---
______
## YOONA YANG
______
&nbsp;

Hi! I am Yoona Yang, living in Finland and born in Republic of Korea.  

Currently, I am studing New Media Design and Production at Aalto University, Finland. Also, I previously studied Visual Communication Design and Media Design in Seoul, Korea. After the study in Korea, I have worked in Seoul based media art studio as a visual designer and production assistant.

Regarding my current study, I am passionated about Interactive art, Kinetic installation, Interaction design, UX/UI, Data Visualization. But I have interested in Graphic design, Photogrphy, and any experimental things, and nature.
  
&nbsp;&nbsp;

![MyImg](./yoona-profile.jpg)
