---
title: '11 / Molding and Casting'
---
______
## Task

Do the following to complete the assignment.

* Review the safety data sheets for at least two of the Labs Molding and Casting materials.
* Make and compare test casts with each of them.
* Design a 3D mold around the stock and tooling that you'll be using.
* Mill it (rough cut + (at least) three-axis finish cut).
* Use it to cast parts.
* Include a Hero shot and source files of your design in your documentation.


&nbsp;
______
## Assignment 11 / Molding and Casting
{{<figure src="./hero-shot.jpg" caption="[Casted book thumb holder in use]" >}}

&nbsp;
______

### **Design a 3D mold and Mill it**

&nbsp;
### / Design a 3D mold with Fusion360 /

**Ideation**

As a design process, I got some references for the book thumb holder from Pinterest. I found that there is some specific dimension for the object so I could easily decide the value for what I wanted to make.  
{{<figure src="./ideation.png" caption="[Ideation for the book thumb holder]" >}}

**Modeling in Fusion360**

* Set the Parameter for the object to make.
* Create the object first.
* Make the positive mold around the object. (H:44 x W:94 x T:20mm)
* Make the negative mold, checking the material size (stock).
* Export the file in STL Mesh format for using it in VCarve.

{{<figure src="./parameter.png" caption="[Parameters for the object dimension]" >}}
{{<figure src="./object-01.jpg" caption="[Design the ojbect]" >}}
{{<figure src="./positivemold-01.jpg" caption="[Make the positive mold]" >}}
{{<figure src="./parameter-2.png" caption="[Check the material size]" >}}

{{<figure src="./negativemold-01.jpg" caption="[Make the nagetiva mold]" >}}

{{<figure src="./export.jpg" caption="[Export the final design as the STL file]" >}}


&nbsp;
### / Mill the designed 3D mold with the MDX-40A /

**Materials**

In the Aalto Fablab, there are several different materials for milling to make the mold (for example, foam, the sika block, and wood). Even though milling with the wax is more complicated than milling with the foam, I tried to use the wax for this assignment to figure out the matter of the feed rate. 

{{<figure src="./material.jpeg" caption="[Where to find the milling materials and the materials for miiling to make the mold]" >}}


**Tools**

We can find the tools for the MDX milling from the drawer under the table where the MDX-40A is put on. Firstly, using a butterfly milling tool, we should make the wax have a flatter surface so that we can start the actual milling process. 

* MDX bits (Ballnose is proper to make the smooth finishing.)
    - Butterfly milling bits (This tool has a 6mm shank.)
    - 6mm ball shorter / longer
    - 6mm flat shorter / longer
    - 3mm ball shorter / longer 
    - 3mm flat shorter / longer 

* Collet, wrenches (10/17), tape measure, double-sided tape, and so on.

For my book thumb holder, I used the 6mm flat longer bit firstly for the rough finishing, then, used the 3mm ball longer bit for the fine finishing path.

{{<figure src="./tool-01.jpg" caption="[MDX milling bits (Left: Butterfly milling bit, Right: 6mm flat longer bit / 3mm ball longer bit)]" >}}
{{<figure src="./tool-02.jpg" caption="[Other tools for milling (Left: collets, Right: other tools)]" >}}

**Milling process step-1. Flatten the surface of wax**

1. Check the dimension of the material with the tape measurement. 
    - In order to make the material surface flat, check the thickness of the two different parts; the thickest and thinnest parts. Also, this process allows the milling tool can have the same length as the material when we set the milling tool. 
2. Prepare milling tools. 
3. Attach the material onto the plate of MDX-40A with double-sided tape. The material shouldn't be loaded on the edge of the plate. 
4. [1st Milling] Set the butterfly tool (6mm shank) with the collet and the two wrenches. The tool should be inserted about 1cm.
{{<figure src="./milling-01.jpg" caption="[Set the collet and the butterfly bit by the wrenches]">}}
5. Go to VCarve on the computer.
6. Set the job size with the thickest thickness for the model thickness, and draw the rectangle for the flattening surface process with a butterfly mill.
{{<figure src="./miling-02.jpg" caption="[Job Setup and draw the rectangle of the material surface's size]">}}
7. Generate toolpath
    * Firstly, set the tool Database for the End Mill (22 mm). 
        - Check the spindle rotation on the [Roland website](https://www.rolanddga.com/products/3d/mdx-50-benchtop-cnc-mill#specs). (maximum 15000rpm)
        - Details
            - Tool Type: End Mill
            - Geometry: Diameter 22.0 mm
            - Cutting Parameters
                - Pass Depth: 1.0 mm
                - Stepover: 8.8 mm, 40.0 %
            - Feeds and Speeds 
                - Spindle Speed 14000 r.p.m
                - Feed Rate 2500.0 mm/min (The machine's maximum feed rate is 3000 mm/min thus followed this value, not the value from the calculation)
                - Plunge Rate 500.0 mm/min
            - Tool Number: 1
        {{<figure src="./milling-03.png" caption="[Tool Database setting]">}}

    >**How to calculate the Feed Rate and Plunge Rate?**
    >   - The wax is quite hard as the copper material. Thus, we need to check the value for milling parameters with the copper's guide values. (But, I refered the MDF value for the butterfly mill)
    >   - fr = nf * cl * ss
    >       - fr: Feed Rate
    >       - nf: Number of Flutes
    >       - cl: Chip Load
    >       - ss: Spindle Speed
    >   - pr (Plunge Rate) = fr/4 
    {{<figure src="./milling-04.jpeg" caption="[guide values for the calculation]">}}
    * Set other details for the toolpaths.
        - Clear Pocket ...
            - Select Raster
            - Select Climb for the Cut Direction
            - Raster Angle: 0.0 degrees
            - Profile Pass: Last
            - Pocket Allowance: -11.0mm (After the first calculation, we found the offset of the edges. So we added the negative Pocket Allowance to remove the offset.)
        - Give a name, 'Facing', and Click Calculate.
    {{<figure src="./milling-05.jpg" caption="[Toolpaths details]">}}
    * Check the preview of the toolpaths and save the toolpaths
    {{<figure src="./milling-06.jpg" caption="[Save the toolpaths]">}}
8. Go to VPanel and choose the G54 to set up the NC Code.
{{<figure src="./milling-07.jpg" caption="[Start VPanel process]">}}
9. Put the Z-origin sensor on the highest part of the material. 
10. After setting the Z-origin, set the XY-origin to the southeast side of the material.
{{<figure src="./milling-08.jpg" caption="[Set the Z-origin, XY-origin, and the milling for the flatten facing ]">}}
11. Start cutting. Set the Cutting Speed at 10% for the start, but we can control the speed up and down gradually during the milling progress. (Wax is hard so, 10% is fine when the milling starts. I changed the speed upto about 40-50% during the cutting.)
{{<figure src="./milling-08-2.jpg" caption="[Start cutting for the flatten facing ]">}}

**Milling process step-2. Roughing and Finishing milling** 
1. Generate Roughing and Finishing toolpaths in VCarve.
    * Open the new file and set the job (width 110, heigh 70, thickness 29.5mm).
    * Import the 3D model (stl) file of the final mold design.
    {{<figure src="./milling-09-01.jpg" caption="[Start VCarve and import the 3D model)]">}}
    * Go to the 3D View; Orientate the 3D Model
        - Set the position with the orientation and rotation, model size, and set the Zero Plane Position in Model. Zero Plane Position in Model is important in this menu. Depending on the set of the Depth Below Top, we can save the milling time and the material to be cut if we select 'Discard data below zero plane'. Because the below part is not going to be processed. 
        {{<figure src="./milling-09-02.jpg" caption="[Orientate 3D Model menu)]">}}
    * Generate toolpath
        - Go to `Material Setup` in the Toolpaths menu
            - Thickness: 29.5 mm (The original material thickness was 39.5mm. 10mm was cut during the butterfly milling process.)
            - XY Datum: select South-west / uncheck 'Use Offset' / check 'Show detailed summary on toolpath tab'
            - Z-Zero: select 'Material Surface'
            - Model Position in Material: select 'Gap Above Model'
            - Rapid Z Gaps above Material: 
                - Clearane(Z1) 5.0 mm
                - Plunge (Z2) 5.0 mm
            - Home / Start Position: 
                - X: 0.0 / Y: 0.0
                - Z Gap above Material: 20.0
        {{<figure src="./milling-09-03.jpg" caption="[Material Setup)]">}}
        - Calculate the Feed Rate for the roughing and finishing milling tool.
        {{<figure src="./feedrate.jpg" caption="[Calculate the feed rate (left: roughing milling / right: finishing milling)]">}}
        - Go to `3D Roughing Toolpath (Rough Machining Toolpath)` in Toolpath Operations of the Toolpaths menu in order to set the tool details of the roughing toolpath with End Mill (6 mm).
            - Tool Database
                - Tool Type: End Mill
                - Geometry: Diameter 6.0 mm
                - Cutting Parameters
                    - Pass Depth: 1.0 mm
                    - Stepover: 2.4 mm, 40.0 %
                - Feeds and Speeds 
                    - Spindle Speed 14000 r.p.m
                    - Feed Rate 1400.0 mm/min
                    - Plunge Rate 350.0 mm/min
                - Tool Number: 1
            - Machining Limit Boundary
                - Select 'Model Boundary'
                - Boundary Offset: 0.0 mm
            - Machining Allowance: 0.1 mm
            - Roughing Strategy
                - Select 'Z Level'
                    - Raster X
                    - Profile...: Last
            - Uncheck Ramp Plunge Moves
            - Give a name '3D Roughing 1'
            - Hit 'Calculate' after the setting above is done.
            - Check the Preview Toolpath
        {{<figure src="./milling-09-04.jpg" caption="[Roughing toolpath details]">}}
        - Go to `3D Finishing Toolpath (Finish Machining Toolpath)` in Toolpath Operations of the Toolpaths menu in order to set the tool details of the finishing toolpath with Ball Nose Mill (3 mm)
            - Tool Database
                - Name: Ball Nose 3mm Dia
                - Tool Type: Ball Nose
                - Geometry: Diameter 3.0 mm
                - Cutting Parameters
                    - Stepover: 0.5 mm, 16.7 %
                - Feeds and Speeds 
                    - Spindle Speed 14000 r.p.m
                    - Feed Rate 700.0 mm/min
                    - Plunge Rate 175.0 mm/min
                - Tool Number: 1
            - Machining Limit Boundary
                - Select 'Model Boundary'
                - Boundary Offset: 0.1 mm
            - Area Machine Strategy...
                - Select 'Offset'
                    - Cut Direction: Climb
                    - Stepover Retract: 0.0 mm
            - Give a name '3D Finish 1'
            - Hit 'Calculate' after the setting above is done.
            - Check the Preview Toolpath
        {{<figure src="./milling-09-05.jpg" caption="[Finishing toolpath details]">}}
        - Save each toolpaths with the nc file format; 3D Roughing 1 / 3D Finish 1 
        {{<figure src="./milling-09-06.jpg" caption="[Save the toolpaths]">}}

2. [2nd Milling] Change the milling bit to the Flat-end milling bit (6mm shank) with the collet and the two wrenches. The tool should be inserted about 1cm.
3. Go to VPanel and choose the G54 to set up the NC Code.
4. Put the Z-origin sensor on the material. 
5. Set the Z-origin with the sensor again. but leave the XY-origin at the first setting for the first milling. 
6. Start cutting for the roughing mill. 
{{<figure src="./milling-10.jpg" caption="[Set the Z-origin again and rough milling with the 6mm milling bit (2 Flute Square 6.0 Shank 75mm)]">}}
7. After the roughing mill process, time to to the final finishing mill.
8. Change the milling bit to the Ball nose milling bit (3mm) and do the same job as the roughing milling process.
{{<figure src="./milling-11.jpg" caption="[Set the Z-origin again and fine milling with the 3mm milling bit (2 Flute Ball nose 3.0 Shank 75mm) for the final]">}}
**The cutting speed can be controlled. But, I didn't make the speed up over 70% because I wanted to prevent the milling tool be broken. 

**Finished Mold Making**

After the all milling processes are done, use the vacuum to clean the wax dust. Finally, take the finished mold out from the plate using a knife. Lastly, uninstall the milling bits and rivet, and put them back in the right place. 
{{<figure src="./milling-12.jpg" caption="[After the milling process done]">}}


&nbsp;
______
### **Casting Materials and Equipment**

For molding and casting, some of the liquid materials are toxic, so it is important to keep the safety rule in the space to process the molding and casting. Also, the datasheets of the materials are needed to be checked in advance. In the Aalto Fablab, the process can be done in the wood-workshop space. The information below is described based on the Aalto Fablab.

#### / Casting materials /
- The materials are found on the top selve of the blue cabinet.
- The **data sheet** is also found on the left side of the same selve.
- Casting materials in stocked in the Aalto Fablab
    - [Dragon Skin™ Series ⇱](https://www.smooth-on.com/product-line/dragon-skin/): durable silicon for mask 
        - Dragon Skin™ 10 FAST
            - Pot Life: 8 min
            - Cure Time: 75 min
        - Dragon Skin™ 10 SLOW
            - Pot Life: 45 min
            - Cure Time: 7 hrs
    - [Mold Star™ Series ⇱](https://www.smooth-on.com/product-line/mold-star/): soft, strong rubbers which are tear resistant and exhibit very low long term shrinkage. not for food safe. (These silicons have a color code: darker blue is a fast cure / light blue is a slow cure)
        - **Mold Star™ 15 SLOW**
            - Pot Life: 50 mins
            - Cure Time: 4 hrs (at the room temperature)
        - Mold Star™ 16 FAST
            - Pot Life: 6 mins 
            - Cure Time: 30 mins (at the room temperature)
    - [SORTA-Clear™ Series ⇱](https://www.smooth-on.com/product-line/sorta-clear/): near-clear translucent silicone rubbers (platinum catalyst)
        - SORTA-Clear™ 37: certified food safe and can be used for culinary applications including casting chocolate and other confections.
            - Pot Life: 25 mins
            - Cure Time: 4 hrs
    - [EpoxAcast™ Series ⇱](https://www.smooth-on.com/product-line/epoxacast/)
        - EpoxAcast™ 690: optically Clear(transparent) Epoxy in order to cast lenses
    - [Smooth-Cast™ Series ⇱](https://www.smooth-on.com/product-line/smooth-cast/)
        - **Smooth-Cast™ 305**: liquid plastics (Polyurethane). Ultra-low viscosity casting resins that yield castings that are bright white and virtually bubble free.
            - Pot Life: 7 mins
            - Cure Time: 30 mins
        - Smooth-Cast™ 325: ColorMatch - Pigmentable.
            - Pot Life: 2.5 mins
            - Cure Time: 10 mins
    - **Silicone Thinner**
        - This material is used to make the mixing and pouring smoothly in the silicone casting process. 
        - Casting a plastic doesn't need this thinner.

{{<figure src="./materials.jpeg" caption="[Casting materials]" >}}

#### / Safety equipments and rules /
- Wear the lab coat and safety glasses
- Use Vinyl Gloves to deal with the silicon or plastic
- Use the container, plastic cup, or paper cup for mixing 
- Use the Mixing stick
- Liquid to wash the eyes
- Trash bin (** do not use the bin in the wood workshop. Use the Formlabs trash bin or bin in the Atex room.)
{{<figure src="./SafetyEquipments.png" caption="[Safety equipments]" >}}

#### / Some other equipment /
- Scale: measuring the material volume
- Vacuum degassing chamber: removing pressure inside the chamber, and removing the bubble inside the materials.
{{<figure src="./OtherEquipment.jpg" caption="[Scale and Vaccum degassing chamber]" >}}


&nbsp;
______
### **Casting Process**

&nbsp;
### / 2 ways of casting process; 2-step and 3-step /
* **[2-step]** If the result object is supposed to be flexible, we can deal with 2 steps process.
    1. Make a rigid mold.
    2. Cast soft material (silicone) in the rigid mold in order to get the soft material's object out.
* **[3-step]** If the object is rigid, we need to follow the 3-step process. 
    1. Make the rigid mold.
    2. Cast the soft material in the rigid mold in order to make the soft (silicone) mold.
    3. Cast the rigid object with polyurethane (SmoothCast) or epoxy (EpoxACast) in the soft mold. 

&nbsp;
### / Try the 3-step casting process /

**Cast the sofe silicone mold**
1. Wear vinyl gloves and glasses and the working clothes.
2. Get the casting materials including a silicone thinner from the blue cabinet.
    - SORTA-Clear 37 (My first try)
        - I used this material for pre-made mold to test the material. 
        - The outcome has a lot of bubbles inside it, even though I used the vacuum chamber. But I checked the Dragon Skin 10 FAST doesn't make the bubble in the outcome from some classmates' results.
    - Mold-star SLOW
        - I choose this slow resin for the slow experiment to casting the mold that I designed for the book thumb holder.
        - It is better than Mold-star FAST in order to do the fine-tuning with the bigger mold because of the longer pot life. 
    - Silicone Thinner 
{{<figure src="./casting-00.jpg" caption="[Prepare casting materials]" >}}

3. Get the water (water density is similar to the silicone, so use the water as a reference to scale the weight of the silicone) and pour the water into the mold.
{{<figure src="./casting_01.jpeg" caption="[Pour water into the mold]" >}}

4. Fill the water in the cup (paper cup).

5. Scale the cup filled with the water.
    - Scale the cup first. Set the 0.0g for the cup; 0.0g with the cup / -6.9g without the cup (cup’s weight is 6.9g)
    - Pour the water into the cup, and wait for it, and check the total vaule.
{{<figure src="./casting_02.jpeg" caption="[Scale the water volume]" >}}

6. Use the compressed air-gun to get rid of the water in the mold.

7. Get a plastic cup to mix the silicone using the wood mixing stick.

8. Prepare the silicones and silicone thinner.
    - Mix each silicone from the bottom before mixing together with the clean stick. After mixing, leave the stick inside the bottle.
    - Mix other silicone with the fresh stick.
    - Mix the silicone thinner with the fresh stick in order to make the silicone mixture more liquid so that it can be easy to be poured into the mold.
    - Mix them together (check the datasheet; technical information inside the package). The mix ratio is vary depending on the materials. 
        - For example, when the total weight is 85g and the mix ratio 1A:1B by volume or weight, mix 40g and 40g of the silicones A/B, and add a silicone thinner to get up to 85g (maybe 90g).
        - Start with part A by using a stick first: 40g (50g)
        - Part B: 40g (50g) 
        - Thinner: pour and use the same stick to mix all (10g)
for a minute.
{{<figure src="./casting_03.jpeg" caption="[Prepare to mix materials]" >}}

9. Use a vacuum chamber.
    - Open the vacuum chamber and put the mixed silicone in a cup inside the chamber.
    - Little manual valve (if close the valve, then start to remove the pressure).
        - Let the air back into the chamber by opening the valve slowly and shortly so that the pressure can be controlled properly (if it’s too fast, the thing inside the chamber will jump up and split everything).
    - Before then, need to build pressure by switching on the vacuum pump (make sure that it’s connected to the power outlet).
        - Use this button to switch on.
        - Meter has a red indicator.
            - Meter goes to until red, which means enough depressure raised. 
            - Leave it in that state for some time to see how the bubble is getting out.
10. Activate the pump and close the valve, do the degassing process.
    - The liquid start making a bubble.
    - Slowly let the air in.
    - Repeat this process 2-3 times.
{{<figure src="./casting_04.jpg" caption="[Vacuum chamber process]" >}}

11. After done, ready to pour it into the mold.
    - Keep the distance between the cup and the mold so that the mixture should be poured as a thin line to avoid the bubbling.
    - Put the mold in the safe space and wait for 4 hours (The cure time is vary depending on the casting materials).
{{<figure src="./casting_05.jpeg" caption="[Casted mold]" >}}
{{<figure src="./moldtest.jpg" caption="[Casting test with the different materials; Mold-star SLOW, SORTA-Clear 37, Smooth-Cast 305)]" >}}

12. Put the casting materials back in the package right away after the casting process is done.

{{<figure src="./mold.jpg" caption="[Finished mold of the book thumb holder]" >}}

**Cast the rigid object in the soft mold**
1. Wear vinyl gloves and glasses and the working clothes.
2. Get the rigid casting material from the blue cabinet. I tried to use Smooth-Cast 305 (resin) to cast the polyurethane in the silicone mold. This time doesn't need a silicone thinner.
{{<figure src="./casting_06.jpg" caption="[Casting materials (Smooth-Cast 305)]" >}}
3. Do the same process as the soft mold casting process, except for using thinner and the vacuum chamber.
    - Use a cup and water to scale the material volume. 
    - Use the compressed air-gun to get rid of the water in the mold. 
    - Use the cup to mix the materials (A and B) by stirring with the wood mixing stick. (Keep the distance from the cup, when mixing the materials)
{{<figure src="./casting_07.jpg" caption="[Scale the materials for the casting volume and the mixing the material A and B]" >}}
4. After done, ready to pour it into the soft mold.
    - Keep the distance between the cup and the mold so that the mixture should be poured as a thin line to avoid the bubbling.
    - Put the mold in the safe space and wait for the cure time. (30mins for the Smooth-Cast 305)
5. Put the casting back in the package right away after the casting process is done. 
{{<figure src="./casting_08.jpg" caption="[Cure the polyurethane and the result]" >}}



&nbsp;
______
### **Original Design Files**
- 3D modeling file for the mold design → [Mold-yoona-fin.stl](./files/Mold-yoona-fin.stl)
- Toolpath files for milling process  → [MillingMDX40.zip](./files/MillingMDX40.zip)
