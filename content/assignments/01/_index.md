---
title: '01 / Introduction'
---
______
## Task

Create a GitLab account and use its CI tools to publish a website. Submit the following.

- Link to your repository, e.g. https://gitlab.com/your-username/digital-fabrication/
- Link to the published website, e.g. https://your-username.gitlab.io/digital-fabrication/
- Add a picture and description about yourself. At least one paragraph of text.

&nbsp;
______