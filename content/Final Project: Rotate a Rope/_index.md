---
title: 'Final Project: Rotate a Rope'
weight: 1
---
______

## Rotate a Rope
______
### **Final Poster and Video**

{{< figure src="./FinalProjectPoster.png" >}}
{{< youtube id="oXCYL_atWCU" title="Final Project" >}}

______
### **Concept**

**Project title: Rotate a Rope**

For the final project, I made the object as a part of the interactive video installation as a media artwork: Rotate a Rope.

Simply, this is how the installation works. The person on the screen just keeps staying in the stopped position until a visitor rotates a rope of the installation. Visitors can rotate the rope to play the video on the screen. According to that, the person on the screen jumps.

{{< figure src="./idea.jpg" caption="[2D Sketch for the installation view]" >}}
 
I planed to make the media artwork including the concept of simulation. The motif of “Rotate a Rope” is from my daily life and the personal experience of a certain space, that I felt while I was doing the skipping rope. The space is kind of a virtual space that does not exist in the real. But, the space has an abstract and experimental story. By the two actions; (1) rotating a rope and (2) jumping the rope, a sphere space can be created. I feel that the short moment I jump allows me to be in the other dimension’s world, which is very flexible and temporary. After jumping, the landing moment itself is also interesting because each step to land after the jump can make me feel the presence on this ground.

In short, the skipping rope experience gave me to feel the layers of the short moment of presence by the simulated other dimension's world from the rotating rope. In the end, I want to express these moments by creating the video installation as an interactive artwork.

The video below shows what I am going to show in the screen of the installation.
{{< youtube id="653vKSLKcpQ" title="Video to be on the screen" >}}

&nbsp;

-----
### **Reference Works**
* Visual and theme aspect

    I couldn’t find the very similar interactive artwork regarding to the skipping rope. Instead, I found the performative video installation artwork which is inspired by the actual object having the playful movement in it. The artwork is by [Annette Arlander ⇱](https://annettearlander.com/)
    {{< youtube id="Z6OP970zmvE">}}
* Mechanical and technical aspect

    I was able to get some references regarding the mechanical and technical aspect of the skipping rope. When it comes to my first try to represent the skipping rope installation, I chose the ball joint structure. 
    {{< youtube id="d6oJ1z4_m8Q">}}

    Apart from ball joint mechanism, I also collect some other mechanical and techincal references for the next developed version of this project. 
    - how to make a skipping rope
    {{< youtube id="07bhNyWhigs">}}
    - Jumping counter making with nRF 51822 and Accelerometer sensor
    {{< youtube id="rGX8ud4ruHs">}}
    - How Rotary Encoder Works and How To Use It with Arduino
    {{< youtube id="v4BbSzJ-hz4">}}
    - Skipping rope with rotary encoder, slide switch and bearing
    (https://www.instructables.com/Smart-Skipping-Rope/)



&nbsp;
______
### **Planning and Prototyping**

I planned to design the object that is connected to one handle of the skipping rope. The object includes the stand and ball joint mechanical structure. Before creating the actual object, I needed to prototype and test for the input data from the movement of the prototyped structure. 

{{< figure src="./initial plan.jpg" caption="[Initial plan]" >}}

The prototyping process can be explained, being divided by three different parts. 
>1. Stand structure
>2. Ball joint structure
>3. Test for the input data and output

{{< figure src="./prototype.png" caption="[Prototype]" >}}

#### / Stand structure /
In order to follow the initial plan, I firstly made a prototype for the stand with the leftover timber in the Aalto Fablab. For this process, I only used hand tools to cut the timber and used the screws to connect to the parts of the structure. The structure were able to be used for setting up the ball joint structure and testing the object to get the data from the gyroscope sensor. Thus, this prototype process was enoughly efficient to create the actual version of the stand with the proper dimension and the shape.

#### / Ball joint structure /
I also went through various different iterations for the ball joint mechanical structure. To do this process, I used 3D printing so I did 3D modeling with Fusion360. 
For the first fast prototyped design, I tried to make this structure have only two parts. One was a **supporting part (ball-holding part)** to be directly attached to the stand structure. This part also played a role to hold the ball. The other is a **ball part**. This included the input sensor (Arduino Nano 33 IoT and the 9V battery), and connected to the rope of the skipping rope. This part could be used for testing the input and output communication. 
* Supporting (ball-holding) part
>The first try of the supporting part needed to be changed its shape because the shape was not available to fix the ball. The shape needed about a half-sphere of a hole that the ball can be put and fixed in it at the same time. So the half-sphere shouldn’t be an exact half or smaller than a half, but a bit more than half. 
>
>In order to fix the supporting part's problem, I tried to make several different shapes of the ball-holding part. Eventually, I designed the supporting part (ball-holding part) to have two components; one was the attachment component to the stand, this component also included the hole of the half-sphere shape to put the ball. The other was a cover after putting the ball into the hole component. The two components were connected by the 2mm bolts and nuts. 

* Ball part (input device part)
>The ball part could be developed as a better package for Input devices including the battery and the sensor, while the ball is connected. Because the first prototype required me to use the cable tie and tape to fix the input devices. So its appearance was not good, also it would not be safe enough when the installation works and the rope rotates. For this reason, I changed the design of this ball part to have a plate for the device and a cover of it as well. 
>
>In addition, even though I fixed the ball part into the supporting part and can rotate the ball part which is connected to a rope, connecting part between the ball and the plate was easy to be broken when the ball part came apart from the supporting part suddenly and fall down to the floor. To fix this matter, I decided to use other materials for this ball and the connecting part. 

{{< figure src="./FirstDesign3D.png" caption="[First Ball joint structure]" >}}

{{< figure src="./Prototypes.png" caption="[Broken first prototype and the second prototype]" >}}

{{< youtube id="lWBWSivjMgE" title="Ball Joint prototype" >}}
[Movement for the ball Joint prototype]

{{< figure src="./SupportingPart.png" caption="[Final prototype for the ball joint structure: Supporting part]">}}
{{< figure src="./BallPart.png" caption="[Final prototype for the ball joint structure: Ball part]">}}

#### / Test for the input data and output /
I used the gyroscope data in Arduino Nano 33 IoT to detect the rope rotation. Arduino Nano 33 IoT includes gyroscope sensor and Wi-Fi for the connectivity. Those are all I need for the input device. Thus I simply chose to use the Arduino Nano 33 IoT. From the first test with the gyroscope without the proper ball joint structure, the Z-axis was the most detectable axis for the rotating rope. However, the usable data for the rotation was from X-axis when I tested it within the prototyped installation. 

{{< figure src="./Arduino.jpg" caption="[Technical part's components: Arduino Nano 33 IoT, 9V Alkaline battery, 9V clip (solder to male)]">}}

{{< youtube id="RYuFpq0S-TY" title="Test for the rotation" >}}
[First test with Gyroscope data with TouchDesigner]

{{< youtube id="g-UKFX_kK4U" title="Test for the rotation" >}}
[Prototype Test]

&nbsp;
______
### **Fabricating**

#### / Components & Materials /
Skipping rope, Wooden ball, Stand structure, Ball joint structure, Various bolts and nuts

| Components | Materials | Dimensions (pcs) | Price | Where to get |
|--------------|-------------|---------------|---------------|---------------|
| Skipping rope | Handles: Polypropylene, Rope: PVC | 275cm length rope | 1.99 euros | Prisma | 
| Ball (bead) | Wood | 30mm diameter | 3.50 euros (4pcs) | Prisma | 
| Stand structure | MDF (1 leftover & 1 new sheet)| 1200mm W x 1200mm H x 19.3mm Thickness (2pcs) | - | Aalto Fablab |
| Ball joint structure | PLA (black) filament | 0.4mm | - | Aalto Fablab |
| Threaded bolt | Steel (cut) | 65mm length, 6mm Thickness / 1pc | - | Aalto Vare metal workshop |
| Bolts | Steel | 8mm (4pcs), 4mm (8pcs), 2mm (6pcs) | - | Aalto Fablab |
| Nuts | Steel | 8mm (4pcs), 6mm (2pcs), 4mm (8pcs), 2mm (6pcs) | - | Aalto Fablab |

#### / Processes for the stand and ball joint structure /
I designed for the stand and the ball joint structure except for the ball itself. I used a CNC milling machine to make the wooden stand. So I designed a 3D model for the CNC milling process. Also, for the ball joint structure, I modeled the mechanical object to be produced by a 3D printer. 

**Stand structure**: 3D modelling, CNC milling, sanding with the hand router and small hand sanding tool

>> * **3D modelling**
>>
>>The wooden stand's basic dimensions (height and the spot for the ball joint structure) followed the prototyped structure. However, I modified the overall shape and some of the dimensions in order to have a better function as a stand. 
>>
>> I first designed the stand to have a bottom plate to put the weight on the backside. For the joint, I added the finger joint so that it doesn't need the extra screws to combine each part of the stand. Also, I added a pocket of 2mm depth on the spot to set the ball joint structure.
>> In addition, I used 20mm thickness MDF but according to the actual thickness of the MDF, I changed it's parameter to 19.3mm.
{{< figure src="./standSketch.jpg" caption="[Sketch for the wooden stand design]">}}
{{< figure src="./standModeling.png" caption="[3D Modeling for the wooden stand on Fusion 360]">}}
{{< figure src="./standParameter.png" caption="[Parameters for the wooden stand on Fusion 360]">}}
>
>> * **CNC Milling by Recontech1312**
>>
>> Basically I followed the process of the [Assignment 09 / Computer-Controlled Machining ⇱](https://yyoona.gitlab.io/digital-fabrication/assignments/09/). 
>>
>>Before milling the actual pieces for the stand structure, I tested a small pieces including the pocket, the hole and the finger joints. The smallest hole of the piece is 8mm so I used the 8mm milling bit for the hole, pocket, and the cutting outline of the pieces. I made the toolpath with the Dog-Bone Fillet for the milling by the VCarve software. 
{{< figure src="./tool.jpeg" caption="[8mm 2Flutes Flat milling bit]">}}
{{< figure src="./CNCTest.png" caption="[CNC milling test progress on VCarve]">}}
>>
>>In order to calculate the feed rate, I refered the [guide chart for the chip load ⇱](https://www.sorotec.de/webshop/Datenblaetter/fraeser/schnittwerte_en.pdf)
{{< figure src="./Reference chart.png" caption="[Chip Load Guide]">}}
>>  ~~~
>>  [Feed rate calculation for the wooden stand]
>>    - Chip load: 0.08 mm
>>    - Number of flute: 2 n
>>    - Spindle speed: 16000 rpm
>>    - Feed Rate = chip load x number of flutes x spindle speed = 0.08 x 2 x 16000 = 2560 mm/ minute
>>    - Plunge Rate = Feed Rate / 4 = 640
>>  ~~~
>>{{< figure src="./FeedRate.jpeg" caption="[Feed rate setting on VCarve]">}}
>>
>>After the test milling, I adjusted the offset size of the hole for the 8mm bolts in Fusion360 and started to mill (Used software for generating a toolpath and milling: VCarve → Mach3) the actual pieces on the two MDF sheets.
>>
>>The stand structure is divided into 4 pieces: front, left-side, right-side, and bottom.
>>I firstly milled the front piece on the leftover MDF piece (thickness 19.3mm) and then other pieces on the new MDF piece (thickness 19.3mm). 
>>The front piece has three different toolpaths because the piece includes a cutting path, hole, and pocket. So I named each path with a different setting of the toolpath. But the differently named toolpaths are saved in a file for the next step (Mach3).
>>On the other hand, it was simpler to make the toolpath than the front piece as other pieces needed only the cutting path. 
>>
>>{{< figure src="./Front_VCarve.png" caption="[Preview of the toolpath for the front piece]">}}
>>{{< figure src="./Second_CNC.png" caption="[CNC milling process (Mach3 software view, starting milling) for the rest pieces of the stand structure]">}}
>
>>* **Post-process**
>>
>>After the milling process was done, the post-process was quite easy because the finger joints were stably well fit. During this process, I only used hand tools such as the hammer, hand router, and sanding paper to separate the pieces from the MDF sheet and remove the tab from the pieces. Finally, I assembled the pieces only with the hammer. 
>>{{< youtube id="PrsWgFSkEM0" title="Stand Structure" >}} 
>>[Assembled stand structure]

**Ball joint structure**: 3D modelling, 3D printing

>In order to make the ball which has a connector part as a ball joint, I used the ready-made wooden ball with a hole through it and threaded bolt. The threaded bolt were made by the matal cutting and sanding machine.
>
>Through the test with the prototype, I decided to use glue to fix the 6mm threaded bolt into the wooden ball because it was very easily taken apart from each other when the installation rotated. The most stable glue for this component was epoxy glue. Also, the threaded bolt with a ball needed to be fixed to the Ball part through the 6mm hole. Thus, I used the two nuts for the two sides of the hole in the Ball part. 
>
>{{< figure src="./woodblock.png" caption="[Prepare the epoxy glue for the 30mm Radius wooden ball and the threaded bolt]">}}
>
>> * **3D modelling**
>>
>>As the prototyping, I modeled the ball joint structure with Fusion360 by dividing it into the supporting part and the ball part. The only different factor from the final prototype is the size of the extruded holes for the bolts and the number of them for the supporting part. 
>>
>>The final version of the modeling has four extruded holes of the 8mm diameter for the attachment component on the wooden stand. Also, it has four extruded holes of the 4mm diameter instead of the 2mm holes for assembling the cover component and the attachment component in the supporting part. It's because the 2mm one was a bit weak so the components were easy to be taken apart from each other and it had a limitation when it comes to the length of the bolt. The general 2mm bolts’ maximum length is 12mm. In case I need the longer length of the bolts, I can not use 2mm bolts.
>>
>>{{< youtube id="sSObsqvmflo" title="Supporting part modeling" >}} 
>>[Rendering view for the supporting part on Fusion360]
>>
>>{{< youtube id="WeeGCDUZv4o" title="Ball part modeling" >}} 
>>[Rendering view for the ball part on Fusion360 (The glass material in the rendering will be the black plastic in the actual object)]
>
>> * **3D printing by Ultimaker 2+ connect**
>>
>>The process of the 3D printing followed the [Assignment 07 / 3D Scanning and Printing ⇱](https://yyoona.gitlab.io/digital-fabrication/assignments/07/#3d-print). I used the 3D printer 'Ultimaker 2+ connect', which is the brand-new machine in the Aalto Fablab. This new 3D printer has a touchscreen to select the menu and it is able to do the single extrusion printing as the Ultimaker 2+ extended. But Ultimaker 2+ connect has a shorter height than the Ultimaker 2+ extended. The build volume of the Ultimaker 2+ connect is 223 x 220 x 205 mm (8.7 x 8.6 x 8 inches).
>>
>>The saved STL files from the fusion360 were processed in the slicing software ‘Ultimaker Cura’ to generate UFP (G.code) file to send to the 3D printer. In order to slice the components, I chose the Normal quality among the default settings, and then changed the infill percentage to 40%, also the infill pattern to Gyroid. Moreover, I chose the Cross or Line as the support pattern.
>>
>>The supporting part took about 10 hours to be printed and the ball part took about 15 hours to be printed. The estimated time in Cura was quite different from the actual time for the printing. 
>>
>>{{< figure src="./supportingpart_Cura.png" caption="[Preparing the 3D printing for the supporting part of the ball joint structure]" >}}
>>
>>{{< figure src="./ballpart_Cura.png" caption="[Preparing the 3D printing for the ball part of the ball joint structure (This image is from the prototyping process. The final design has some changes from this image's component.)]" >}}
>>
>>{{< figure src="./3dprint_process_01.png" caption="[Process of the 3D printing by Ultimaker 2+ connect]" >}}
>>
>>{{< figure src="./3dprint_process_02.png" caption="[Printed components: sensor package in the ball part and the supporting part]" >}}


&nbsp;
______
### **Technical Making**

#### / Components /
Arduino Nano 33 IoT, Micro-usb cable, 9V Alkaline battery, 9V clip (solder to male)

| Components | Price | Where to get / etc|
|--------------|-------------|---------------|
| Arduino Nano 33 IoT | 17,60 euros | This Arduino Nano 33 IoT is from Matti and it was already soldered with the header pins. | 
| Micro-usb cable | about 10 euros | - | 
| 9V Alkaline battery | 3,95 euros| - |
| 9V clip | 1.05 euros | Aalto Vare Mechatronics workshop / The clip needed to be soldered to the male headers to put them into the female header of Arduino Nano 33 IoT. |

{{< figure src="./arduinoNano33IoT.jpg" caption="[Arduino Nano 33 IoT connected to the battery]">}}

#### / Processes /
**Arduino & TouchDesginer**: Input and output device (Programming)

>> * **Arduino**
>>
>> In this project, I used the gyroscope data to detect the rope rotation. From the prototype test, the Z-axis are the most detectable axis for the rotating rope within the installation. 
{{< figure src="./gyroscope.png" caption="[How the gyroscope inside the Arduino Nano 33 IoT works]" >}}
>>
>> In order to start using Arduino Nano 33 IoT, I needed to look into the specification so I refered the document of the [official Arduino website ⇱](https://docs.arduino.cc/hardware/nano-33-iot). From the documents, I tried to find the input voltage because I needed to choose the possible voltage of the battery to connect the device to the battery wirelessly. The input voltage of the Arduino Nano 33 IoT is 5-18V.
>>
>> Also, I found the pinout information from the datasheet. The wiring for my project is very simple because I only need GND and VIN pin to be connected by the battery. 
>> {{< figure src="./ABX00027-pinout.png" caption="[Arduino Nano 33 IoT Pinout information]">}}
>>
>>
>>  ~~~
>>  [Libraries]
>>    - ArduinoOSC
>>    - WiFiNiNA
>>    - Arduino_LSM6DS3
>>    - Arduino ACR Boards
>>    - Arduino megaAVR Borads
>>  ~~~
>>
>>{{< figure src="./Arduino_programming_01.png" caption="[Choose the board in Arduino]" >}}
>>
>>>* Programming code
>>> ~~~
>>>//#include <ArduinoOSCEther.h>
>>>#include <ArduinoOSCWiFi.h>
>>>
>>>/*
>>>  Arduino LSM6DS3 - Simple Gyroscope
>>>
>>>  This example reads the gyroscope values from the LSM6DS3
>>>  sensor and continuously prints them to the Serial Monitor
>>>  or Serial Plotter.
>>>
>>>  The circuit:
>>>  - Arduino Uno WiFi Rev 2 or Arduino Nano 33 IoT
>>>
>>>  created 10 Jul 2019
>>>  by Riccardo Rizzo
>>>
>>>  This example code is in the public domain.
>>>*/
>>>
>>>#include <Arduino_LSM6DS3.h>
>>>#include <ArduinoOSCWiFi.h>
>>>
>>>float x, y, z;
>>>
>>>
>>>// WiFi stuff
>>>const char* ssid = "aalto open";
>>>const char* pwd = "";
>>>
>>>// for ArduinoOSC
>>>// this is the IP of your computer
>>>// make sure you are on the same network
>>>const char* host = "10.100.57.166";
>>>const int recv_port = 54321;
>>>const int send_port = 55555;
>>>
>>>void setup() {
>>>  Serial.begin(9600);
>>>  //while (!Serial);
>>>
>>>  if (!IMU.begin()) {
>>>    Serial.println("Failed to initialize IMU!");
>>>    while (1);
>>>  }
>>>  //connect to WiFi
>>>  WiFi.begin(ssid, pwd);
>>>  while (WiFi.status() != WL_CONNECTED) {
>>>    Serial.print(".");
>>>  }
>>>  Serial.print("WiFi connected, IP = ");
>>>  Serial.println(WiFi.localIP());
>>>
>>>  Serial.print("Gyroscope sample rate = ");
>>>  Serial.print(IMU.gyroscopeSampleRate());
>>>  Serial.println(" Hz");
>>>  Serial.println();
>>>  Serial.println("Gyroscope in degrees/second");
>>>  Serial.println("X\tY\tZ");
>>>
>>>  // this command starts to publish the values
>>>  OscWiFi.publish(host, send_port, "/gyro", x, y, z);
>>>}
>>>
>>>void loop() {
>>>
>>>  if (IMU.gyroscopeAvailable()) {
>>>    IMU.readGyroscope(x, y, z);
>>>
>>>  Serial.print(x);
>>>    Serial.print('\t');
>>>    Serial.print(y);
>>>    Serial.print('\t');
>>>    Serial.println(z);
>>>  }
>>>  // this update is needed for the publishing to work
>>>  OscWiFi.update();
>>>  delay(100);
>>>}
>>>  ~~~
>>{{< figure src="./SensorData.png" caption="[Gyroscope data flows in Serial monitor view of Arduino]" >}}
>
>> * **TouchDesigner**
>>
>> TouchDesigner (TD) is a node based visual programming language for real time interactive multimedia content. I chose TouchDesigner as an output device to show the video which is controlled by the sensor data from the input device (Arduino Nano 33 IoT). The reason is that TD can communicate with Arduino by the OSC function. In order to make them communicate, the IP Address for the Wi-Fi is needed.
>>> - What is OSC? 
>>>
>>> Open Sound Control (OSC) is an open, transport-independent, message-based encoding developed for communication among computers, sound synthesizers, and other multimedia devices.
>>
>>{{< figure src="./TouchDesignerTest.png" caption="[Check the data flow in TouchDesigner]" >}}
>>
______

### **How the installation works**

1. Visitors rotate the rope with the handle. 
2. If the rope rotate, the gyroscope sensor detects the clockwise and counterclockwise. The gyroscope is already built in the Arduino Nano 33 IoT. 
3. The dectection is sent to Arduino as a data on the computer.
4. Arduino sends the data to TouchDesigner.
5. Through the TouchDesigner, the data becomes a trigger to start to make the person do the skipping rope. Visitors can rotate the rope in two directions. Regardless of the direction, the person on the screen jumps and stops. 

______

### **Reflections**

The plastic material of the ball joint keeps getting worn from the rotating ball. To fix this problem, I can try to make this part with other material such as the nylon plastic. 

In addition, I couldn’t manage the programming part. 
I used TouchDesigner to map the sensor(gyroscope) data to video play through TouchDesigner. But the data is too fluctuate and to map it to the video play with controlling the rotation of the skipping rope.

&nbsp;
______
### **Source files**
- Original design files for the structure → [DesignFiles.zip](./../Final%20Project%3A%20Rotate%20a%20Rope/files/DesignFiles.zip)
- Code for Arduino & Touch Designer → [CodeFiles.zip](./../Final%20Project%3A%20Rotate%20a%20Rope/files/CodeFiles.zip)

